/****************************************************************************************
 * 文件名: kf_it.c
 * 项目名: I2C_Slave_Mode
 * 版 本:  V2.4
 * 日 期:  2021年7月29日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了工程的中断入口函数。
 *
 ****************************************************************************************/
#include "system_init.h"
#include "I2C_Slave.h"


extern void I2C_receive_input(uint8_t Rev_Temp);
volatile uint32_t i2c_data_temp;   //接收到I2C数据的变量
uint8_t txdata[10]={0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xA};
volatile uint8_t txIndex = 0;
volatile uint8_t i2c_stop_flg = 0;  //i2c stop 软件标志（说明一帧数据发完）
extern volatile uint8_t I2C_RecNum;

/**
  * 描述   I2C1中断函数
  * 输入
  *
  * 返回
*/
void __attribute__((interrupt))_I2C1_exception(void)
{


	volatile uint8_t I2C1_REDATA = 0;
	volatile uint32_t I2C1_ADD = 0;
	i2c_data_temp = 0;



	if(I2C1_SR & I2C_SR_SIF) //Start 中断
	{
		I2C1_SR &= 0xFFFFFFFE;  //I2C_Clear_Start_Flag(I2C1_SFR);
		asm("NOP");
		I2C1_SR &= 0xFFFFF7FF;	//I2C_Clear_INTERRUPT_Flag(I2C1_SFR);
		return;
	}


	if(I2C1_SR & I2C_SR_PIF) // Stop 中断
	{
		I2C1_SR &= 0xFFFFFFFD;  //I2C_Clear_Stop_Flag(I2C1_SFR);
		asm("NOP");
		I2C1_SR &= 0xFFFFF7FF;  //I2C_Clear_INTERRUPT_Flag(I2C1_SFR);
		i2c_stop_flg = 1;
		return;
	}

	if(I2C_Get_Write_Read_Flag(I2C1_SFR))//主机读状态位，从动发送
	{
		if(I2C1_SR & I2C_SR_DATA)       //接收到的是数据
		{
			/*
			 * Add user code...
			 */
		}
		else                            //接收到的是地址
		{
			I2C1_ADD = I2C1_BUFR;
			txIndex=0;    				//接受到地址开始发送第一个数据
			/*
			 * Add user code...
			 */
		}

		if(txIndex>9)
		{
			txIndex=9;
		}
		I2C1_BUFR = *(txdata+(txIndex++));

	}
	else                                //主机写状态位，从动接收
	{
		i2c_data_temp = I2C1_BUFR;
		if(I2C1_SR & I2C_SR_DATA)       //接收到的是数据
		{
			I2C1_REDATA = i2c_data_temp & 0xff;
			I2C_receive_input(I2C1_REDATA);
		}
		else                            //接收到的是地址
		{
			I2C1_ADD = i2c_data_temp;
			I2C_RecNum = 0;
		}

	}

	I2C1_SR &= 0xFFFFF7FF;  //I2C_Clear_INTERRUPT_Flag(I2C1_SFR);
}




