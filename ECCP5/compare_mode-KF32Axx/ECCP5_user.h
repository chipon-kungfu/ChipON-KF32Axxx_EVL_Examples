/*
 * ECCP5_user.h
 *
 *  Created on: 2019-12-5
 *      Author: charles_cai
 */

#ifndef ECCP5_USER_H_
#define ECCP5_USER_H_

void cfg_gpio_for_ECCP5();
void cfg_ECCP5();

#endif /* ECCP5_USER_H_ */
