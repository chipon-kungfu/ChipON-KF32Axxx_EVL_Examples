/****************************************************************************************
 *
 * 文件名 kf_it.c
 * 作  者  ChipON_AE/FAE_Group
 * 日  期  2019-11-16
 * 描  述  该文件提供了QEI应用例程使用的
 *       +QEI周期计数中断
 *       +T0中断模拟正交编码信号
 ****************************************************************************************/
#include"system_init.h"

//*****************************************************************************************
//                              T0中断函数
//*****************************************************************************************
void __attribute__((interrupt))_T0_exception (void)
{
	//模拟编码编码器信号
	static uint16_t num0 = 1;
	GPTIM_Clear_Updata_INT_Flag(T0_SFR);										//清更新时间标志位
	GPTIM_Clear_Overflow_INT_Flag (T0_SFR);										//清T0溢出中断标志位
	if(num0%2)
	{
#ifdef A_LEAD_B
		GPIO_Toggle_Output_Data_Config (GPIOF_SFR,GPIO_PIN_MASK_1);			    //A相翻转
#else
		GPIO_Toggle_Output_Data_Config (GPIOF_SFR,GPIO_PIN_MASK_2);				//B相翻转
#endif
	}
	else
	{
#ifdef A_LEAD_B
		GPIO_Toggle_Output_Data_Config (GPIOF_SFR,GPIO_PIN_MASK_2);				//B相翻转
#else
		GPIO_Toggle_Output_Data_Config (GPIOF_SFR,GPIO_PIN_MASK_1);			    //A相翻转
#endif
	}
	num0 ++;
}

//*****************************************************************************************
//                                 QEI中断函数
//*****************************************************************************************	
void __attribute__((interrupt))_T7_T8_QEI_exception (void)
{
	//GPIOF3输出脉冲，指示QEI周期匹配进入中断（x2模式，周期值例程为3）
	GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_3,Bit_SET);//GPIOF3为高
	QEI_Clear_Timer_Overflow_INT_Flag (QEI0_SFR);
	GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_3,Bit_RESET);//GPIOF3为低
}

