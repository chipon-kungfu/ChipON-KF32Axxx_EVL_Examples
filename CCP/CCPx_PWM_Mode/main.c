/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: CCPx_PWM_Mode
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了CCP PWM输出应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
#define					PWM_Edge_Out				0	//边沿对齐PWM信号
#define					PWM_Center_Out 			    1	//中心对齐PWM信号

#define					CCP_PWM_WORK			PWM_Edge_Out	//  选择PWM工作模式，  0：边沿对齐PWM信号    1：中心对齐PWM信号




/**
  * 描述  CCPx PWM功能GPIO重映配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *     GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  *    PinRemap: 引脚重映射选择
  * 返回  无。
  */
void CCPx_PWM_GPIO_Init(GPIO_SFRmap* GPIOx,uint16_t GpioPin,uint8_t PinRemap)													//CCP0 的PWM模式输出
{
	 static uint16_t GpioPinNum;

	GPIO_Write_Mode_Bits(GPIOx,GpioPin,GPIO_MODE_RMP);

	if(GpioPin==GPIO_PIN_MASK_0)
	{
		GpioPinNum =GPIO_Pin_Num_0;
	}else if(GpioPin==GPIO_PIN_MASK_1)
	{
		GpioPinNum =GPIO_Pin_Num_1;
	}else if(GpioPin==GPIO_PIN_MASK_2)
	{
		GpioPinNum =GPIO_Pin_Num_2;
	}else if(GpioPin==GPIO_PIN_MASK_3)
	{
		GpioPinNum =GPIO_Pin_Num_3;
	}else if(GpioPin==GPIO_PIN_MASK_4)
	{
		GpioPinNum =GPIO_Pin_Num_4;
	}else if(GpioPin==GPIO_PIN_MASK_5)
	{
		GpioPinNum =GPIO_Pin_Num_5;
	}else if(GpioPin==GPIO_PIN_MASK_6)
	{
		GpioPinNum =GPIO_Pin_Num_6;
	}else if(GpioPin==GPIO_PIN_MASK_7)
	{
		GpioPinNum =GPIO_Pin_Num_7;
	}else if(GpioPin==GPIO_PIN_MASK_8)
	{
		GpioPinNum =GPIO_Pin_Num_8;
	}else if(GpioPin==GPIO_PIN_MASK_9)
	{
		GpioPinNum =GPIO_Pin_Num_9;
	}else if(GpioPin==GPIO_PIN_MASK_10)
	{
		GpioPinNum =GPIO_Pin_Num_10;
	}else if(GpioPin==GPIO_PIN_MASK_11)
	{
		GpioPinNum =GPIO_Pin_Num_11;
	}else if(GpioPin==GPIO_PIN_MASK_12)
	{
		GpioPinNum =GPIO_Pin_Num_12;
	}else if(GpioPin==GPIO_PIN_MASK_13)
	{
		GpioPinNum =GPIO_Pin_Num_13;
	}else if(GpioPin==GPIO_PIN_MASK_14)
	{
		GpioPinNum =GPIO_Pin_Num_14;
	}else if(GpioPin==GPIO_PIN_MASK_15)
	{
		GpioPinNum =GPIO_Pin_Num_15;
	}

	GPIO_Pin_RMP_Config(GPIOx,GpioPinNum,PinRemap);     //重映射功能


}

/**
  * 描述  CCPx PWM输出四路同周期不同占空比的功能函数
  * 输入 : CCPx: 指向CCP或通用定时器内存结构的指针，
  *               取值CCP0_SFR/CCP1_SFR/CCP2_SFR/CCP3_SFR/CCP4_SFR/
  *
  *
  * 返回  无。
  */
void CCPx_FOUR_PWM(CCP_SFRmap* CCPx)
{

	TIM_Reset(CCPx);//定时器外设复位，使能外设时钟

	CCP_PWM_Mode_Config(CCPx,CCP_CHANNEL_1,CCP_PWM_MODE);								//CCP PWM功能通道模式
	CCP_PWM_Mode_Config(CCPx,CCP_CHANNEL_2,CCP_PWM_MODE);								//CCP PWM功能通道模式
	CCP_PWM_Mode_Config(CCPx,CCP_CHANNEL_3,CCP_PWM_MODE);								//CCP PWM功能通道模式
	CCP_PWM_Mode_Config(CCPx,CCP_CHANNEL_4,CCP_PWM_MODE);								//CCP PWM功能通道模式
	CCP_Channel_Output_Control(CCPx,CCP_CHANNEL_1,CCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//配置CCP通道输出控制PWM输出，高有效
	CCP_Channel_Output_Control(CCPx,CCP_CHANNEL_2,CCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//配置CCP通道输出控制PWM输出，高有效
	CCP_Channel_Output_Control(CCPx,CCP_CHANNEL_3,CCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//配置CCP通道输出控制PWM输出，高有效
	CCP_Channel_Output_Control(CCPx,CCP_CHANNEL_4,CCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//配置CCP通道输出控制PWM输出，高有效
	CCP_Set_Compare_Result(CCPx,CCP_CHANNEL_1,0x1FFF);									//CHANNEL_1 PWM占空比
	CCP_Set_Compare_Result(CCPx,CCP_CHANNEL_2,0x3FFF);									//CHANNEL_2 PWM占空比
	CCP_Set_Compare_Result(CCPx,CCP_CHANNEL_3,0x7FFF);									//CHANNEL_3 PWM占空比
	CCP_Set_Compare_Result(CCPx,CCP_CHANNEL_4,0xAFFF);									//CHANNEL_4 PWM占空比


	GPTIM_Updata_Immediately_Config(CCPx,TRUE);									//立即更新控制
	GPTIM_Updata_Enable(CCPx,TRUE);												//配置更新使能
	GPTIM_Work_Mode_Config(CCPx,GPTIM_TIMER_MODE);								//定时模式选择
	GPTIM_Set_Counter(CCPx,0);													//定时器计数值
	GPTIM_Set_Period(CCPx,0xFFFF);											    //定时器周期值
	GPTIM_Set_Prescaler(CCPx,47);											   //定时器预分频值
#if		CCP_PWM_WORK==PWM_Edge_Out
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_OF);						   //向上计数模式,即边沿对齐PWM信号
#else
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_DOWN_OF);					   //向上、向下计数模式,即中心对齐PWM信号
#endif

	GPTIM_Clock_Config(CCPx,GPTIM_SCLK);										//选用SCLK时钟
	GPTIM_Cmd(CCPx,TRUE);                                                       //定时器启动控制使能
}

/**
  * 描述  CCP20 CCP21 PWM输出功能初始化参数。
  * 输入 : CCPx: 指向CCP或通用定时器内存结构的指针，
  *               取值/CCP20_SFR/CCP21_SFR
  *    Channel:  CCP_CHANNEL_1: 通道1
  *              CCP_CHANNEL_2: 通道2
  *              CCP_CHANNEL_3: 通道3
  *              CCP_CHANNEL_4: 通道4
  *      Period： 周期值：32位数
  *     Duty：占空比：32位数
  *
  * 返回  无。
  */
void CCP2021_PWM_PPX_Duty(CCP_SFRmap* CCPx, uint32_t Channel,uint32_t Period,uint32_t Duty)
{

	TIM_Reset(CCPx);//定时器外设复位，使能外设时钟

	CCP_PWM_Mode_Config(CCPx,Channel,CCP_PWM_MODE);								//CCP PWM功能通道模式
	CCP_Channel_Output_Control(CCPx,Channel,CCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//配置CCP通道输出控制PWM输出，高有效
	CCP_Set_Compare_Result(CCPx,Channel,Duty);									//PWM占空比


	GPTIM_Updata_Immediately_Config(CCPx,TRUE);									//立即更新控制
	GPTIM_Updata_Enable(CCPx,TRUE);												//配置更新使能
	GPTIM_Work_Mode_Config(CCPx,GPTIM_TIMER_MODE);								//定时模式选择
	GPTIM_Set_Counter(CCPx,0);													//定时器计数值
	GPTIM_Set_Period(CCPx,Period);											    //定时器周期值PPX
	GPTIM_Set_Prescaler(CCPx,2);												//定时器预分频值

#if		CCP_PWM_WORK==PWM_Edge_Out
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_OF);							//向上计数模式,即边沿对齐PWM信号
#else
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_DOWN_OF);							    	//向上、向下计数模式,即中心对齐PWM信号
#endif

	GPTIM_Clock_Config(CCPx,GPTIM_SCLK);										//选用SCLK时钟
	GPTIM_Cmd(CCPx,TRUE);                                                       //定时器启动控制使能
}

/**
  * 描述  CCPx PWM输出功能初始化参数。
  * 输入 : CCPx: 指向CCP或通用定时器内存结构的指针，
  *               取值CCP0_SFR/CCP1_SFR/CCP2_SFR/CCP3_SFR/CCP4_SFR/
  *               CCP18_SFR/CCP19_SFR/CCP22_SFR/CCP23_SFR。
  *    Channel:  CCP_CHANNEL_1: 通道1
  *              CCP_CHANNEL_2: 通道2
  *              CCP_CHANNEL_3: 通道3
  *              CCP_CHANNEL_4: 通道4
  *      Period： 周期值：16位数
  *        Duty： 占空比：16位数
  *
  * 返回  无。
  */
void CCPx_PWM_PPX_Duty(CCP_SFRmap* CCPx, uint32_t Channel,uint16_t Period,uint16_t Duty)
{

	TIM_Reset(CCPx);//定时器外设复位，使能外设时钟
	CCP_PWM_Mode_Config(CCPx,Channel,CCP_PWM_MODE);								//CCP PWM功能通道模式
	CCP_Channel_Output_Control(CCPx,Channel,CCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//配置CCP通道输出控制PWM输出，高有效
	CCP_Set_Compare_Result(CCPx,Channel,Duty);									//PWM占空比
	GPTIM_Updata_Immediately_Config(CCPx,TRUE);									//立即更新控制
	GPTIM_Updata_Enable(CCPx,TRUE);												//配置更新使能
	GPTIM_Work_Mode_Config(CCPx,GPTIM_TIMER_MODE);								//定时模式选择
	GPTIM_Set_Counter(CCPx,0);													//定时器计数值
	GPTIM_Set_Period(CCPx,Period);											    //定时器周期值Period
	GPTIM_Set_Prescaler(CCPx,118);											   //定时器预分频值
#if		CCP_PWM_WORK==PWM_Edge_Out
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_OF);						   //向上计数模式,即边沿对齐PWM信号
#else
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_DOWN_OF);				       //向上、向下计数模式,即中心对齐PWM信号
#endif
	GPTIM_Clock_Config(CCPx,GPTIM_SCLK);										//选用SCLK时钟
	GPTIM_Cmd(CCPx,TRUE);                                                       //定时器启动控制使能
}




//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现PWM模式的配置功能。
	     1、设置CCP0  通道3  1KHZ 占空比为50% PWM输出PB8
	     2、设置CCP3 通道3  15HZ 占空比为50% PWM输出PB13  D4会以15HZ频率闪烁
	     3、设置CCP20 通道0 10HZ 占空比为50% PWM输出PE0
	     4、设置CCP1    四个通道    以相同频率不同占空比的PWM输出  38HZ PWM输出
	     CCP_PWM_WORK    宏定义选择：边沿对齐模式或中心对齐模式
	  挂示波器可以测量到PB8为1KHZ左右的PWM,测量PB13为15HZ的PWM，PE0为10HZ的PWM 占空比都为50%
	 */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化
	CCPx_PWM_GPIO_Init(GPIOB_SFR,GPIO_PIN_MASK_8,GPIO_RMP_AF1_T0);  //CCP0  PB8重映射PWM功能
	CCPx_PWM_GPIO_Init(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_RMP_AF1_T3); //CCP3  PB13重映射PWM功能
	CCPx_PWM_GPIO_Init(GPIOE_SFR,GPIO_PIN_MASK_0,GPIO_RMP_AF3_T20); //CCP20 PE0重映射PWM功能

	CCPx_PWM_GPIO_Init(GPIOF_SFR,GPIO_PIN_MASK_0,GPIO_RMP_AF1_T1); //CCP1   PF0重映射为PWM功能  38HZ PWM输出
	CCPx_PWM_GPIO_Init(GPIOB_SFR,GPIO_PIN_MASK_1,GPIO_RMP_AF1_T1); //CCP1   PB1重映射为PWM功能  38HZ PWM输出
	CCPx_PWM_GPIO_Init(GPIOB_SFR,GPIO_PIN_MASK_2,GPIO_RMP_AF1_T1); //CCP1   PB2重映射为PWM功能  38HZ PWM输出
	CCPx_PWM_GPIO_Init(GPIOB_SFR,GPIO_PIN_MASK_3,GPIO_RMP_AF1_T1); //CCP1   PB3重映射为PWM功能  38HZ PWM输出

	CCPx_PWM_PPX_Duty(CCP0_SFR,CCP_CHANNEL_3,1000,500);  //设置CCP0   通道3 PB8 1KHZ 占空比为50% PWM输出
	CCPx_PWM_PPX_Duty(CCP3_SFR,CCP_CHANNEL_3,65535,32767);//设置CCP3  通道3  PB13 15HZ  占空比为50% PWM输出
	CCP2021_PWM_PPX_Duty(CCP20_SFR,CCP_CHANNEL_1,0X3D09FF,0X1E8480);//设置CCP20 通道0 PE0 10HZ 占空比为50% PWM输出

	CCPx_FOUR_PWM(CCP1_SFR); //设置CCP1    四个通道    以相同频率不同占空比的PWM输出  38HZ PWM输出
	while(1)
    {
    }
}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

