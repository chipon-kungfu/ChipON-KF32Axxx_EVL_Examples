/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ADC_user.h"
#include "USART_user.h"
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               硬件错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                                堆栈错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//                              T1中断函数
//*****************************************************************************************
void __attribute__((interrupt))_T1_exception (void)
{
	if(GPTIM_Get_Overflow_INT_Flag(T1_SFR)) //溢出中断，1s
	{
		GPTIM_Clear_Overflow_INT_Flag(T1_SFR);
	}
}

//                              DMA0中断函数
//*****************************************************************************************
void __attribute__((interrupt))_DMA0_exception (void)
{
	char i=0;
	unsigned int buf_a, buf_b, buf_c;
	unsigned short a ,b ,c;
	if(DMA_Get_Half_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_5)) //实际上ADC产生数据的速度要比串口发送数据的速度快的多，
	{
		buf_a= buf_b =buf_c =0;
		DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_HALF_TRANSFER); //DMA0_ch5 对应AD0

		for(i =0;i<16;i++)
		{
			buf_a +=buf_1[3*i];
			buf_b +=buf_1[3*i +1];
			buf_c +=buf_1[3*i +2];
		}
		a =buf_a>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF
		b =buf_b>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF
		c =buf_c>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF

		USART_Send_byte(USART2_SFR,(unsigned char)(a>>8));
		USART_Send_byte(USART2_SFR,(unsigned char)a);
		USART_Send_byte(USART2_SFR,0x00);

		USART_Send_byte(USART2_SFR,(unsigned char)(b>>8));
		USART_Send_byte(USART2_SFR,(unsigned char)b);
		USART_Send_byte(USART2_SFR,0x01);

		USART_Send_byte(USART2_SFR,(unsigned char)(c>>8));
		USART_Send_byte(USART2_SFR,(unsigned char)c);
		USART_Send_byte(USART2_SFR,0x02);
	}

	if(DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_5))
	{
		buf_a= buf_b =buf_c =0;
		DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_FINISH_TRANSFER); //DMA0_ch3 对应AD0
		for(i =0;i<16;i++)
		{
			buf_a +=buf_1[3*(i+16)];
			buf_b +=buf_1[3*(i+16) +1];
			buf_c +=buf_1[3*(i+16) +2];
		}

		a =buf_a>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF
		b =buf_b>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF
		c =buf_c>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF

		USART_Send_byte(USART2_SFR,(unsigned char)(a>>8));
		USART_Send_byte(USART2_SFR,(unsigned char)a);
		USART_Send_byte(USART2_SFR,0x00);

		USART_Send_byte(USART2_SFR,(unsigned char)(b>>8));
		USART_Send_byte(USART2_SFR,(unsigned char)b);
		USART_Send_byte(USART2_SFR,0x01);

		USART_Send_byte(USART2_SFR,(unsigned char)(c>>8));
		USART_Send_byte(USART2_SFR,(unsigned char)c);
		USART_Send_byte(USART2_SFR,0x02);
	}
}

