/*******************************************************************************
 *
 * 文件名  AES.h
 * 作  者  ChipON_AE/FAE_Group
 * 日  期  2019-10-19
 * 描  述  该文件提供了AES加密/解密相关函数头文件
 *
 ******************************************************************************/
#ifndef _AES_H_
#define _AES_H_

extern void AES_Encry(uint8_t* Data);//硬件加密
extern void AES_ECB_Configuration();//硬件AES配置
extern void AES_Load_Data(uint32_t* SK_Data, uint32_t* Play_data);//硬件AES数据加载

extern void AES_soft_Encrypt(unsigned char *ucInput_Chal, unsigned char *ucInputKey);//软件AES加密
extern void AES_soft_Decrypt(unsigned char *ucInput_Chal, unsigned char *ucInputKey);//软件AES解密

#endif
