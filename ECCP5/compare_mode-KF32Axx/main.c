/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目描述了将ECCP5比较模式模拟输出PWM波形。在ECCP5配置为比较模式的情况下，
  * 	通过反复切换每个T5定时器周期、比较值和比较输出的高低电平状态，实现PWM波形的输出。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ECCP5_user.h"
#include "Adv_timer.h"
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit(); //在操作备份域之前，需要将系统时钟降低到48M

	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_MODE_OUT); //配置STATUS灯为输出状态，
	cfg_gpio_for_ECCP5(); //配置比较模式的输出管脚
	TIM_Reset(T5_SFR);	//定时器和ECCP5外设同时复位，并使能其时钟管脚
	cfg_ECCP5(); //配置比较模式
	cfg_Tx(T5_SFR); //配置T5定时器
	cfg_T5_intrup(); //配置T5的中断
	while(1)
	{

	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
