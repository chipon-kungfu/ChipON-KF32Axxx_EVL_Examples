/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: EXIT
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了外部中断应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);


	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	 //先设置为高电平

}

/**
  * 描述  GPIOx 输入初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Input_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{

	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输入模式 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Mode = GPIO_MODE_IN;                      //初始化 GPIO方向为输入
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;                     //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;                   //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);
}



/**
  * 描述  外部中断(EINT)中断源配置(针对GPIPA~GPIOH)
  * 输入 :  EintNum: 外部中断编号，
  *              取值为宏INT_EXTERNAL_INTERRUPT_0至INT_EXTERNAL_INTERRUPT_31中的一个 即0~15。
  *
  * PeripheralSource: 外设中断线的中断输入源， INT_EXTERNAL_SOURCE_PA~INT_EXTERNAL_SOURCE_PF
  * 返回  无。
  */
void EXIT_INTx_Config(uint32_t EintNum,uint32_t PeripheralSource)
{
	EINT_InitTypeDef EXIT_InitStructure;
	INT_External_Struct_Init(&EXIT_InitStructure);
	EXIT_InitStructure.m_Line = EintNum;                   //设置外部中断编号
	EXIT_InitStructure.m_Mask = TRUE;                      //设置外部中断使能控制
	EXIT_InitStructure.m_Rise = TRUE;                     //设置外部中断上升沿中断使能
	EXIT_InitStructure.m_Fall = FALSE;                     //设置外部中断下降沿中断使能
	EXIT_InitStructure.m_Source = PeripheralSource;        //设置外部中断的中断源选择
	INT_External_Configuration(&EXIT_InitStructure);       //中断配置功能
	INT_External_Source_Enable(EintNum,PeripheralSource);  //使能外部中断源


    if(EintNum==INT_EXTERNAL_INTERRUPT_0)
    {
    	INT_Interrupt_Enable(INT_EINT0,TRUE);								//使能中断EINT0
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_1)
    {
    	INT_Interrupt_Enable(INT_EINT1,TRUE);								//使能中断EINT1
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_2)
    {
    	INT_Interrupt_Enable(INT_EINT2,TRUE);								//使能中断EINT2
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_3)
    {
    	INT_Interrupt_Enable(INT_EINT3,TRUE);								//使能中断EINT3
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_4)
    {
    	INT_Interrupt_Enable(INT_EINT4,TRUE);								//使能中断EINT4
    }else if((EintNum>=INT_EXTERNAL_INTERRUPT_5)&&(EintNum<=INT_EXTERNAL_INTERRUPT_9))
    {
    	INT_Interrupt_Enable(INT_EINT9TO5,TRUE);							//使能中断EINT5~EINT9
    }else if((EintNum>=INT_EXTERNAL_INTERRUPT_10)&&(EintNum<=INT_EXTERNAL_INTERRUPT_15))
    {
    	INT_Interrupt_Enable(INT_EINT15TO10,TRUE);							//使能中断EINT10~EINT15
    }

	INT_All_Enable (TRUE);      //使能总中断
}


//主函数
void main()
{

	/*用户可参考该例程在KF32A151_demo板上实现外部中断EINT配置功能。
	 * 例程中使用外部EINT7实验，通过中断源为PD7 上升沿来触发EINT7并使PB13翻转，
	 * 按键USER按下，D4灯熄灭，再次按下D4灯点亮。
     *
     * *注意：调用函数设置中断向量号和中断源的选择，要将中断源IO初始化为输入模式以及打开中断函数的入口
     * ETIN5~ETIN9   共用中断入口地址 及中断标志位
     * ETIN10~ETIN15 共用中断入口地址 及中断标志位
     * ETIN17~ETIN19 共用中断入口地址 及中断标志位
     * ETIN20~ETIN21 共用中断入口地址 及中断标志位
     * ETIN22~ETIN31 共用中断入口地址 及中断标志位
     * 进入中断后需要软件判断是哪个中断向量触发的中断标志
	 */
	//系统时钟120M,外设高频时钟16M
	SystemInit();//时钟初始化

	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);  //PB13初始化输出高电平
	GPIOInit_Input_Config(GPIOD_SFR,GPIO_PIN_MASK_7);    //PD7初始化为输入

	//外部中断EINT7上升沿触发中断
	EXIT_INTx_Config(INT_EXTERNAL_INTERRUPT_7,INT_EXTERNAL_SOURCE_PD);  //外部中断EINT7中断源PD7配置
    while(1)
    {
    }

}


void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

