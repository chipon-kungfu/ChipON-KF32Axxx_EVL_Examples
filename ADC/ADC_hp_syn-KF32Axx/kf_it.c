/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "ADC_user.h"
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               硬件错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                                堆栈错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

void __attribute__((interrupt))_ADC0_exception(void)
{
	Adc_value a ,b ,c;
	if(ADC_Get_INT_Flag (ADC0_SFR, ADC_INT_HPEND))
	{
		ADC_Clear_INT_Flag (ADC0_SFR, ADC_INT_HPEND);

		a.buf_16 =ADC_Get_HPConversion_Data(ADC0_SFR,ADC_HPDATA_0);
		b.buf_16 =ADC_Get_HPConversion_Data(ADC0_SFR,ADC_HPDATA_1);
		c.buf_16 =ADC_Get_HPConversion_Data(ADC0_SFR,ADC_HPDATA_2);

		USART_Send_byte(USART2_SFR,a.buf_8[1]);
		USART_Send_byte(USART2_SFR,a.buf_8[0]);
		USART_Send_byte(USART2_SFR,(uint8_t)(a.buf_16 /124));
		USART_Send_byte(USART2_SFR,0x11);

		USART_Send_byte(USART2_SFR,b.buf_8[1]);
		USART_Send_byte(USART2_SFR,b.buf_8[0]);
		USART_Send_byte(USART2_SFR,(uint8_t)(b.buf_16 /124));
		USART_Send_byte(USART2_SFR,0x12);

		USART_Send_byte(USART2_SFR,c.buf_8[1]);
		USART_Send_byte(USART2_SFR,c.buf_8[0]);
		USART_Send_byte(USART2_SFR,(uint8_t)(c.buf_16 /124));
		USART_Send_byte(USART2_SFR,0x13);
	}
}

void __attribute__((interrupt))_ADC1_exception(void)
{
	Adc_value a ,b ,c;
	if(ADC_Get_INT_Flag (ADC1_SFR, ADC_INT_HPEND))
	{
		ADC_Clear_INT_Flag (ADC1_SFR, ADC_INT_HPEND);

		a.buf_16 =ADC_Get_HPConversion_Data(ADC1_SFR,ADC_HPDATA_0);
		b.buf_16 =ADC_Get_HPConversion_Data(ADC1_SFR,ADC_HPDATA_1);
		c.buf_16 =ADC_Get_HPConversion_Data(ADC1_SFR,ADC_HPDATA_2);

		USART_Send_byte(USART2_SFR,a.buf_8[1]);
		USART_Send_byte(USART2_SFR,a.buf_8[0]);
		USART_Send_byte(USART2_SFR,(uint8_t)(a.buf_16 /124));
		USART_Send_byte(USART2_SFR,0x14);

		USART_Send_byte(USART2_SFR,b.buf_8[1]);
		USART_Send_byte(USART2_SFR,b.buf_8[0]);
		USART_Send_byte(USART2_SFR,(uint8_t)(b.buf_16 /124));
		USART_Send_byte(USART2_SFR,0x15);

		USART_Send_byte(USART2_SFR,c.buf_8[1]);
		USART_Send_byte(USART2_SFR,c.buf_8[0]);
		USART_Send_byte(USART2_SFR,(uint8_t)(c.buf_16 /124));
		USART_Send_byte(USART2_SFR,0x16);
	}

}

