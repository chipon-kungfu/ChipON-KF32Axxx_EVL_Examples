/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了ECCP5的捕捉模式的使用方法。该例程使用了测量模式，对示波器的的参考信号进行验证（1kHz、50%占空比）。
  * 	测量模式下，ECCP5CH1H做为信号输入管脚，ECCP5通道1捕捉脉宽数据，通道2捕捉周期数据。每个信号周期，都会将T5的计数值请0。
  * 	如果输入信号正确（1kHz、50%占空比），LED0将会快速闪烁。
  *********************************************************************
  */
#include "system_init.h"

unsigned int UP_PLUSE_WIDTH=0 ,DUTY =0;

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit(); //在操作备份域之前，需要将系统时钟降低到48M

	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_11,GPIO_MODE_RMP); //CCP5CH1H做为CCP5捕捉模式的输入引脚
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_MODE_OUT);

	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_11,GPIO_RMP_AF2_T5);//CCP5CH1H

	TIM_Reset(T5_SFR);																	//定时器外设复位，ECCP5也复位，使能外设时钟
	ECCP_PWM_Input_Enable(ECCP5_SFR,TRUE);												//配置为测量模式
	ATIM_X_Slave_Mode_Config(T5_SFR,ATIM_SLAVE_RESET_MODE);								//从模式配置为复位模式，触发信号上升沿将进行定时器计数复位
	ATIM_Trigger_Select_Config(T5_SFR,ATIM_TRIGGER_ECCPXCH1);							//ECCP5CH1H引脚做捕捉输入
	ECCP_Capture_Mode_Config(ECCP5_SFR,ECCP_CHANNEL_1,ECCP_CAP_FALLING_EDGE);			//CCP5CH1下降沿捕捉
	ECCP_Capture_Mode_Config(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_CAP_RISING_EDGE);			//CCP5CH2上升沿捕捉

	ATIM_X_Updata_Output_Ctl(T5_SFR,TRUE);												//立即更新以Tx为时基的输出控制寄存器
	ATIM_X_Updata_Enable(T5_SFR,TRUE);													//允许以Tx为时基占空比、周期、预分频、更新计数器、输出控制寄存器更新
	ATIM_X_Work_Mode_Config(T5_SFR,ATIM_TIMER_MODE);									//定时模式
	ATIM_X_Set_Counter(T5_SFR,0);														//T5计数值
	ATIM_X_Set_Prescaler(T5_SFR,15);													//预分频器 1:1
	ATIM_X_Postscaler_Config(T5_SFR,ATIM_POSTSCALER_DIV_1);								//后分频比为1:1
	ATIM_X_Counter_Mode_Config(T5_SFR,ATIM_COUNT_UP_OF);							//中心对齐的PWM信号,向上-向下计数，上溢时产生中断标志
	ATIM_X_Clock_Config(T5_SFR,ATIM_SCLK);												//选用SCLK时钟为Tx工作时钟
	ATIM_X_Updata_Immediately_Config(T5_SFR,TRUE);										//立即更新控制位
	ATIM_X_Cmd(T5_SFR,TRUE);															//Tx启动控制使能

	while(1)
	{
		Delay(0xFFFF);
		UP_PLUSE_WIDTH	= ECCP_Get_Capture_Result(ECCP5_SFR,ECCP_CHANNEL_1);	//通道1测量PWM的正脉宽
		if((UP_PLUSE_WIDTH >3700)&&(UP_PLUSE_WIDTH<3800))
			GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_RESET);//如果脉宽符合500us，翻转STATUS
		Delay(0xFFFF);
		DUTY	= ECCP_Get_Capture_Result(ECCP5_SFR,ECCP_CHANNEL_2);	//通道2测量PWM的周期
		if((DUTY >7400)&&(DUTY<7600))
			GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_SET); //如果周期值符合1K频率，翻转STATUS
	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
