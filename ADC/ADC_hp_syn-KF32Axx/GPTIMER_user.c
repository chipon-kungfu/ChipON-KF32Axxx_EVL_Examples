/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */
#include "system_init.h"
#include "GPTIMER_user.h"
#ifndef GPTIMER_C_
#define GPTIMER_C_

void Init_T2()
{
	 //	T1触发使能配置
	TIM_Reset(T2_SFR);//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(T2_SFR,TRUE);//立即更新控制   TxUR, 立刻强制更新生效此刻的寄存器
	GPTIM_Updata_Enable(T2_SFR,TRUE);//配置更新使能   TXUDEN
	GPTIM_Work_Mode_Config(T2_SFR,GPTIM_TIMER_MODE);//定时模式选择  TxCS
	GPTIM_Set_Counter(T2_SFR,0);//定时器计数值
	GPTIM_Set_Period(T2_SFR,0X1000);//定时器周期值
	GPTIM_Set_Prescaler(T2_SFR,0x0F00);//定时器预分频值
	GPTIM_Counter_Mode_Config(T2_SFR,GPTIM_COUNT_UP_DOWN_OUF);//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(T2_SFR,GPTIM_HFCLK);//选用HFCLK时钟
	GPTIM_Cmd(T2_SFR,TRUE);//定时器启动控制使能
}

#endif /* GPTIMER_C_ */
