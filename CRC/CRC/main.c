/****************************************************************************************
  *
  * 文件名  CRC.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了CRC模块应用例程
  *
 ****************************************************************************************/
#include "system_init.h"
#include "stdio.h"
#include "Usart.h"
#include "CRC.h"
#define BUFFER_SIZE   8

uint32_t aDataBuffer[BUFFER_SIZE] =
  {
    0x00001021, 0x32732252, 0xd58d3653, 0xc7bc48c4, 0x40257046, 0x00a130c2, 0x32d24235, 0x918881a9
  };

void GPIO_USART1();//USART1引脚重映射
void Usart_line_feed(USART_SFRmap *USARTx);//发送换行符

uint32_t CRCValue;  //CRC结果
uint8_t Data[4];    //发送数组

//主频时120M 1ms延时函数
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=700;
		while(j--);
	}

}

/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程实现CRC_32校验，例程输入原始数据aDataBuffer[8],
	* 校验结果放在CRCValue中，将CRCValue转换成8位数据通过串口1发出
	* 校验结果还可以通过fprintf函数通过串口1发送*/

	 unsigned char i;
	//系统时钟120M,外设高频时钟16M
	SystemInit();
	//配置USART1引脚重映射，PB0-RX，PB1-TX0
	GPIO_USART1();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART1_SFR);

	//CRC配置，CRC_32，初始值0xFFFFFFFF
	CRC_Init_Config();
	//获取CRC计算结果
	CRCValue=CRC_CalcBlockCRC(aDataBuffer,BUFFER_SIZE);

	//把32bit数据转换成8bit数据，低8位在数组头，便于串口发送
    for(i=0;i<4;i++)
    {
    	Data[i]=CRCValue>>8*(i%4);
    }

	while(1)
	{
		delay_ms(1000);
		//CRC该例程正确校验值为0x76484696
//		USART_Send(USART1_SFR, Data, 4);//串口发送函数
//		Usart_line_feed(USART1_SFR);//发送换行符
		fprintf(USART1_STREAM,"CRC-32/MPEG-2 Result is : 0x%X \r\n",CRCValue);
	}
}
/**
  * 描述    USART1引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART1()
{
	/* 端口重映射AF5 */
	//USART1_RX		PB0
	//USART1_TX0	PB1
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_0, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_1, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);	  //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_1, GPIO_RMP_AF5_USART1);     //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_0, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_1, TRUE);                  //配置锁存
}
/**
  * 描述   串口发送换行符
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void Usart_line_feed(USART_SFRmap *USARTx)
{
	USART_SendData(USARTx,0x0D);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	USART_SendData(USARTx,0x0A);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
}
/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	  while(1)
	  {
		  ;
	  }
}
