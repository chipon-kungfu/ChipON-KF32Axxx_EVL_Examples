/**
  ******************************************************************************
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */
#include "system_init.h"


void Goto_StandBy()
{
	BKP_Write_And_Read_Enable(TRUE); 					 //备份域读写使能
	PM_CTL0 |= PM_CTL0_IOLATCH;                          //锁存IO状态
	PM_Low_Power_Mode_Config(PM_LOW_POWER_MODE_STANDBY); //进入StandBy模式
	PM_External_Wakeup_Pin_Enable(PM_PIN_WKP5,TRUE);     //使能WakeUp5引脚
	PM_External_Wakeup_Edge_Config(PM_PIN_WKP5 , PM_TRIGGER_FALL_EDGE); //下降沿触发，但该脚需要配置为模拟通道

	PM_Clear_External_Wakeup_Pin_Flag(PM_WAKEUP_EXTERNAL_PIN_WKP5);

    PM_CAL2 |= ((PM_STAC & 0x0000FFFF)| 0x8000);
    PM_CTL0 |= 0x800;

	BKP_Write_And_Read_Enable(FALSE);

	// Clear all of interrupt flags
	INT_EIF2 = 0;
	INT_EIF1 = 0;
	INT_EIF0 = 0;

	asm("SLEEP" );
	asm("NOP");
}
