/**
  ******************************************************************************
  * 文件名 canhl.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了CAN模块相关的配置与发送功能，包括
  *          + CAN引脚重映射
  *          + CAN中断配置
  *          + CAN发送
  *          + CAN初始化
  ******************************************************************************/
#include "system_init.h"
#include "canhl.h"
#define 	xCAN_ACR    (uint32_t)0x00000000;  //验收代码
#define 	xCAN_MSK 	(uint32_t)0xffffffff;  //验收屏蔽
#define     HARDWARE_ARBITRATION   0

volatile uint8_t CAN_TX_FLAG=0; 	//发送标志
uint32_t SENDID = 0x211;			    //ID
uint32_t SENDID1 = 0x111;			    //ID
uint8_t  SendData[8]={'A','B','C','D','E','F','G','H'};  //发送数据
volatile uint8_t  ReceiveData[8]={0};	    //接收数据

/*============================================================================
 *void xGPIO_CAN()
------------------------------------------------------------------------------
描述		: can引脚重定义
输入		: 无
输出		: 无
备注		:
============================================================================*/
void xGPIO_CAN()
{
#if 1
	GPIO_InitTypeDef GPIO_InitStructure;

	/*配置CAN引脚类型为复用模式，开启对应端口时钟*/
	GPIO_InitStructure.m_Mode = GPIO_MODE_RMP;
	GPIO_InitStructure.m_Speed = GPIO_HIGH_SPEED;
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;
	GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
	GPIO_InitStructure.m_Pin = GPIO_PIN_MASK_13|GPIO_PIN_MASK_14;
	GPIO_Configuration(GPIOD_SFR,&GPIO_InitStructure);
	/*配置引脚映射功能为CAN模式*/
	PCLK_CTL0_Peripheral_Clock_Enable(PCLK_CTL0_GPIODCLKEN,TRUE);
	GPIO_Pin_RMP_Config(GPIOD_SFR,GPIO_Pin_Num_13,GPIO_RMP_AF9_CAN1);
	GPIO_Pin_RMP_Config(GPIOD_SFR,GPIO_Pin_Num_14,GPIO_RMP_AF9_CAN1);
#else


	GPIO_InitTypeDef GPIO_InitStructure;

	/*配置CAN引脚类型为复用模式，开启对应端口时钟*/
	GPIO_InitStructure.m_Mode = GPIO_MODE_RMP;
	GPIO_InitStructure.m_Speed = GPIO_HIGH_SPEED;
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;
	GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
	GPIO_InitStructure.m_Pin = GPIO_PIN_MASK_4|GPIO_PIN_MASK_5;
	GPIO_Configuration(GPIOA_SFR,&GPIO_InitStructure);
	/*配置引脚映射功能为CAN模式*/
	PCLK_CTL0_Peripheral_Clock_Enable(PCLK_CTL0_GPIODCLKEN,TRUE);
	GPIO_Pin_RMP_Config(GPIOA_SFR,GPIO_Pin_Num_4,GPIO_RMP_AF9_CAN1);
	GPIO_Pin_RMP_Config(GPIOA_SFR,GPIO_Pin_Num_5,GPIO_RMP_AF9_CAN1);
#endif

}
/*============================================================================
 *void xInit_CAN(CAN_SFRmap* CANx,uint8_t Bdrt,uint32_t MODE)
------------------------------------------------------------------------------
描述		: can模块初始化
输入		: 1.CANx: 指向CAN内存结构的指针，取值为CAN0_SFR~CAN3_SFR
		  2.Bdrt  波特率，见canhl.h定义
		  3.MODE  工作模式：CAN_MODE_NORMAL CAN_MODE_SILENT CAN_MODE_LOOPBACK CAN_MODE_SILENT_LOOPBACK
输出		: 无
备注		:
============================================================================*/
void xInit_CAN(CAN_SFRmap* CANx,uint8_t Bdrt,uint32_t MODE)
{
	CAN_InitTypeDef CAN_InitStructure;
	/* CAN时钟使能，复位禁能 */

	CAN_Reset(CANx);
	CAN_InitStructure.m_Acceptance = xCAN_ACR;                    //验收滤波
	CAN_InitStructure.m_AcceptanceMask = xCAN_MSK;		          //验收屏蔽
	CAN_InitStructure.m_WorkSource = CAN_SOURCE_HFCLK_DIV_2;      //CAN时钟内部高频

	if(Bdrt==CAN_BAUDRATE_125K)
	{
		CAN_InitStructure.m_BaudRate = 7;					  //时钟分频  1M
	}
	else if(Bdrt==CAN_BAUDRATE_250K)
	{
		CAN_InitStructure.m_BaudRate = 3;					  //时钟分频  2M
	}
	else if(Bdrt==CAN_BAUDRATE_500K)
	{
		CAN_InitStructure.m_BaudRate = 1;					  //时钟分频  4M
	}
	else if(Bdrt==CAN_BAUDRATE_1M)
	{
		CAN_InitStructure.m_BaudRate = 0;					  //时钟分频 8M
	}
	//TSEG1设置与TSEG2设置比值75%采样率，采样点8个，采样率=（Seg1+Sync）/(Seg1+Seg2+Sync)*100%
	CAN_InitStructure.m_TimeSeg1 = 4;						   //TSEG1
	CAN_InitStructure.m_TimeSeg2 = 1;						   //TSEG2
	CAN_InitStructure.m_BusSample = CAN_BUS_SAMPLE_3_TIMES;	   //采样点次数
	CAN_InitStructure.m_SyncJumpWidth = 1;					   //同步跳转宽度
	CAN_InitStructure.m_Enable = TRUE;						   //使能
	CAN_InitStructure.m_Mode = MODE;				           //模式
	CAN_Configuration(CAN1_SFR,&CAN_InitStructure);			   //写入配置
}
/*============================================================================
 *void xINT_CAN(CAN_SFRmap* CANx)
------------------------------------------------------------------------------
描述		: can中断配置
输入		: 1.CANx: 指向CAN内存结构的指针，取值为CAN0_SFR~CAN3_SFR
输出		: 无
备注		:
============================================================================*/
void xINT_CAN(CAN_SFRmap* CANx)
{

	/* 开启发送、错误、仲裁中断 */
    #if HARDWARE_ARBITRATION
        CAN_Set_INT_Enable(CANx,CAN_INT_TRANSMIT|CAN_INT_BUS_ERROR,TRUE);
    #else
	    CAN_Set_INT_Enable(CANx,CAN_INT_TRANSMIT|CAN_INT_BUS_ERROR|CAN_INT_ARBITRATION_LOST,TRUE);
    #endif 

	/* 开启接收中断 */
	CAN_Set_INT_Enable(CANx,CAN_INT_RECEIVE,TRUE);
	/* 开启包溢出中断 */
//	CAN_Set_INT_Enable(CANx,CAN_INT_DATA_OVERFLOW,TRUE);
	/* CAN中断配置 */
	INT_Interrupt_Priority_Config(INT_CAN1,1,2);						//CAN抢占优先级4,子优先级0
	INT_Clear_Interrupt_Flag(INT_CAN1);									//CAN清中断标志
	INT_Interrupt_Enable(INT_CAN1,TRUE);								//CAN中断使能

	if(INT_EIE2 & 0x8)
	{
		GPIO_Toggle_Output_Data_Config(GPIOH_SFR,GPIO_PIN_MASK_13);
	}

	delay_ms(0xFF);
	INT_All_Enable (TRUE);

	if(INT_EIE2 & 0x8)
	{
		GPIO_Toggle_Output_Data_Config(GPIOH_SFR,GPIO_PIN_MASK_13);
	}
}

/*============================================================================
 *void CAN_Transmit_DATA(CAN_SFRmap* CANx, uint8_t *data , uint8_t lenth)
------------------------------------------------------------------------------
描述		: 数据发送加载
输入		: 1.要初始化的CAN标号	CAN0_SFR、CAN1_SFR、CAN2_SFR
		  2.ID
		  2.发送数据数组地址
		  3.发送数据长度
		  4.帧类型        CAN_DATA_FRAME数据帧                     CAN_REMOTE_FRAME远程帧
		  5.帧格式        CAN_FRAME_FORMAT_SFF标准帧     CAN_FRAME_FORMAT_EFF扩展帧
输出		: CAN_ERROR_BUFFERFULL  CAN_ERROR_NOERROR
备注		:
============================================================================*/
CAN_ErrorT CAN_Transmit_DATA(CAN_SFRmap* CANx,   //CAN通道
						uint32_t  TargetID,//ID
						uint8_t*  data ,   //数据指针
						uint8_t   lenth,   //长度
						uint32_t  MsgType, //帧类型        CAN_DATA_FRAME//数据帧           CAN_REMOTE_FRAME//远程帧
						uint32_t  RmtFrm   //帧格式        CAN_FRAME_FORMAT_SFF//标准帧     CAN_FRAME_FORMAT_EFF//扩展帧
						)
{
	CAN_ErrorT x;
	uint8_t i;
	CAN_MessageTypeDef	CAN_MessageStructure;

	CAN_MessageStructure.m_FrameFormat = RmtFrm;             	 //帧格式
	CAN_MessageStructure.m_RemoteTransmit = MsgType;		 	 //帧类型
	if(RmtFrm==CAN_FRAME_FORMAT_SFF)//标准帧
	{
		CAN_MessageStructure.m_StandardID = TargetID;			 //标准帧ID
		CAN_MessageStructure.m_ExtendedID = 0;			         //扩展帧ID
	}
	else if(RmtFrm==CAN_FRAME_FORMAT_EFF)//扩展帧
	{
		CAN_MessageStructure.m_StandardID = 0;					 //标准帧ID
		CAN_MessageStructure.m_ExtendedID = TargetID;			 //扩展帧ID
	}
	if(lenth>8)
	{
		lenth=8;
	}
	CAN_MessageStructure.m_DataLength = lenth;			    	 //长度

	for(i=0;i<lenth;i++)
	{
		CAN_MessageStructure.m_Data[i] = data[i];				 //数据
	}
	/* 发送缓冲器空 */
	if((!CAN_Get_Transmit_Status(CANx,CAN_TX_BUFFER_STATUS)))
	{
		x=CAN_ERROR_BUFFERFULL;
		return x;
	}
	/* 转载数据到发送缓冲器 */
	CAN_Transmit_Message_Configuration(CANx,&CAN_MessageStructure);


    #if HARDWARE_ARBITRATION
        CANx->CTLR = CANx->CTLR | 0x100;    //硬件自动重发
    #else
	    CANx->CTLR = CANx->CTLR | 0x300;	// 单次发送 
    #endif 


	x=CAN_ERROR_NOERROR;
	return x;
}

