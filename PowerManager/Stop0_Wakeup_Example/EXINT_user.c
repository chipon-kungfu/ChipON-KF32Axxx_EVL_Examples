/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */
#include "system_init.h"


void PMC_Error_Init()
{
	EINT_InitTypeDef EINT_19To17;

	INT_External_Struct_Init(&EINT_19To17);
	EINT_19To17.m_Mask = TRUE; 	//EXIT enable or disable.
	EINT_19To17.m_Rise = TRUE; 	//EXIT rising.
	EINT_19To17.m_Line = INT_EXTERNAL_INTERRUPT_19; //EXIT num.
	EINT_19To17.m_Fall = FALSE;    //EXIT falling.
	INT_External_Configuration (&EINT_19To17);

	INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_19);
	BKP_Write_And_Read_Enable(TRUE);
	/* Enable PMC error interrupt. */
	PM_CTL2  |= (1 << 19);
	BKP_Write_And_Read_Enable(FALSE);
	/* Configuration EINT Interrupt Priority */
	INT_Interrupt_Priority_Config(INT_EINT19TO17, 0, 0);
	/* Enable EINT Interrupt. */
	INT_Interrupt_Enable(INT_EINT19TO17,TRUE);
	/* Clear EINT interrupt flag. */
	INT_Clear_Interrupt_Flag(INT_EINT19TO17);
	/*Enable Global INT */
	INT_All_Enable (TRUE);
}

