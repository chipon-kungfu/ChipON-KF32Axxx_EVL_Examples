/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: CCPx_Compare_Mode
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了CCP比较模式应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  CCPx 比较功能GPIO重映配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *     GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  *    PinRemap: 引脚重映射选择
  * 返回  无。
  */
void CCPx_COMPARE_GPIO_Init(GPIO_SFRmap* GPIOx,uint16_t GpioPin,uint8_t PinRemap)
{
	 static uint16_t GpioPinNum;


//	//////////////////////////////////////////////////////////////
	GPIO_Write_Mode_Bits(GPIOx,GpioPin,GPIO_MODE_RMP);  //配置GPIO为重映射功能

	if(GpioPin==GPIO_PIN_MASK_0)
	{
		GpioPinNum =GPIO_Pin_Num_0;
	}else if(GpioPin==GPIO_PIN_MASK_1)
	{
		GpioPinNum =GPIO_Pin_Num_1;
	}else if(GpioPin==GPIO_PIN_MASK_2)
	{
		GpioPinNum =GPIO_Pin_Num_2;
	}else if(GpioPin==GPIO_PIN_MASK_3)
	{
		GpioPinNum =GPIO_Pin_Num_3;
	}else if(GpioPin==GPIO_PIN_MASK_4)
	{
		GpioPinNum =GPIO_Pin_Num_4;
	}else if(GpioPin==GPIO_PIN_MASK_5)
	{
		GpioPinNum =GPIO_Pin_Num_5;
	}else if(GpioPin==GPIO_PIN_MASK_6)
	{
		GpioPinNum =GPIO_Pin_Num_6;
	}else if(GpioPin==GPIO_PIN_MASK_7)
	{
		GpioPinNum =GPIO_Pin_Num_7;
	}else if(GpioPin==GPIO_PIN_MASK_8)
	{
		GpioPinNum =GPIO_Pin_Num_8;
	}else if(GpioPin==GPIO_PIN_MASK_9)
	{
		GpioPinNum =GPIO_Pin_Num_9;
	}else if(GpioPin==GPIO_PIN_MASK_10)
	{
		GpioPinNum =GPIO_Pin_Num_10;
	}else if(GpioPin==GPIO_PIN_MASK_11)
	{
		GpioPinNum =GPIO_Pin_Num_11;
	}else if(GpioPin==GPIO_PIN_MASK_12)
	{
		GpioPinNum =GPIO_Pin_Num_12;
	}else if(GpioPin==GPIO_PIN_MASK_13)
	{
		GpioPinNum =GPIO_Pin_Num_13;
	}else if(GpioPin==GPIO_PIN_MASK_14)
	{
		GpioPinNum =GPIO_Pin_Num_14;
	}else if(GpioPin==GPIO_PIN_MASK_15)
	{
		GpioPinNum =GPIO_Pin_Num_15;
	}

	GPIO_Pin_RMP_Config(GPIOx,GpioPinNum,PinRemap);  //配置GPIO重映射外设功能
}


/**
  * 描述  CCPx比较功能初始化配置参数。
  * 输入 : CCPx: 指向CCP或通用定时器内存结构的指针，
  *               取值CCP0_SFR/CCP1_SFR/CCP2_SFR/CCP3_SFR/CCP4_SFR/
  *               CCP18_SFR/CCP19_SFR/CCP20_SFR/CCP21_SFR/CCP22_SFR/CCP23_SFR。
  *    Peripheral: 外设CCPx编号，取值范围为： 枚举类型InterruptIndex中的CCPx外设中断向量编号。
  *
  *
  * 返回  无。
  */
void CCPx_Compare_Config(CCP_SFRmap* CCPx,InterruptIndex Peripheral)
{
///配置CCPx的比较功能，并设置比较匹配后四个通道输出电平，初始化CCPx周期为0XFFFF,预分频为16，并使能中断

	CCP_CompareInitTypeDef CCP_Compare_InitStructure;
	CCP_Compare_Struct_Init(&CCP_Compare_InitStructure);
	 // 通道编号，     取值范围满足“CCP通道”的宏。
	CCP_Compare_InitStructure.m_Channel=CCP_CHANNEL_1|CCP_CHANNEL_2|CCP_CHANNEL_3|CCP_CHANNEL_4;
	CCP_Compare_InitStructure.m_CompareMode=CCP_CMP_ACTIVE_LEVEL;     //捕捉/比较器模式选择，  取值满足宏CHECK_CCP_CMP_MODE的约定条件。
	CCP_Compare_InitStructure.m_CompareValue=0X9FFF;                  //设置与TX比较的值，     取值范围为16位数据。
	CCP_Compare_Configuration(CCPx,&CCP_Compare_InitStructure);




	TIM_Reset(CCPx);//定时器外设复位，使能外设时钟
	CCP_Compare_Mode_Config(CCPx,CCP_CHANNEL_1,CCP_CMP_TOGGLE_LEVEL);		//匹配时电平翻转					//CCP PWM功能通道模式
	CCP_Compare_Mode_Config(CCPx,CCP_CHANNEL_2,CCP_CMP_TOGGLE_LEVEL);		//匹配时电平翻转						//CCP PWM功能通道模式
	CCP_Compare_Mode_Config(CCPx,CCP_CHANNEL_3,CCP_CMP_ACTIVE_LEVEL);		//输出高电平							//CCP PWM功能通道模式
	CCP_Compare_Mode_Config(CCPx,CCP_CHANNEL_4,CCP_CMP_INACTIVE_LEVEL);		//输出低电平							//CCP PWM功能通道模式


	GPTIM_Updata_Immediately_Config(CCPx,TRUE);							    //立即更新控制
	GPTIM_Updata_Enable(CCPx,TRUE);										    //配置更新使能
	GPTIM_Work_Mode_Config(CCPx,GPTIM_TIMER_MODE);						    //定时模式选择
	GPTIM_Set_Counter(CCPx,0);											    //定时器计数值
	GPTIM_Set_Period(CCPx,0XFFFF);										    //定时器周期值
	GPTIM_Set_Prescaler(CCPx,15);										    //定时器预分频值
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_OF);					    //向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(CCPx,GPTIM_SCLK);								    //选用SCLK时钟
	GPTIM_Overflow_INT_Enable(CCPx,TRUE);								    //计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);								    //外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);								    //清中断标志
	GPTIM_Cmd(CCPx,TRUE);												    //定时器启动控制使能
	INT_All_Enable(TRUE);												    //全局中断使能

}


/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}



//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现比较模式的配置功能。
	 * 例程中使用CCP0模块实现比较模式，PE1 为CCP0_CHANNEL_1 PE2为CCP0_CHANNEL_2 PA2为CCP0_CHANNEL_3 PA2为CCP0_CHANNEL_3，
	 * 当在比较周期匹配后产生中断，然后再在 中断里再次设置CCP0_CH1通道的比较值，判断比较值后把PB13进行电平翻转。
	 * PB13对应D4灯的控制,挂示波器可以看到PB13输出60HZ 占空比50%的信号
	 */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化
	//初始化PB13输出高电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);

	////IO重映射通道
	//PE1 为CCP0_CHANNEL_1 PE2为CCP0_CHANNEL_2 PA2为CCP0_CHANNEL_3 PA2为CCP0_CHANNEL_3
	GPIO_Reset(GPIOE_SFR);                //复位GPIOE，并打开GPIOE时钟
	GPIO_Reset(GPIOA_SFR);                //复位GPIOA，并打开GPIOA时钟
	CCPx_COMPARE_GPIO_Init(GPIOE_SFR,GPIO_PIN_MASK_1,GPIO_RMP_AF1_T0);
	CCPx_COMPARE_GPIO_Init(GPIOE_SFR,GPIO_PIN_MASK_2,GPIO_RMP_AF1_T0);
	CCPx_COMPARE_GPIO_Init(GPIOA_SFR,GPIO_PIN_MASK_2,GPIO_RMP_AF1_T0);
	CCPx_COMPARE_GPIO_Init(GPIOA_SFR,GPIO_PIN_MASK_3,GPIO_RMP_AF1_T0);
	//配置CCP0比较模式并开启中断
	CCPx_Compare_Config(CCP0_SFR,INT_T0);
    while(1)
    {
    }

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

