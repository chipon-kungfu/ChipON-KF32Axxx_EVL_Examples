/*
 * dma.h
 *
 *  Created on: 2019-11-5
 *      Author: charles_cai
 */

#ifndef DMA_H_
#define DMA_H_
/***
 *         ADC0对应DMA0的 CH5,ADC1对应DMA1的CH5,ADC2对应DMA1的CH7
 */

#define DMA_ADC_CH      DMA_CHANNEL_7
#define DMA_UNIT        DMA1_SFR
#define DMA_INTx        INT_DMA1
#define ADC_Data_Addr   (uint32_t) &ADC2_DATA

void Init_dma() ;
void cfg_dma_intrupt();
#endif /* DMA_H_ */
