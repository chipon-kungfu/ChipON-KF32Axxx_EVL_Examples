/**
  ******************************************************************************
  * 文件名 lin_master.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了异步串口相关配置函数，包括
  *          + 串口发送函数
  *          + 标准校验和
  *          + LIN主机发送
  *          + 串口配置，使能LIN唤醒
  *          + 串口接收中断使
  *          + 基本定时器配置
  ******************************************************************************/
#include "system_init.h"
#include "lin_master.h"
volatile uint8_t Rev_Temp;		 //接收数据（清接收中断标志）
uint8_t l_Tx_data[10];           //发送数据
uint8_t xxx;             		 //计数

void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//串口发送函数
uint8_t Check_Sum(uint8_t* Databuf,uint32_t point,uint32_t length);  //标准校验和
void LIN_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//LIN主机发送
void USART_LIN_config(USART_SFRmap *USARTx);//串口配置，使能LIN唤醒
void USART_LIN_INT_config(USART_SFRmap *USARTx,InterruptIndex Peripheral);//串口接收中断使
void BASIC_TIMER_Config(BTIM_SFRmap* BTIMx, InterruptIndex Peripheral);//定时器配置
/**
  * 描述   串口发送
  * 输入   USARTx:   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Databuf：   指向发送数据的指针
  *      length：      发送的长度
  * 返回   无
  */
void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length)
{
	uint32_t i;
	for(i=0;i<length;i++)
	{
		//串口发送
		USART_SendData(USARTx,Databuf[i]);
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	}
}

/**
  * 描述   标准校验和
  * 输入   Databuf:  指向校验数据的指针
  *      point：        校验数据起始位置
  *      length：      校验长度=length-point
  * 返回   校验和
  */
uint8_t Check_Sum(uint8_t* Databuf,uint32_t point,uint32_t length)
{
	 uint8_t i=0;
	 uint16_t check_sum;
	 uint8_t check_sum_return;
	 check_sum=0;
	 for(i=point;i<length;i++)   //校验和（标准校验）
	 {

		 check_sum+=Databuf[i];
		 if(check_sum>0xFF)
		 {
			 check_sum = (check_sum % 0x100) +1;
		 }
	 }
	 check_sum_return = (uint8_t) check_sum;


	 return (~check_sum_return);
}

/**
  * 描述   LIN主机发送
  * 输入   USARTx:   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Databuf：   指向发送数据的指针
  *      length：      发送的长度
  * 返回   无
  */
void LIN_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length)
{
	uint32_t i;

//	USART_SendData(USARTx,0x55);							    //设置同步码
	USART_Send_Blank_Enable(USARTx,TRUE);						//使能发送间隔
	BTIM_Clear_Overflow_INT_Flag (T15_SFR);						//清T15溢出中断标志位
	BTIM_Set_Counter(T15_SFR,0);								//定时器计数值清0
	BTIM_Cmd(T15_SFR,TRUE);										//定时器启动控制使能
	while(!BTIM_Get_Overflow_INT_Flag(T15_SFR));				//等待定时
	USART_Send_Blank_Enable(USARTx,FALSE);						//清发送间隔
	BTIM_Cmd(T15_SFR,FALSE);									//定时器关闭
	USART_SendData(USARTx,0x55);							    //设置同步码

	while(!USART_Get_Transmitter_Empty_Flag(USARTx));           //等待同步码发送完毕发

	//发送ID & 数据
	for(i=0;i<length;i++)
	{
		//串口发送
		USART_SendData(USARTx,Databuf[i]);
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	}
}



/**
  * 描述   串口异步全双工配置(默认8bit收发使能  全双工 9600)，使能LIN唤醒
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void USART_LIN_config(USART_SFRmap *USARTx)
{
	USART_InitTypeDef USART_InitStructure;

	USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode=USART_MODE_FULLDUPLEXASY;                        //全双工
    USART_InitStructure.m_TransferDir=USART_DIRECTION_FULL_DUPLEX;              //传输方向
    USART_InitStructure.m_WordLength=USART_WORDLENGTH_8B;                       //8位数据
    USART_InitStructure.m_StopBits=USART_STOPBITS_1;                            //1位停止位
    USART_InitStructure.m_BaudRateBRCKS=USART_CLK_HFCLK;                        //内部高频时钟作为 USART波特率发生器时钟
    USART_InitStructure.m_BRAutoDetect=USART_ABRDEN_OFF;                        //自动检测波特率关闭
    /* 波特率 =Fck/(16*z（1+x/y)) 外设时钟内部高频16M*/
    //4800    z:208    x:0    y:0
    //9600    z:104    x:0    y:0
    //19200   z:52     x:0    y:0
    //115200  z:8      x:1    y:13
    //波特率9600
    USART_InitStructure.m_BaudRateInteger=104;         //USART波特率整数部分z，取值为0~65535
    USART_InitStructure.m_BaudRateNumerator=0;         //USART波特率小数分子部分x，取值为0~0xF
    USART_InitStructure.m_BaudRateDenominator=0;       //USART波特率小数分母部分y，取值为0~0xF

	USART_Reset(USARTx);                                       //USARTx复位
	USART_Configuration(USARTx,&USART_InitStructure);          //USARTx配置
	USART_WeakUP_Enable(USARTx,TRUE);                          //自动唤醒使能，等待下降沿
	USART_Clear_Transmit_BUFR_INT_Flag(USARTx);                //USARTx发送BUF清零
	USART_Transmit_Data_Enable(USARTx,TRUE);				   //使能发送
	USART_RESHD_Enable (USARTx, TRUE);						   //使能RESHD位
	USART_Cmd(USARTx,TRUE);                                    //USARTx使能
}

/**
  * 描述   串口接收中断配置
  * 输入   USARTx:指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Peripheral:外设或内核中断向量编号，取值范围为：
  *                 枚举类型InterruptIndex中的外设中断向量编号
  * 返回   无
  */
void USART_LIN_INT_config(USART_SFRmap *USARTx,InterruptIndex Peripheral)
{
	USART_RDR_INT_Enable(USARTx,TRUE);//接收中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);
	INT_All_Enable(TRUE);
}


/*/*描述基本定时器初始化配置
* 输入  BTIMx:取值为T14_SFR/T15_SFR
*    Peripheral：取值为INT_T14/INT_T15
*/
void BASIC_TIMER_Config(BTIM_SFRmap* BTIMx, InterruptIndex Peripheral)
{
	//定时器时钟源选用SCLK  设周期为16000 设预分频2分频 16M 定时20ms进一次中断

	TIM_Reset(BTIMx);												//定时器外设复位，使能外设时钟
	BTIM_Updata_Immediately_Config(BTIMx,TRUE);						//立即更新控制
	BTIM_Updata_Enable(BTIMx,TRUE);									//配置更新使能
	BTIM_Work_Mode_Config(BTIMx,BTIM_TIMER_MODE);					//定时模式选择
	BTIM_Set_Counter(BTIMx,0);										//定时器计数值
	BTIM_Set_Period(BTIMx,40000);									//定时器周期值80000
	BTIM_Set_Prescaler(BTIMx,7);								    //定时器预分频值1+1=2
	BTIM_Counter_Mode_Config(BTIMx,BTIM_COUNT_UP_OF);				//向上计数,上溢产生中断标志
	BTIM_Clock_Config(BTIMx,BTIM_HFCLK);							//选用HFCLK时钟
	INT_Interrupt_Priority_Config(Peripheral,4,0);					//抢占优先级4,子优先级0
	BTIM_Overflow_INT_Enable(BTIMx,TRUE);							//计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);						    //外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);							//清中断标志
	BTIM_Cmd(BTIMx,TRUE);											//定时器启动控制使能
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);					//中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);											//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断
}
