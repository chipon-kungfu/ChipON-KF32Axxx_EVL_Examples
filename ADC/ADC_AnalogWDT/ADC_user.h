/*
 * adc.h
 *
 *  Created on: 2019-11-5
 *      Author: charles_cai
 */

#ifndef ADC_H_
#define ADC_H_
extern unsigned int buf_1[32];
void Init_adc0();
extern void Delay(volatile uint32_t cnt);
void Init_ADC_Analog_Wd(uint32_t ADC_WDCH_N);
void Init_adc0_INT();
#endif /* ADC_H_ */
