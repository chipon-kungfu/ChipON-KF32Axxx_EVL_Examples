/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: Breathing_LED
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了通用定时器呼吸灯应用
 *
 *
 ****************************************************************************************/
#include "system_init.h"

volatile uint8_t direc_flag;     //调整占空比变化方向变量
volatile uint32_t PWM_VLAUE;     //调整占空比大小变量
volatile uint32_t HUXI_COUNT;    //调整占空比变化速度变量


/**
  * 描述  通用定时器GPTIMx 初始化配置。
  * 输入  GPTIMx:取值为T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/ T18_SFR/T19_SFR/T20_SFR/T21_SFR/T22_SFR/T23_SFR。
  *  Peripheral：取值为INT_T0/INT_T1/INT_T2/INT_T3/INT_T4/INT_T18/INT_T19/INT_T20/INT_T21/INT_T22/INT_T23
  *
  * 返回  无。
  */
void GENERAL_TIMER_SINGLE_Tx_Config(GPTIM_SFRmap* GPTIMx,InterruptIndex Peripheral)
{
	//定时器的时钟源选主时钟120M，经过定时器60分频，0.5us执行计数一次，计数100次50us进一次中断
	TIM_Reset(GPTIMx);																//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);									//立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);												//配置更新使能
	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);								//定时模式选择
	GPTIM_Set_Counter(GPTIMx,0);													//定时器计数值
	GPTIM_Set_Period(GPTIMx,100);											    	//定时器周期值100
	GPTIM_Set_Prescaler(GPTIMx,59);													//定时器预分频值59+1=60
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_DOWN_OUF);						//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_SCLK);											//选用SCLK时钟
	INT_Interrupt_Priority_Config(Peripheral,4,0);									//抢占优先级4,子优先级0
	GPTIM_Overflow_INT_Enable(GPTIMx,TRUE);											//计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);											//外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);											//清中断标志
	GPTIM_Cmd(GPTIMx,TRUE);															//定时器启动控制使能
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);									//中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);															//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

}

/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}

//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上D4 灯实现呼吸灯效果。
	 * 例程中使用通用定时器18 设置定时时间为50us产生一次中断，
	 *然后在中断函数里对PB13进行设置成模拟的频率为100HZ PWM信号 ，
	 *在主函数里改变其占空比大小及方向，从而实现D4呼吸灯
	 * */

	//系统时钟120M,外设高频时钟16M
	SystemInit();
	direc_flag =0;//占空比更改方向变量初始化
	PWM_VLAUE =50;//占空比大小初始化
	//初始化PB13输出高电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);
	//使用通用定时器T18作为定时及中断应用
	GENERAL_TIMER_SINGLE_Tx_Config(T18_SFR,INT_T18);   //50us进一次中断

    while(1)
    {
        	if((HUXI_COUNT >= 1000) && (direc_flag == 0))//占空比变化速度调整
        	{
        		//占空比减小
        	   HUXI_COUNT =0;
        		PWM_VLAUE++;

        		if(PWM_VLAUE >= 99)
        		{
        			PWM_VLAUE =99;
        			direc_flag = 1;//占空比更改方向
        		}
        	}

        	if((HUXI_COUNT >= 500) && (direc_flag == 1)) //占空比变化速度调整
        	{
        		//占空比增加
        		HUXI_COUNT =0;
        		PWM_VLAUE--;
        		if(PWM_VLAUE <= 10)
        	    {
        			direc_flag = 0; //占空比更改方向
        			PWM_VLAUE =10;
        	    }


        	}

    }


}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

