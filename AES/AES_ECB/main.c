/****************************************************************************************
 *
 * 文件名  main.c
 * 作  者  ChipON_AE/FAE_Group
 * 日  期  2019-10-19
 * 描  述  该文件提供了AES加密模块应用例程与软件AES加密解密
 * 
 ****************************************************************************************/
#include "system_init.h"
#include "Usart.h"
#include "AES.h"
uint8_t  AES_Data[16];//加密/解密数据
//密钥数组
uint32_t SKey_Data[4]={0x43686970,0x4F4E5F4B,0x46333241,0x3135315F}; //"Chip ON_K F32A 151_"
//明文数组
uint32_t PLay_Data[4]={0x30313233,0x34353637,0x38394142,0x43444546}; //"0123 4567 89AB CDEF"

//密钥数组
char SKey_Data_chr[16]={'p','i','h','C','K','_','N','O','A','2','3','F','_','1','5','1'}; //对应SKey_Data，按地址顺序放置数据
//明文数组
char PLay_Data_chr[16]={'3','2','1','0','7','6','5','4','B','A','9','8','F','E','D','C'}; //对应PLay_Data，按地址顺序放置数据

void GPIO_USART1();//USART1引脚重映射
void Usart_line_feed(USART_SFRmap *USARTx);//发送换行

//主频时120M 1ms延时函数
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=700;
		while(j--);
	}

}

/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程实现硬件AES加密，软件AES解密，软件AES加密功能。硬件加密后产生数据
	* 在AES_Data[16]中，作为软件解密的密文输入，解密后解密数据在AES_Data[16]中（解密后
	* 数据与PLay_Data_chr[16]数组相等），并且作为下次软件加密的输入，可通过串口输出数据*/


	//系统时钟120M,外设高频时钟16M
	SystemInit();
	//配置USART1引脚重映射，PB0-RX，PB1-TX0
	GPIO_USART1();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART1_SFR);
	//AES电码本模式配置
	AES_ECB_Configuration();

	//加载数据，默认密钥“ChipON_KF32A151_”，明文“0123456789ABCDEF” 字符
	AES_Load_Data(SKey_Data,PLay_Data);

	while(1)
	{
        //硬件加密，加密后数据在AES_Data[]中
		AES_Encry(AES_Data);
	    //发送密文
	    USART_Send(USART1_SFR,AES_Data,16);
	    Usart_line_feed(USART1_SFR);

	    //软件解密，密文输入为硬件加密后的密文AES_Data[]，解密后明文在AES_Data[]中
	    AES_soft_Decrypt(AES_Data,SKey_Data_chr);
	    //发送软件解密后明文
	    USART_Send(USART1_SFR,AES_Data,16);
	    Usart_line_feed(USART1_SFR);


	    //软件加密，明文输入为软件解密后的明文AES_Data[]，加密后数据在AES_Data[]中，与第一次硬件加密结果相等
	    AES_soft_Encrypt(AES_Data,SKey_Data_chr);
	    //发送软件加密后密文
	    USART_Send(USART1_SFR,AES_Data,16);
	    Usart_line_feed(USART1_SFR);

	    delay_ms(1000);
	}
}
/**
  * 描述    USART1引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART1()
{
	/* 端口重映射AF5 */
	//USART1_RX		PB0
	//USART1_TX0	PB1
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_0, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_1, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);	  //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_1, GPIO_RMP_AF5_USART1);     //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_0, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_1, TRUE);                  //配置锁存
}
/**
  * 描述   串口发送换行符
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void Usart_line_feed(USART_SFRmap *USARTx)
{
	USART_SendData(USARTx,0x0D);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	USART_SendData(USARTx,0x0A);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
}
/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	  while(1)
	  {
		  ;
	  }
}



