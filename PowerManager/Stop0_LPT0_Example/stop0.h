/*
 * stop0.h
 *
 *  Created on: 2022-3-20
 *      Author:
 */

#ifndef STOP0_H_
#define STOP0_H_


#include "system_init.h"
#include "vector.h"

extern const VectorEnter _start __attribute__((section(".text")));
extern const VectorEnter _startRAM __attribute__((section(".ramvector")));

int __attribute__((section(".indata"))) FLASH_CYCLE_SET(void);
void __attribute__((section(".indata"))) Goto_Stop0(void);
void  WKUP_Init(void);
void  Stop0_Init(void);

#endif /* STOP0_H_ */
