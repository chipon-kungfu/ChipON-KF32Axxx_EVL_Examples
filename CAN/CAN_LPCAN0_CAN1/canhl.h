/**
  ******************************************************************************
  * 文件名 canhl.h
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了CAN例程使用的变量与函数
  ******************************************************************************/

#ifndef _CANHL_H_
#define _CANHL_H_

//波特率
typedef enum
{
    CAN_BAUDRATE_125K = 0,   //!<baudrate 125K
    CAN_BAUDRATE_250K = 1,   //!<baudrate 250K
    CAN_BAUDRATE_500K = 2,   //!<baudrate 500K
    CAN_BAUDRATE_1M   = 3    //!<baudrate 1M
} CAN_BaudRateT;

//CAN发送函数返回值
typedef enum
{
    CAN_ERROR_NOERROR = 0,			 //!<return no error
    CAN_ERROR_BUFFERFULL=1,
} CAN_ErrorT;

//全局变量声明
extern volatile uint8_t CAN_TX_FLAG;
extern uint32_t SENDID;
extern uint8_t  SendData[8];
extern uint8_t  ReceiveData[8];

//函数声明
void xGPIO_CAN();
void xINT_CAN(CAN_SFRmap* CANx);
void xInit_CAN(CAN_SFRmap* CANx,uint8_t Bdrt,uint32_t MODE);
CAN_ErrorT CAN_Transmit_DATA(CAN_SFRmap* CANx,   //CAN通道
						uint32_t  TargetID,		 //ID
						uint8_t*  data ,   	     //数据指针
						uint8_t   lenth,   		 //长度
						uint32_t  MsgType, 		 //帧类型        CAN_DATA_FRAME数据帧                     CAN_REMOTE_FRAME远程帧
						uint32_t  RmtFrm   		 //帧格式        CAN_FRAME_FORMAT_SFF标准帧     CAN_FRAME_FORMAT_EFF扩展帧
						);
void xInit_LP_CAN(CAN_SFRmap* CANx,uint8_t Bdrt,uint32_t MODE);
extern void delay_ms(volatile uint32_t nms);
#endif /* _CANHL_H_ */
