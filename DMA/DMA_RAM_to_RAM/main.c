/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了DMA外设的配置方法，演示DMA内存到内存传输的过程
  *
  *********************************************************************
  */
#include "system_init.h"
#include "DMA_user.h"
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	char j=0;
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13, GPIO_MODE_OUT); //STATUS灯对应控制脚PB13为输出模式
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_13, Bit_RESET); //STATUS灯设置为低电平

	DMA_init(DMA0_SFR); //初始化DMA0
	DMA_Memory_To_Memory_Enable (DMA0_SFR, DMA_CHANNEL_1, TRUE);// 使能M2M寄存器可以作为软件触发的信号，开始DMA传输，软件触发后将持续进行DMA传输
	while (0 == DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_1));  //等待传输完成
	/**********逐个验算DMA传输的字符是否正确********/
	for(char i=0; i<20 ;i++)
	{
		if(buf_2[i] == buf_1[i]) //对正确数据进行计数
		j++;
	}
	if(j == 20) //如果数据正确则 STATUS灯亮
		GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_13, Bit_SET);
	else
		GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_13, Bit_RESET); //STATUS灯设置为低电平
	while(1)
	{

	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
