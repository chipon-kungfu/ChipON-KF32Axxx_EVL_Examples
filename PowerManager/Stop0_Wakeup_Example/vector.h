/*
 * vector.h
 *
 *  Created on: 2022-3-20
 *      Author:
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#define pFunc void (*Func)(void)
typedef struct
{
  pFunc;
}interruptVector;

typedef struct
{
  int  			 *value;				//auto variable by tool ,value is default sp
  interruptVector Reset_Enter;			//Enter Function,design lead function and run to main

  interruptVector NMI_Enter;
  interruptVector HardFault_Enter;
  interruptVector Rev4_Enter;
  interruptVector StackFault_Enter;
  interruptVector AriFault_Enter;
  interruptVector Intended_Rev;   		// create characteristic by linker,code write 0 here

  interruptVector interrupt[120] ;		// other
}VectorEnter;



#endif /* VECTOR_H_ */
