/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: WWDG
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了实现窗口看门狗功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
uint32_t WWDGCNT_DATA = 0;

#define WWDT_Threshold      0x1F      //窗口设定值，取值范围 0~0x3F。
#define WWDT_Enable_INT       0       //是否使能WWDG中断取值范围 0或1。

#define    KEYUSERE    GPIO_Read_Input_Data_Bit(GPIOD_SFR,GPIO_PIN_MASK_7)  //读取PD7的输入电平

//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=7000;
		while(j--);
	}

}

/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}

/**
  * 描述  GPIOx 输入初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Input_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输入模式 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;                   //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_IN;                      //初始化 GPIO方向为输入
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;                     //初始化 GPIO是否上拉 不上拉
	GPIO_InitStructure.m_PullDown = GPIO_PULLUP;                   //初始化 GPIO是否下拉 不下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);
}
/**
  * 描述  GPIOx 输入与输出初始化配置。
  * 输入 : 无
  * 返回  无。
  */
void GPIO_Init(void)
{
	GPIOInit_Input_Config(GPIOD_SFR,GPIO_PIN_MASK_7);//把PD7设为输入模式
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//把PB13设为输出，并设为高电平
}



/**
  * 描述  窗口看门狗使用窗口值配置。
  * 输入 : Threshold：设置窗口看门狗可操作区下限值取值为0~0x3F
  *
  * 预分频大小为64，最大可设262144分频
  * 返回  无。
  */
void WWDT_Config_TH(uint32_t Threshold)
{

	WWDT_Reset();                                  //复位窗口看门狗，并使能窗口看门狗时钟
	WWDT_Threshold_Config(Threshold);              //设置窗口看门狗可操作区下限值
	WWDT_Prescaler_Config(WWDT_PRESCALER_64);      //设置预分频1：64，32KHZ时钟源，频率为500HZ,2ms计数一次到40H进中断，即128ms进一次中断
	WWDT_Enable (TRUE);
#if WWDT_Enable_INT
	WWDT_INT_Enable(TRUE);                            //使能WWDT中断
    INT_Interrupt_Enable(INT_WWDT,TRUE);			  //打开WWDT中断
	INT_All_Enable (TRUE);
#endif
}


/**
  * 描述  设定下限值到溢出值前的窗口内去喂狗清零。
  * 输入 : 无
  *
  * 返回  无。
  */
void WWDT_Clear(void)
{
	WWDGCNT_DATA =WWDT_Get_Counter();            //获取窗口看门狗计数值
	if((WWDGCNT_DATA>WWDT_Threshold)&&(WWDGCNT_DATA<0x3F))
	{
		WWDT_Counter_Config(0);                    //计数寄存器清零
	}
}




//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现使用窗口看门狗功能。
	 * 例程中窗口看门狗的时钟源为内部32KHZ晶振 设置预分频1：64，频率为500HZ,
	 * WWDT_Enable_INT是否使能窗口看门狗的中断宏定义，1为使能，0为禁止
	 * 1、在使能窗口看门狗的中断2ms计数一次到40H进中断，即128ms进一次中断，需要在中断里喂狗。
	 * 2、没有使能窗口看门狗的中断时，在设定WT<6:0>的窗口值WWDT_Threshold后，在窗口看门狗计数值 0~WWDT_Threshold范围内喂狗
	 * 以及大于41H时喂狗系统都会复位。
	 * DEMO板功能在没有KEYUSERE按键按下时，系统以128ms复位一次D4灯闪烁，按下KEYUSERE键后不松手系统正常，D4灯常亮
	 *
     */

	//系统时钟120M,外设高频时钟16M
	SystemInit(); //系统时钟初始化

	OSC_LFCK_Division_Config(LFCK_DIVISION_1);		//低频工作分频选择
	OSC_LFCK_Source_Config (LFCK_INPUT_INTLF);		//选择INTLF作为LFCLK时钟
	OSC_LFCK_Enable (TRUE);							//LFCLK时钟信号允许
	OSC_INTLF_Software_Enable(TRUE);
	OSC_INTHF_Software_Enable(TRUE);				//内部高频振荡器软件使能
	//IO初始化
	GPIO_Init(); //把PD7设为输入模式，PB13设为输出，并设为高电平

	//配置设定WWDG的可操作区下限值
	WWDT_Config_TH(WWDT_Threshold);  //窗口设定值0x1F
	while(1)
    {
#if WWDT_Enable_INT
		delay_ms(200);
#else

		delay_ms(10);
		if(KEYUSERE)
		{
			WWDT_Clear();//按键按下后喂狗系统正常工作，没喂狗则系统复位
		}
#endif

    }

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

