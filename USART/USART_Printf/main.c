/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了串口异步收发应用例程参考
  *
 ****************************************************************************************/
#include "system_init.h"
#include <stdio.h>
#include "Usart.h"
//ChipON_KF32A151 ASCII
uint8_t USART_Array_Tansmit[]="ChipON_KF32A151";
volatile uint8_t Receive_flag_Uart_1; //接收标志位
volatile uint8_t Receive_flag_Uart_2; //接收标志位

void GPIO_USART1();//USART1引脚重映射
void GPIO_USART2();//USART2引脚重映射
void Usart_line_feed(USART_SFRmap *USARTx);//换行

/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程实现USART1/2的全双工异步收发功能,波特率9600，串口接收
	* 配置为中断方式，接收到0x5A后，主函数实现stdio.h中支持的printf等相关函数功能**/

	//系统时钟120M,外设高频时钟16M
	SystemInit();

	//配置USART1引脚重映射，PB0-RX，PB1-TX0
	GPIO_USART1();
	//配置USART2引脚重映射，PB15-RX，PB14-TX0(USB -> UART)
	GPIO_USART2();

	//全双工异步8bit 9600波特率
	USART_Async_config(USART1_SFR);
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);

	//串口接收中断使能
	USART_ReceiveInt_config(USART1_SFR,INT_USART1);
	//串口接收中断使能
	USART_ReceiveInt_config(USART2_SFR,INT_USART2);

	Receive_flag_Uart_1 = 0;
	Receive_flag_Uart_2 = 0;
	while(1)
	{
		if(Receive_flag_Uart_1)
		{
			Receive_flag_Uart_1 = 0;
			//打印当前函数所在文件名和行号
			fprintf(USART1_STREAM,"文件file %s : %d \r\n",__FUNCTION__,__LINE__);
		}

		if(Receive_flag_Uart_2)
		{
			Receive_flag_Uart_2 = 0;
			//打印当前函数所在文件名和行号
			fprintf(USART2_STREAM,"文件file %s : %d \r\n",__FUNCTION__,__LINE__);
		}
	}
}

/**
  * 描述   USART1引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART1()
{
	/* 端口重映射AF5 */
	//USART1_RX		PB0
	//USART1_TX0	PB1
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_0, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_1, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);	  //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_1, GPIO_RMP_AF5_USART1);     //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_0, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_1, TRUE);                  //配置锁存
}

/**
  * 描述   USART2引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART2()
{
	/* 端口重映射AF5 */
	//USART1_RX		PB15
	//USART1_TX0	PB14
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_14, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_15, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_14, GPIO_RMP_AF5_USART2);	  //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_15, GPIO_RMP_AF5_USART2);     //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_14, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_15, TRUE);                  //配置锁存
}

/**
  * 描述   串口发闪送换行符
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void Usart_line_feed(USART_SFRmap *USARTx)
{
	USART_SendData(USARTx,0x0D);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	USART_SendData(USARTx,0x0A);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line)*/

	  while(1)
	  {
	  }
}



