/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: SysTick
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了实现系统节拍定时器功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	 //先设置为高电平

}


/**
  * 描述  系统节拍定时器 初始化重加载值配置。
  * 输入 : Reload: 取值24位数值，系统节拍定时器重加载值，节拍定时器的周期值为重加载值加1。
  *
  * 返回  无。
  */
void SysTick_Configuration(uint32_t Reload)
{
	SYSTICK_Cmd (FALSE);
	SYSTICK_Reload_Config(Reload);
	SYSTICK_Counter_Updata();                           //向ST_CV寄存器写任意值，以清零当前值寄存器
	SYSTICK_Clock_Config(SYSTICK_SYS_CLOCK_DIV_1);      //系统节拍定时器时钟源选择，SCLK作为时钟源
	SYSTICK_Systick_INT_Enable(TRUE);
	SYSTICK_Cmd(TRUE);
    INT_Interrupt_Enable(INT_SysTick,TRUE);				//使能SYSTICK中断
	INT_All_Enable (TRUE);
}




//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现使用节拍定时器功能。
	 * 例程中节拍定时器时钟源为主频 SCLK ，8.3ns左右计数一次，计数6000000进一次中断，50ms进一次中断，
	 * PB13在中断函数中翻转，D4灯闪烁，并且在中断里无需软件清除标志
	 * 系统节拍定时器重加载值为24位数据
	 * 注：此版本节拍定时器时钟源只能为主频 SCLK，不能用SCLK/2
     */

	//系统时钟120M,外设高频时钟16M
	SystemInit(); //系统时钟初始化
	//把PB13设为输出，并设为高电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);
	/*节拍定时器重装值配置,时钟源为120M，8.3ns左右计数一次 */
	SysTick_Configuration(6000000);     //计数6000000进一次中断，8.3ns X 6000000 =50mS进一次中断
    while(1)
    {
    }

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

