/** ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了ADC高优先级通道同步模式的使用方法。使用CCP2CH1作为触发信号源，每次触发后，
  * 	ADC0和ADC1的高优先级通道同时进行转换。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "ADC_user.h"
#include "CCP_user.h"
#include "GPTIMER_user.h"
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13,GPIO_MODE_OUT);  //STATUS灯配置为输出模式

	//配置USART2引脚重映射
	GPIO_USART();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);

	Init_T2();
	Init_ccp1_for_trog(); //初始化ADC高优先级同步的触发源，CCP2_CH1

	Init_adc0();
	Init_adc0_it(); //配置ADC0的高优先级的扫描结束中断，用于处理高优先级通道的数据

	Init_adc1();
	Init_adc1_it(); //配置ADC1的高优先级的扫描结束中断，用于处理高优先级通道的数据

	ADC_Double_Mode_Config(ADC_HPRIORITY_SYNC); //不能使用混合模式，高低优先级的通道必须分开使用，配置为高优先级通道同步

 	Delay(0xFFF);  //ADC使能后需要一段短暂的时间，等待电路充电完毕后，才能去采样，否者可能存在较大的采样误差

 	INT_All_Enable (TRUE);//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断
	while(1)
	{
		Delay(0xFFFFF);
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13); //状态灯翻转
	}		
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
