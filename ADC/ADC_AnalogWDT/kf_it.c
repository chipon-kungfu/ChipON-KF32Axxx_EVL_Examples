/****************************************************************************************
 *
 * File Name: kf_it.c
 * Project Name: ADC_AnalogWDT
 * Version: v1.0
 * Date: 2022-01-05- 15:10:21
 * Author: chipon
 * 
 ****************************************************************************************/
//#include<KF32A151MQV.h>
#include "stdio.h"
#include "system_init.h"
#include "USART_user.h"
#include "ADC_user.h"
//asm(".include		\"KF32A151MQV.inc\"	");	 

//Note:
//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	

void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

void __attribute__((interrupt))_ADC0_exception (void)
{
	if(ADC_Get_INT_Flag(ADC0_SFR,ADC_INT_AWD))
	{
		ADC_Clear_INT_Flag(ADC0_SFR,ADC_INT_AWD);
		fprintf(USART2_STREAM,"ADC WDT is active\r\n");
	}
}
