/*
 * stop0.c
 *
 *  Created on: 2022-3-20
 *      Author:
 */

#include "stop0.h"

int __attribute__((section(".indata"))) FLASH_CYCLE_SET(void)
{
     asm("DSI");  			//关中断，避免其他未定义到RAM中的程序执行
     asm("push lr");		//函数现场保护

     asm("push R4 ");		//函数现场保护
     asm("push R3 ");		//函数现场保护
     asm("push R2  ");	//函数现场保护
     asm("push R1  ");	//函数现场保护
     asm("SUB SP,#12  "); //开辟新的栈空间
//   asm("push R0  ");
     asm("ld r2,#0x40200100");
     asm("ld r1,#0x87654321");
     asm("ST.W  [r2 + #10],r1");
     asm("ld  r1,#0x05040302");
     asm("ST.W  [r2 + #10],r1");   	//解锁ISP寄存器
     asm("ld  r1,#0x879A43DE");
     asm("ST.W  [r2 + #12],r1 ");
     asm("ld r1,#0x27262524");
     asm("ST.W  [r2 + #12],r1");   	//解锁CFG寄存器
     asm("ld r1,#0x00000018");
     asm("st.w  [r2],r1");        	//set dwen ifen
     asm("mov   r1,#0x02");
     asm("st.w  [r2 + #1],R1");    	//将recallen置1
     //#########################################
     asm("mov  r2,SP");            	//校准值存放地址放在新开辟的堆栈空间
asm("Trim_info_new:  ");				//flash校准开始
     asm("LD r1,#0x00100000  ");
//------------------------------------------------------
asm("Trim_info1_new:    ");		//读出96bit的flash校准值
     asm("ld.w  r3,[r1] ");			//读信息区校准值，读出后高16位与低16										//位互反校验
     asm("mov r0,R3 ");
     asm("ror r0,#16");
     asm("not       r0,R0");
     asm("cmp       r0,R3");
     asm("jnz       cal_err");		//校验出错跳转到cal_err
     asm("ld.w  r4,[r1 + #2]");
     asm("mov       r0,R4");
     asm("ror       r0,#16");
     asm("not       r0,R0");
     asm("cmp       r0,R4");
     asm("jnz       cal_err");      //校验出错跳转到cal_err
     asm("zxt.h r3,r3");
     asm("lsl       r4,#16");
     asm("xrl       r3,R4");			//2个 16bit校准值组成1个32bit校准值
     asm("st.w  [r2++],R3");        //校准值缓存到RAM
     //---------------------------------------
     asm("add       r1,#0x10");     //循环读出需要的校准值
     asm("LD        r3,#0x00100030");
     asm("cmp       r1,r3");
     asm("JNZ       Trim_info1_new");
     //################################################################
asm("Trim_Flash_new: ");			//将校准值写入flash中完成校准
     asm("ld        r2,#0x40200100");
     asm("ld        r0,#0xa05f0000");
     asm("st.w  [r2 + #1],R0");  			//清零recallen
     //################################################################
     asm("MOV       r4,SP");
asm("Trim_Flash1_new:");
     asm("LD        r1,#0x00000000");
asm("Trim_Flash2_new:");
     asm("ld.w  r3,[r4++]");
     asm("ST.w  [R1],R3");          			//校准值写入写缓存
     asm("mov       r3,#0x04");
     asm("orl       r3,r3,r0");
     //-------------------------  load
     asm("st.w  [r2 + #1],R3");				//使能写缓存写入flash
     asm("st.w  [r2 + #1],R0");
     //---------------------------- next
     asm("add       r1,#0x8");				//循环直到所有值均写入flash
     asm("LD        r3,#0x18");
     asm("cmp       r1,r3");
     asm("JNZ       Trim_Flash2_new");
//##############################################################################
asm("Trim_End:");
     asm("add       r3,r2,#4");
     asm("clr       [r3],#0"); 				//清IPSEL，无效
     asm("clr       [r2],#3"); 				//清dwen
//=========================================================================
     asm("mov   r0,#0");						//校准正确值返回值为0
     asm("jmp   cal_end");					//校准结果后进入cal_end
//#####################################################################
asm("cal_err:");
     asm("mov r0,#1");						//出错时返回值为1
//#####################################################################
asm("cal_end:");
     asm("ld        r2,#0x40200100");
     asm("ld        r3,#0xa05f0000");
     asm("add       r1,r2,#4");
//   asm("clr       [r1],#0");
//   asm("jnb       [r1],#1");
//   asm("add       r3,#0x02");
     asm("st.w      [r1],R3");				//清零recallen，IFEN
    // asm
     asm("mov   r3,#0x00");
     asm("st.w  [r2 + #2],r3");
     asm("ld    r3,#0x50af0007");
     asm("st.w  [r2],r3");					//flash寄存器上锁
//#####################################################################
//   asm("pop R0");
     asm("ADD SP,#12  ");
     asm("pop R1");
     asm("pop R2");
     asm("pop R3");
     asm("pop R4");
     asm("pop lr");
//#####################################################################
     asm("ENI");  		//开中断
     asm("jmp lr"); 	//函数退出
}


/* Name:Goto_Stop0
 * Describe:定义在RAM中执行
 * */
void __attribute__((section(".indata"))) Goto_Stop0(void)
{
	volatile uint8_t flash_cal_err = 0;
	volatile uint8_t flash_cal_retry = 5;
    /*Lower the system clock*/
	OSC_SCK_Division_Config(SCLK_DIVISION_1);
	/* Select INTHF for SCK. */
	OSC_SCK_Source_Config(SCLK_SOURCE_INTHF);

	/*Close the PLL and HFCK to reduce power consumption.*/
	OSC_PLL_Software_Enable(FALSE);
	/* Disable HFCK. */
	OSC_HFCK_Enable(FALSE);
	/* Disable EXTHF. */
	OSC_EXTHF_Software_Enable(FALSE);
	/* Disable EXTLF. */
	OSC_EXTLF_Software_Enable(FALSE);

    asm("DSI");
    /* Switch back the interrupt vector table to RAM. */
    SYS_VECTOFF = (volatile uint32_t)&_startRAM;
    asm("ENI");

    /* Select INTLF for SCK. */
	OSC_SCK_Source_Config(SCLK_SOURCE_INTLF);
	/*Disable INTHF. */
	OSC_INTHF_Software_Enable(FALSE);

    BKP_Write_And_Read_Enable(TRUE);

    /* Clear core interrupt register flag. */
    INT_EIF2 = 0;
    INT_EIF1 = 0;
    INT_EIF0 = 0;

    /*Clear wake up interrupt flags*/
     PM_STAC |= 0xF8000000u;
     asm("NOP");
     asm("NOP");
     asm("NOP");
     asm("NOP");
     PM_STAC &= ~0xF8000000u;

    /* Enable WKUP Interrupt */
    INT_EIE2 |= (1u << 25u);

    /* Enter stop0. */
    asm("SLEEP");
    asm("NOP");

    asm("DSI");

    /* Must calibrate Flash after stop0 mode is be awaked. */
	do
	{
		flash_cal_err = FLASH_CYCLE_SET();
	} while (flash_cal_err && (flash_cal_retry-- > 0));
	if(flash_cal_retry == 0)
	{
		asm("RESET");
	}


	/* Switch back the interrupt vector table to Flash. */
	SYS_VECTOFF = (volatile uint32_t) & _start;
    asm("ENI");

    BKP_Write_And_Read_Enable(FALSE);
}

void  WKUP_Init(void)
{
    /* Enable WKUP pin pull if WKUP edge is falling. */
    GPIO_Pull_Up_Enable(GPIOC_SFR, GPIO_PIN_MASK_7, TRUE);
    /*Enable the BKP area is enable*/
    BKP_Write_And_Read_Enable(TRUE);

    /* Enable WKUP to wake up*/
    PM_External_Wakeup_Pin_Enable(PM_PIN_WKP4, TRUE);
    /* Set WKUP edge trigger. */
    PM_External_Wakeup_Edge_Config(PM_PIN_WKP4, PM_TRIGGER_FALL_EDGE);
    /* Clear WKUP flag. */
    PM_Clear_External_Wakeup_Pin_Flag(PM_WAKEUP_EXTERNAL_PIN_WKP4);

    BKP_Write_And_Read_Enable(FALSE);

	/* Configuration WKUP Interrupt Priority */
    INT_Interrupt_Priority_Config(INT_WKP4, 0, 0);
    /* Clear WKUP interrupt flag. */
    INT_Clear_Interrupt_Flag(INT_WKP4);
    /* Enable WKUP Interrupt. */
    INT_Interrupt_Enable(INT_WKP4, FALSE);
}

void  Stop0_Init(void)
{
    BKP_Write_And_Read_Enable(TRUE);
    /* Set stop0 mode. */
    PM_Low_Power_Mode_Config(PM_LOW_POWER_MODE_STOP_0);
	BKP_Write_And_Read_Enable(FALSE);

}
