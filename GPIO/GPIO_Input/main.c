/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: GPIO_Input
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了GPIO输入应用参考
 *
 ****************************************************************************************/
#include "system_init.h"
#define    KEYUSER    GPIO_Read_Input_Data_Bit(GPIOD_SFR,GPIO_PIN_MASK_7)  //读取PD7的输入电平
//延时函数 局部变量用volatile声明，否则可能会被优化

void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=7200;
		while(j--);
	}

}


/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);					//先设置为高电平

}

/**
  * 描述  GPIOx 输入初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Input_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输入模式 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;                   //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_IN;                      //初始化 GPIO方向为输入
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;                     //初始化 GPIO是否上拉 不上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;                   //初始化 GPIO是否下拉 不下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);
}


/**
  * 描述  GPIOx 输入与输出初始化配置。
  * 输入 : 无
  * 返回  无。
  */
void GPIO_Init(void)
{
	GPIOInit_Input_Config(GPIOD_SFR,GPIO_PIN_MASK_7);//把PD7设为输入模式
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);  //PB13初始化输出高电平
}

//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现GPIO输入配置功能。
	 * 例程中使用GPIO PD7作为按键的输入IO PB13作为输出
	 * 按键KEYUSER按下，DD4灯熄灭，松手后再次按下D4灯点亮。
	 */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//GPIO初始化
	GPIO_Init();   //把PD7设为输入模式，PB13初始化输出高电平

	while(1)
	{
        if(KEYUSER)
        {
        	delay_ms(50);   //延时抖动
        	if(KEYUSER)         //有按键按下
        	{
        		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_13);//PB13翻转,D4灯点亮或熄灭
        		while(KEYUSER);//等按键松手
        	}

        }

	}
}


void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

