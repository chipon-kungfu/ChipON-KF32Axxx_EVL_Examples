/****************************************************************************************
 *
 * 文件名 kf_it.c
 * 作  者  ChipON_AE/FAE_Group
 * 日  期  2019-11-16
 * 描  述  该文件提供了串口异步收发应用例程参考
 *       +SPI主机中断收发
 *       +SPI从机中断收发
 *
 ****************************************************************************************/
#include"system_init.h"
#include "Usart.h"
#include "SPI.h"

//*****************************************************************************************
//                                 SPI1中断函数
//*****************************************************************************************	
void __attribute__((interrupt))_SPI1_exception (void)
{
#if SPI_MASTER
	if (SPI_Get_Transmit_Buf_Flag(SPI_COM) == RESET)//主机发送为空
	{
		if(!Command_flag)//判断命令标志，发送命令
		{
			  Command_flag=1;
			  SPI_I2S_SendData8(SPI_COM,Command);
		}
		else if(!Dummy_flag)//判断dummy标志，发送dummy
		{
			   Dummy_flag=1;
			   SPI_I2S_SendData8(SPI_COM,Dummy);
		}
		else//主机发送与接收
		{
			if (Tx_length < BufferSize )//循环发
			{
			    /*发送byte */
				SPI_I2S_SendData8(SPI_COM, Tx_master[Tx_length++]);
				/*等待接收完毕*/
			    while (SPI_Get_Receive_Buf_Flag(SPI_COM) == RESET);
			    /*取接收数据*/
			    Read_data[Rx_length++]=SPI_I2S_ReceiveData(SPI_COM);
			}
			else
			{
				/*关闭发送中断*/
				SPI_TNEIE_INT_Enable(SPI_COM,FALSE);
		        /*等待发送buffer空闲 */
		        while(SPI_Get_BUSY_Flag(SPI_COM)==SET);
		    	/*初始化变量 */
		    	Tx_length=0;
		        Rx_length=0;
		        Command_flag=0;
		    	Dummy_flag=0;
		    	Uart_flag=1;
			}
		}
	}
#else
	if(SPI_Get_Receive_Buf_Flag(SPI_COM)== SET)//接收BUF未空
	{
		if(!Command_flag)//接收命令
		{
			if((SPI_I2S_ReceiveData(SPI_COM)==Command)&&(Rx_length==0))//判断第一个字节接收到命令
			{
				Command_flag=1;
			}
		}
		else if((!Dummy_flag)&&(Command_flag))//接收dummy
		{
			Dummy_flag=1;
			SPI_I2S_ReceiveData(SPI_COM);//清接收buff
			SPI_I2S_SendData8(SPI_COM, 0x00);//启动发送
		}
		else
		{
			if (Rx_length < BufferSize)//循环接收
			{
				/* 去接收数据 */
				Read_data[Rx_length++] = SPI_I2S_ReceiveData(SPI_COM);

				if(Rx_length==BufferSize)//接收满清标志
				{
					Rx_length=0;
					Command_flag=0;
					Dummy_flag=0;
					Uart_flag=1;
				}
			}
		}
	}

	if (SPI_Get_Transmit_Buf_Flag(SPI_COM) == RESET)//发送BUF为空
	{
		if(Command_flag && Dummy_flag)//接收命令正确发数据
		{
			if (Tx_length < BufferSize )//循环发
			{
				/*发数据*/
				SPI_I2S_SendData8(SPI_COM, Tx_slave[Tx_length++]);
			}
			else
			{
				/* 发送结束 */
				if(Tx_length==BufferSize)
				{
//					Tx_length ++;
					Tx_length = 0;
				}
			}
		}
		else//初始化buff
		{
			Tx_length=0;
//			SPI_I2S_SendData8(SPI_COM, 0x00);
		}
	}
#endif
}


