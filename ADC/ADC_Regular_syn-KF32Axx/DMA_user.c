/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */

#include "system_init.h"
#include "DMA_user.h"
volatile uint32_t  buf_1[96] ={0};
volatile uint32_t  buf_2[96] ={0};

#ifndef DMA_C_
#define DMA_C_

void Init_dma0()
{
	DMA_InitTypeDef dmaNewStruct;
	/* DMA功能配置 */

	/* 配置 传输数据个数: 24 */
	dmaNewStruct.m_Number = 96;
	/* 配置 DMA传输方向：外设到内存 */
	dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
	/* 配置 DMA通道优先级：低优先级 */
	dmaNewStruct.m_Priority = DMA_CHANNEL_HIGHER;
	/* 配置 外设数据位宽：32位宽 */
	dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
	/* 配置 存储器数据位宽:32位宽 */
	dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
	/* 配置 外设地址增量模式使能: 使能 */
	dmaNewStruct.m_PeripheralInc = FALSE;
	/* 配置 存储器地址增量模式使能: 使能 */
	dmaNewStruct.m_MemoryInc = TRUE;
	/* 配置 DMA通道选择:通道1 */
	dmaNewStruct.m_Channel = DMA_CHANNEL_3;
	/* 配置 数据块传输模式： */
	dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BLOCK;//
	/* 配置 循环模式使能: 禁止 */
	dmaNewStruct.m_LoopMode = TRUE;
	/* 配置 外设起始地址：等待发送的数据的起始地址 */
	dmaNewStruct.m_PeriphAddr = (uint32_t) 0x400005A0; //ADC0外设的

	/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
	dmaNewStruct.m_MemoryAddr = (uint32_t) buf_1;
	/* 配置DMA功能函数 */
	DMA_Configuration (DMA0_SFR, &dmaNewStruct);

	/* 使能通道1 DMA */
	DMA_Channel_Enable (DMA0_SFR, DMA_CHANNEL_3, TRUE); //DMA0_ch3 对应AD0
}

void Init_dma0_it()
{
	DMA_Set_INT_Enable(DMA0_SFR, DMA_CHANNEL_3, DMA_INT_HALF_TRANSFER,TRUE); //DMA0_ch3 对应AD0
	DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_3, DMA_INT_HALF_TRANSFER); //DMA0_ch3 对应AD0

	DMA_Set_INT_Enable(DMA0_SFR, DMA_CHANNEL_3, DMA_INT_FINISH_TRANSFER,TRUE); //DMA0_ch3 对应AD0
	DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_3, DMA_INT_FINISH_TRANSFER); //DMA0_ch3 对应AD0
}

void Init_dma1()
{
	DMA_InitTypeDef dmaNewStruct;
	/* DMA功能配置 */

	/* 配置 传输数据个数: 24 */
	dmaNewStruct.m_Number = 96;
	/* 配置 DMA传输方向：外设到内存 */
	dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
	/* 配置 DMA通道优先级：低优先级 */
	dmaNewStruct.m_Priority = DMA_CHANNEL_LOWER;
	/* 配置 外设数据位宽：32位宽 */
	dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
	/* 配置 存储器数据位宽:32位宽 */
	dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
	/* 配置 外设地址增量模式使能: 使能 */
	dmaNewStruct.m_PeripheralInc = FALSE;
	/* 配置 存储器地址增量模式使能: 使能 */
	dmaNewStruct.m_MemoryInc = TRUE;
	/* 配置 DMA通道选择:通道1 */
	dmaNewStruct.m_Channel = DMA_CHANNEL_4;
	/* 配置 数据块传输模式： */
	dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BLOCK;//
	/* 配置 循环模式使能: 禁止 */
	dmaNewStruct.m_LoopMode = TRUE;
	/* 配置 外设起始地址：等待发送的数据的起始地址 */
	dmaNewStruct.m_PeriphAddr = (uint32_t) &ADC1_DATA; //ADC0外设的

	/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
	dmaNewStruct.m_MemoryAddr = (uint32_t) buf_2;
	/* 配置DMA功能函数 */
	DMA_Configuration (DMA0_SFR, &dmaNewStruct);

	/* 使能通道1 DMA */
	DMA_Channel_Enable (DMA0_SFR, DMA_CHANNEL_4, TRUE); //DMA0_ch3 对应AD0
}

void Init_dma1_it()
{
	DMA_Set_INT_Enable(DMA0_SFR, DMA_CHANNEL_4, DMA_INT_HALF_TRANSFER,TRUE); //DMA0_ch3 对应AD0
	DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_4, DMA_INT_HALF_TRANSFER); //DMA0_ch3 对应AD0

	DMA_Set_INT_Enable(DMA0_SFR, DMA_CHANNEL_4, DMA_INT_FINISH_TRANSFER,TRUE); //DMA0_ch3 对应AD0
	DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_4, DMA_INT_FINISH_TRANSFER); //DMA0_ch3 对应AD0
}

#endif /* DMA_C_ */
