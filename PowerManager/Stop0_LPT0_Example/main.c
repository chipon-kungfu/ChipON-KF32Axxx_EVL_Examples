/****************************************************************************************
 *
 * File Name: main 
 * Project Name: Stop0_Wakeup_Example
 * Version: v1.0
 * Date: 2022-03-20- 16:07:40
 * Author:
 * 
 ****************************************************************************************/
//#include<KF32A151MQV.h>
#include "system_init.h"
#include "usart.h"
#include "stop0.h"
#include "EXINT_user.h"
void LPTIMER_T0_Config(uint32_t Period,uint32_t Prescaler);

void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;

    for (i = 0; i < nms; i++)
    {
        j = 7000;
        while (j--)
            ;
    }
}

//Main Function
int main()
{
    /* Init system clock, SCLK = 120M. */
    SystemInit();
    /* Init LED(PB13) GPIO. */
    GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13, GPIO_MODE_OUT);
    GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_13, Bit_RESET); //LED OFF
    /*Init User Key.*/
    GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_7, GPIO_MODE_IN);
	/*Init Usart2.*/
	GPIO_USART();
	USART_Async_config(USART2_SFR);	// 19200
	/* Init LPT0. */
	LPTIMER_T0_Config(2499,63);
	/* Init PMC interrupt. */
	PMC_Error_Init();
	/* Init stop0 mode */
	Stop0_Init();
    /* Enable global interrupt. */
    INT_All_Enable(TRUE);


	while(1)
	{
		delay_ms(1000);
		USART_Send(USART2_SFR , "MCU is running\r\n" ,sizeof("MCU is running\r\n"));
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13);
		if(GPIO_Read_Input_Data_Bit(GPIOD_SFR,GPIO_PIN_MASK_7) == SET)
		{
			delay_ms(5);
			if(GPIO_Read_Input_Data_Bit(GPIOD_SFR,GPIO_PIN_MASK_7) == SET)
			{
				USART_Send(USART2_SFR , "MCU is going into the mode of Stop0\r\n" ,sizeof("MCU is going into the mode of Stop0\r\n"));
				/* Goto stop0 mode. Must be running in RAM*/
				Goto_Stop0();
				/* Reinitialize system clock, SCLK = 120M. */
				SystemInit();
				USART_Send(USART2_SFR , "MCU has been awaked up\r\n" ,sizeof("MCU has been awaked up\r\n"));
			}
		}
	}		
}

/**
  * 描述 低功耗定时器T0定时时间配置。
  * 输入  Period：新的周期值，取值16位数据
  *     Prescaler：对INTLF的预分频值
  * 返回  无。
  */
void LPTIMER_T0_Config(uint32_t Period,uint32_t Prescaler)
{
	BKP_Write_And_Read_Enable(TRUE);                          //使能备份域读写功能
	PM_CCP0CLKLPEN_Enable(TRUE);                              //CCP0低功耗时钟使能
	PM_CTL2 &= (~PM_CTL2_CCP0LPEN);							  //T0复位PM_Peripheral_Reset_Config (PM_PERIPHERAL_CCP,PERIPHERAL_RST_STATUS);
	BKP_Write_And_Read_Enable(FALSE);

	TIM_Reset(T0_SFR);										  //定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(T0_SFR,TRUE);			  //立即更新控制
	GPTIM_Updata_Enable(T0_SFR,TRUE);						  //配置更新使能
	GPTIM_Work_Mode_Config(T0_SFR,GPTIM_TIMER_MODE);		  //定时模式选择
	GPTIM_Set_Counter(T0_SFR,0);							  //定时器计数值
	GPTIM_Set_Period(T0_SFR,Period);						  //定时器周期值
	GPTIM_Set_Prescaler(T0_SFR,Prescaler);				      //定时器预分频值
	GPTIM_Counter_Mode_Config(T0_SFR,GPTIM_COUNT_UP_DOWN_OUF);//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(T0_SFR,GPTIM_LFCLK);					  //选用LFCLK时钟

	INT_Interrupt_Priority_Config(INT_T0,4,0);				  //抢占优先级4,子优先级0
	GPTIM_Overflow_INT_Enable(T0_SFR,TRUE);					  //计数溢出中断使能
	INT_Interrupt_Enable(INT_T0,TRUE);						  //外设中断使能
	INT_Clear_Interrupt_Flag(INT_T0);						  //清中断标志
	GPTIM_Cmd(T0_SFR,TRUE);									  //定时器启动控制使能
	INT_All_Enable (TRUE);									  //全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

}
