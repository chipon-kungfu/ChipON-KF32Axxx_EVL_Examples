/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"

#include "ADC_user.h"
#ifndef ADC_C_
#define ADC_C_

void Init_adc0()
{
	ADC_InitTypeDef adcStruct;

	GPIO_Write_Mode_Bits(GPIOG_SFR,GPIO_PIN_MASK_4,GPIO_MODE_AN);//ADC_CHANNEL_22
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_1,GPIO_MODE_AN);//ADC_CHANNEL_1

	ADC_Reset (ADC0_SFR); //复位ADC外设并打开ADC的时钟

	/* 初始化 ADC时钟源选择 */
	adcStruct.m_Clock = ADC_HFCLK;
	/* 初始化 ADC时钟分频 */
	adcStruct.m_ClockDiv = ADC_CLK_DIV_8;
	/* 初始化 ADC扫描模式使能 */
	adcStruct.m_ScanMode = FALSE;  //不使用扫描模式，一次触发只转换第一个通道
	/* 初始化 ADC连续转换模式 */
	adcStruct.m_ContinuousMode = ADC_CONTINUOUS_MODE; //使用连续转换功能
	/* 初始化 ADC转换结果输出格式 */
	adcStruct.m_DataAlign = ADC_DATAALIGN_RIGHT; //转换的结果右对齐
	/* 初始化 ADC常规通道外部触发转换模式使能 */
	adcStruct.m_ExternalTrig_EN = FALSE; //常规通道失能外部条件触发ADC转换，
	/* 初始化 ADC常规通道外部触发事件 */
	adcStruct.m_ExternalTrig = ADC_EXTERNALTRIG_T1TRGO; //常规通道外部触发信号源配置为T1TRGO
	/* 初始化 ADC高优先级通道外部触发转换模式使能 */
	adcStruct.m_HPExternalTrig_EN = FALSE; //高优先级通道失能外部条件触发ADC转换，
	/* 初始化 高优先级通道外部触发事件 */
	adcStruct.m_HPExternalTrig = ADC_HPEXTERNALTRIG_CCP1_CH1; //高优先级通道触发信号源配置为CCP1的通道1
	/* 参考电压选择，取值为宏“ADC参考电压选择”中的一个。 */
	adcStruct.m_VoltageRef = ADC_REF_AVDD; //转换的参考电压源配置为VDDA
	/* 初始化 ADC常规通道扫描长度 */
	adcStruct.m_NumOfConv = 0; //扫描的长度配置为1个通道
	/* 初始化 ADC高优先级通道扫描长度 */
	adcStruct.m_NumOfHPConv = 1; //高优先级通道的扫描长度
	ADC_Configuration (ADC0_SFR, &adcStruct);

	ADC_Cmd (ADC0_SFR, TRUE); //ADC使能

	ADC_High_Priority_Channel_Config (ADC0_SFR, ADC_CHANNEL_22, 0x01);   //高优先级通道配置

	Delay(0xFF);  //ADC使能后需要一段短暂的时间，等待电路充电完毕后，才能去采样，否者可能存在较大的采样误差
}

void Init_ADC_Analog_Wd(uint32_t ADC_WDCH_N)
{
	ADC_WD_InitTypeDef M;

    /* 初始化 ADC模拟看门狗单通道使能 */
    M.m_WDSingleCH = ADC_WD_SINGLE_CH;

    /* 初始化 ADC高优先级通道上看门狗使能 */
    M.m_HPChannelWDEN = TRUE;

    /* 初始化 ADC常规通道上看门狗使能 */
    M.m_ChannelWDEN = FALSE;

    /* 初始化 ADC模拟看门狗通道选择 */
    M.m_WDChannel = ADC_WDCH_N;

    /* 初始化 ADC模拟看门狗高阈值 */
    M.m_Threshold_H = 4000;

    /* 初始化 ADC模拟看门狗低阈值 */
    M.m_Threshold_L = 500;

	ADC_Analog_Watchdog_Configuration (ADC0_SFR,&M);
}
void Init_adc0_INT()
{
	ADC_Set_INT_Enable(ADC0_SFR,ADC_INT_AWD,TRUE);  //使能模拟看门狗中断
	INT_Interrupt_Priority_Config(INT_ADC0,4,0);    //优先级
	INT_Clear_Interrupt_Flag(INT_ADC0);
	INT_Interrupt_Enable(INT_ADC0,TRUE);            //使能ADC中断
}
#endif /* ADC_C_ */
