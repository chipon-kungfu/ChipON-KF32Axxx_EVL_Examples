/**
  ********************************************************************
  * 文件名  Usart.h
  * 作  者   ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了Usart GPIO初始化、Usart配置参数、Usart发送函数等相关函数宏定义。
  *
  *********************************************************************
*/
#ifndef USART_H_
#define USART_H_
#include <string.h>
#include <stdint.h>
#include "system_init.h"

#define   Usart_Print  0          //串口打印宏定义  1：开 0：关

void GPIO_USART(void);
void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);
void USART_Async_config(USART_SFRmap *USARTx);

#endif /* USART_H_ */
