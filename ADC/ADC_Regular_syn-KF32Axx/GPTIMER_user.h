/*
 * gptimer.h
 *
 *  Created on: 2019-11-6
 *      Author: charles_cai
 */

#ifndef GPTIMER_H_
#define GPTIMER_H_

void Init_T1();
void Init_T1_it();
void Init_T2();

#endif /* GPTIMER_H_ */
