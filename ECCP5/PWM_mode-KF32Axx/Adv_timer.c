/**
  ******************************************************************************
  * 文件名  Adv_timer.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了关于高级定时器的使用方法
  *
  *********************************************************************
  */
#include "system_init.h"
#include "Adv_timer.h"
/**
  * 描述   Tx系列定时器的配置
  * 输入   T5_SFR/T9_SFR
  * 返回   无
  */
void cfg_Tx(ATIM_SFRmap* ATIMx)
{
	ATIM_X_Work_Mode_Config(ATIMx,ATIM_TIMER_MODE);				//定时模式
	ATIM_X_Set_Counter(ATIMx,0);									//T9计数值清0
	ATIM_X_Set_Period(ATIMx,0xffff);								//周期值
	ATIM_X_Set_Prescaler(ATIMx,0);									//预分频器 1:1
	ATIM_X_Postscaler_Config(ATIMx,ATIM_POSTSCALER_DIV_1);			//后分频比为1:1
	ATIM_X_Counter_Mode_Config(ATIMx,ATIM_COUNT_UP_OF);			//中心对齐的PWM信号,向上计数，上溢时产生中断标志
	ATIM_X_Clock_Config(ATIMx,ATIM_HFCLK);							//选用HFCLK时钟为Tx工作时钟
	ATIM_X_Updata_Immediately_Config(ATIMx,TRUE);					//立即更新以 T9 为时基的占空比、周期、预分频、更新计数器寄存器，并将定时器清零

	ATIM_X_Updata_Output_Ctl(ATIMx,TRUE);							//立即更新以T9为时基的输出控制寄存器
	ATIM_X_Updata_Enable(ATIMx,TRUE);								//允许以T9为时基的相关配置信息更改
	ATIM_X_Cmd(ATIMx,TRUE);										//T9启动控制使能
}

/**
  * 描述   Tz系列定时器的配置
  * 输入   T6_SFR/T10_SFR
  * 返回   无
  */
void cfg_Tz(ATIM_SFRmap* ATIMx)
{
	ATIM_Z_Work_Mode_Config(ATIMx,ATIM_TIMER_MODE);				//定时模式
	ATIM_Z_Set_Counter(ATIMx,0);									//T10计数值清0
	ATIM_Z_Set_Period(ATIMx,0x0fff);								//周期值
	ATIM_Z_Set_Prescaler(ATIMx,0);									//预分频器 1:1
	ATIM_Z_Postscaler_Config(ATIMx,ATIM_POSTSCALER_DIV_1);			//后分频比为1:1
	ATIM_Z_Counter_Mode_Config(ATIMx,ATIM_COUNT_UP_DOWN_OUF);			//中心对齐的PWM信号,向上向下计数，上/下溢时产生中断标志，PWM输出周期将放大1倍
	ATIM_Z_Clock_Config(ATIMx,ATIM_HFCLK);							//选用HFCLK时钟为Tx工作时钟
	ATIM_Z_Updata_Immediately_Config(ATIMx,TRUE);					//立即更新以 T10为时基的占空比、周期、预分频、更新计数器寄存器，并将定时器清零

	ATIM_Z_Updata_Output_Ctl(ATIMx,TRUE);							//立即更新以T10为时基的输出控制寄存器
	ATIM_Z_Updata_Enable(ATIMx,TRUE);								//允许以T10为时基的相关配置信息更改
	ATIM_Z_Cmd(ATIMx,TRUE);										//T10启动控制使能
}
