/****************************************************************************************
 * 文件名: I2C_Slave.c
 * 项目名: I2C_Slave_Mode
 * 版 本:  V2.4
 * 日 期:  2021年7月29日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了I2C从通信功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
#include "I2C_Slave.h"


/**
  * 描述   I2C GPIO重映射初始化
  * 输入   无
  * 返回   无
*/
void I2C_GPIO_init(void)
{
	/*I2Cx*/
	/*引脚配置*/
	GPIO_InitTypeDef I2C_GPIO_Init;

	I2C_GPIO_Init.m_Mode = GPIO_MODE_RMP;                           //配置GPIO重映射功能
	I2C_GPIO_Init.m_OpenDrain = GPIO_POD_OD;                        //GPIO开漏输出
	I2C_GPIO_Init.m_Speed = GPIO_HIGH_SPEED;						//IO配置为高速
	I2C_GPIO_Init.m_Pin = GPIO_PIN_MASK_1|GPIO_PIN_MASK_2;          //Px1 Px2
	I2C_GPIO_Init.m_PullUp = GPIO_NOPULL;                           //配置GPIO上拉
	I2C_GPIO_Init.m_PullDown = GPIO_NOPULL;                         //不使能GPIO下拉
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_1,GPIO_RMP_AF8_I2C1); //配置PB1为I2C1 SCL
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_2,GPIO_RMP_AF8_I2C1); //配置PB2为I2C1 SDA
	GPIO_Configuration(GPIOB_SFR,&I2C_GPIO_Init);

}


/**
  * 描述   I2Cx 初始化
  * 输入   I2Cx：取值范围I2C0_SFR~I2C3_SFR   选用的I2C
  *      I2C_ADDRESS:取值范围0x0~0x3FF。     从机响应的7位地址或10位地址
  * 返回   无
*/
void I2C_init_Slave(I2C_SFRmap* I2Cx,uint16_t I2C_ADDRESS)
{
	/*配置I2Cx应用参数*/
	I2C_Reset(I2Cx);
	I2C_InitTypeDef newStruct_I2C;                        //
	newStruct_I2C.m_Mode = I2C_MODE_I2C;                  //配置为I2C模式
	newStruct_I2C.m_ClockSource = I2C_CLK_SCLK;           //时钟源选择SCLK
	newStruct_I2C.m_BADR10 = I2C_BUFRADDRESS_7BIT;        //配置7位从机地址
	newStruct_I2C.m_MasterSlave = I2C_MODE_SMBUSDEVICE;    //SMBus类型选择从机模式
	newStruct_I2C.m_BaudRateL = 12;                       //I2C波特率低电平时间周期   这个值 不能小于3
	newStruct_I2C.m_BaudRateH = 12;                       //I2C波特率高电平时间周期   这个值 不能小于3
	newStruct_I2C.m_AckEn = TRUE;                         //使能应答
	newStruct_I2C.m_AckData = I2C_ACKDATA_ACK;            //选择应答信号为ACK
	I2C_Configuration(I2Cx, &newStruct_I2C);

	I2C_ADDR_Config(I2Cx,0,I2C_ADDRESS);   //设置从机I2C地址0
	I2C_ADDR_Config(I2Cx,1,I2C_ADDRESS);   //设置从机I2C地址1
	I2C_ADDR_Config(I2Cx,2,I2C_ADDRESS);   //设置从机I2C地址2
	I2C_ADDR_Config(I2Cx,3,I2C_ADDRESS);   //设置从机I2C地址3
	I2C_MSK_Config (I2Cx,0,0x0000);        //配置I2C地址屏蔽位

	I2C_Clear_INTERRUPT_Flag(I2Cx);        //清I2C中断标志位

	I2C_MATCH_ADDRESS_Config(I2Cx,TRUE);   //使能配置I2C地址匹配
	I2C_Cmd(I2Cx,TRUE);                    //使能I2C模块

	/********************I2C中断配置*****************/
	INT_Interrupt_Enable(INT_I2C1, TRUE);     //使能I2C1 接收中断
	I2C_ISIE_INT_Enable(I2C1_SFR, TRUE);      //使能I2C接收中断
	I2C_Start_INT_Enable(I2C1_SFR, TRUE);     //使能START中断
	I2C_Stop_INT_Enable(I2C1_SFR, TRUE);	  //使能STOP中断
	INT_All_Enable(TRUE);                     //开总中断

}

