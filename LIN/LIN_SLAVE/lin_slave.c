/**
  ******************************************************************************
  * 文件名  lin_slave.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了异步串口相关配置函数，包括
  *          + 串口发送函数
  *          + 串口异步全双工配置，唤醒使能
  *          + 串口接收中断使能，自动唤醒中断使能
  *          + 标准校验和
  ******************************************************************************/
#include "system_init.h"
#include "lin_slave.h"
volatile uint8_t l_sys_mode;     //从机接收模式标志
volatile uint8_t Rev_Temp;       //接收数据（清接收中断标志
volatile uint8_t Rev_Temp_Clear;       //接收数据（清接收中断标志
volatile uint8_t l_rec_data[10]; //接收数据
uint8_t l_Tx_data[10]; 	 		 //发送数据
uint8_t lin_re_state;            //主机写，从机接收数据状态
uint8_t xxx;             	     //计数

void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//串口发送函数
uint8_t Check_Sum(uint8_t* Databuf,uint32_t point,uint32_t length);  //标准校验和
void USART_LIN_config(USART_SFRmap *USARTx);//串口异步全双工配置，唤醒使能
void USART_LIN_INT_config(USART_SFRmap *USARTx,InterruptIndex Peripheral);//串口接收中断使能，自动唤醒中断使能


/**
  * 描述   串口发送
  * 输入   USARTx:   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Databuf：   指向发送数据的指针
  *      length：      发送的长度
  * 返回   无
  */
void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length)
{
	uint32_t i;
	for(i=1;i<length;i++)
	{
		//串口发送
		USART_SendData(USARTx,Databuf[i]);
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	}
}
/**
  * 描述   标准校验和
  * 输入   Databuf:  指向校验数据的指针
  *      point：        校验数据起始位置
  *      length：      校验长度=length-point
  * 返回   校验和
  */
uint8_t Check_Sum(uint8_t* Databuf,uint32_t point,uint32_t length)
{
#if 1
	 uint8_t i=0;
	 uint16_t check_sum;
	 uint8_t check_sum_return;
	 check_sum=0;
	 for(i=point;i<length;i++)   //校验和（标准校验）
	 {

		 check_sum+=Databuf[i];
		 if(check_sum>0xFF)
		 {
			 check_sum = (check_sum % 0x100) +1;
		 }
	 }
	 check_sum_return = (uint8_t) check_sum;


	 return (~check_sum_return);
#endif
#if 0	 
	 
	 uint8_t i;
	 uint8_t check_sum;
	 check_sum=0;
	 for(i=point;i<length;i++)   //校验和（标准校验）
	 {
		 check_sum+=Databuf[i];
	 }
	 check_sum=0xFF-check_sum;

	 return check_sum; 
#endif	 
}

/**
  * 描述   串口异步全双工配置(默认8bit收发使能  全双工 9600),自动唤醒使能
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void USART_LIN_config(USART_SFRmap *USARTx)
{
	USART_InitTypeDef USART_InitStructure;

	USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode=USART_MODE_FULLDUPLEXASY;                        //全双工
    USART_InitStructure.m_TransferDir=USART_DIRECTION_FULL_DUPLEX;              //传输方向
    USART_InitStructure.m_WordLength=USART_WORDLENGTH_8B;                       //8位数据
    USART_InitStructure.m_StopBits=USART_STOPBITS_1;                            //1位停止位
    USART_InitStructure.m_BaudRateBRCKS=USART_CLK_HFCLK;                        //系统时钟作为 USART波特率发生器时钟
    /* 波特率 =Fck/(16*z（1+x/y)) 外设时钟内部高频16M*/
    //4800    z:208    x:0    y:0
    //9600    z:104    x:0    y:0
    //19200   z:52     x:0    y:0
    //115200  z:8      x:1    y:13
    //波特率9600
#if 1
    USART_InitStructure.m_BaudRateInteger=104;         //USART波特率整数部分z，取值为0~65535
    USART_InitStructure.m_BaudRateNumerator=0;         //USART波特率小数分子部分x，取值为0~0xF
    USART_InitStructure.m_BaudRateDenominator=0;       //USART波特率小数分母部分y，取值为0~0xF
#endif
	USART_Reset(USARTx);                                       //USARTx复位
	USART_Configuration(USARTx,&USART_InitStructure);          //USARTx配置
//	USART_WeakUP_Enable(USARTx,TRUE);                          //自动唤醒使能，等待下降沿
	USART_Clear_Transmit_BUFR_INT_Flag(USARTx);                //USARTx发送BUF清零
	USART_RESHD_Enable (USARTx, TRUE);						   //使能RESHD位
	USART_Cmd(USARTx,TRUE);                                    //USARTx使能
}

/**
  * 描述   串口接收中断配置
  * 输入   USARTx:指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Peripheral:外设或内核中断向量编号，取值范围为：
  *                 枚举类型InterruptIndex中的外设中断向量编号
  * 返回   无
  */
void USART_LIN_INT_config(USART_SFRmap *USARTx,InterruptIndex Peripheral)
{
	USART_Clear_Receive_ERROR_INT_Flag(USARTx);
//	USART_WeakUP_INT_Enable (USARTx,TRUE);//自动唤醒中断使能
	USART_Blank_INT_Enable(USARTx,TRUE);
	USART_RDR_INT_Enable(USARTx,TRUE);//接收中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);
	INT_All_Enable(TRUE);
}
