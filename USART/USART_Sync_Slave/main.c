/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了串口同步从机收发应用例程参考
  *
 ****************************************************************************************/
#include "system_init.h"
#include "Usart.h"
volatile uint8_t Receive_data; //接收数据

void GPIO_USART1();//USART1引脚重映射
void GPIO_USART2();//USART2引脚重映射
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程实现USART1的半双工同步接收功能,默认波特率9600，串口接
	* 收配置在主函数处理，接收到数据后，通过USART2全双工异步发送接收到的数据，
	* （TX接TX，RX接RX）*/

	//系统时钟120M,外设高频时钟16M
	SystemInit();

	//配置USART2引脚重映射，PA8-RX，PA11-TX1
	GPIO_USART2();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);
	USART_Passageway_Select_Config(USART2_SFR,USART_U7816R_PASSAGEWAY_TX1);//UASRT2选择TX1通道

	//配置USART1引脚重映射，PB0-RX，PB1-TX0
	GPIO_USART1();
	//半双工同步主模式  9bit 9600波特率 奇校验
	USART_Sync_config(USART1_SFR);
	//修改配置位从模式
	USART_Clock_Source_Config(USART1_SFR,USART_SLAVE_CLOCKSOURCE_EXTER);            //从模式
	USART_Transmit_Data_Enable(USART1_SFR,FALSE);                                   //USART1发送关闭
	USART_Receive_Data_Enable(USART1_SFR,TRUE);                                     //USART1接收使能
	USART_Cmd(USART1_SFR,TRUE);                                                     //USART1使能

	//串口接收中断使能
	USART_ReceiveInt_config(USART1_SFR,INT_USART1);
	INT_All_Enable(FALSE);//关闭总中断

	while(1)
	{
		if(USART_Get_Receive_BUFR_Ready_Flag(USART1_SFR))//接收标志位
		{
			Receive_data=USART_ReceiveData(USART1_SFR);
			USART_SendData(USART2_SFR,Receive_data);     //发送接收数据
		}
	}
}

/**
  * 描述    USART1引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART1()
{
	/* 端口重映射AF5 */
	//USART1_RX		PB0
	//USART1_TX0	PB1
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_0, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_1, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);	  //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_1, GPIO_RMP_AF5_USART1);     //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_0, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_1, TRUE);                  //配置锁存
}

/**
  * 描述    USART2引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART2()
{
	 /* 端口重映射AF5 */
	//USART2_RX		PA8
	//USART2_TX1	PA11
	GPIO_Write_Mode_Bits(GPIOA_SFR ,GPIO_PIN_MASK_8, GPIO_MODE_RMP);           //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOA_SFR ,GPIO_PIN_MASK_11, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_8, GPIO_RMP_AF5_USART2);	   //重映射为USART2
	GPIO_Pin_RMP_Config (GPIOA_SFR,GPIO_Pin_Num_11, GPIO_RMP_AF5_USART2);      //重映射为USART2
	GPIO_Pin_Lock_Config (GPIOA_SFR ,GPIO_PIN_MASK_8, TRUE);                   //配置锁存
	GPIO_Pin_Lock_Config (GPIOA_SFR ,GPIO_PIN_MASK_11, TRUE);                  //配置锁存
}
/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}



