/****************************************************************************************
 * 文件名: main.c
 * 项目名: I2C_Slave_Mode
 * 版 本:  V2.4
 * 日 期:  2021年7月29日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了I2C从机通信功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
#include "I2C_Slave.h"
#include "Usart.h"


#define BUFFER_SIZE_Read     10   			  //接收数据个数
uint8_t i2c_buffer_read[BUFFER_SIZE_Read]={0};//读取到I2C数据后存储到数组

volatile uint8_t I2C_RecNum = 0;                 //接收到第几个数据的变量

/*从机模式用于和读到I2C地址、数据的数组对比,*/
uint8_t i2c_buffer_read_test[BUFFER_SIZE_Read] = {
	0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA
};

extern volatile uint8_t i2c_stop_flg;  //i2c stop 软件标志（说明一帧数据发完）


/*延时函数 局部变量用volatile声明，否则可能会被优化*/
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=2000;
		while(j--);
	}

}


/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
	GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平
}


/**
  * 描述   I2C数据接收到数组
  * 输入   Rev_Temp:  8位数据
  * 返回   无
  */
void I2C_receive_input(uint8_t Rev_Temp)
{
	if(I2C_RecNum < sizeof(i2c_buffer_read))
    {
		i2c_buffer_read[I2C_RecNum] = Rev_Temp;
		I2C_RecNum++;
    }
}


/**
  * 描述  对比I2C的数组是否与读到数组数据匹配
  * 输入   Rev_Temp:  8位数据
  * 返回   无
  */
uint8_t I2C_Data_test(void)
{

	volatile uint16_t i;
    for(i = 0;i < BUFFER_SIZE_Read;i++)
    {
        if(i2c_buffer_read[i] != i2c_buffer_read_test[i])
        {
           return RESET;
        }
    }

    return SET;
}


/*
* 用户可参考该例程在KF32A151_demo板上实现使用I2C主从功能。
* 例程中使用I2C1 PB1 PB2 用于从机I2C通信。
* 功能简述:	1.测试I2C Slave功能，将主机发送过来10个数据读出来，并作数据对比如果匹配PB13翻转，D4灯闪烁。
* 			2.测试I2C Slave功能，主机可以读取数据
* 硬件说明： 使用两块KF32A151demo对接，PB1与PB1对接，PB2与PB2对接；
*           PB13对应控制D4灯
*           如果需要用串口接USART0_RX PA0 	USART0_TX1	PA2  并在Usart.h打开 串口宏定义Usart_Print  1
*
*/
void main()
{
	/*系统时钟初始化,系统时钟120M,外设高频时钟16M*/
	SystemInit();

	/*初始化PB13为输出高电平*/
	GPIO_Reset(GPIOB_SFR);
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);

	I2C_GPIO_init(); //I2C GPIO重映射SCL:PB1   SDA:PB2
	I2C_init_Slave(I2C1_SFR,I2C_SLAVE_ADDRESS);	  //配置I2C1 从机7位地址0XA0

#if Usart_Print
	//配置USART0引脚重映射，	//USART0_RX	PA0  //USART0_TX1 PA2
	GPIO_USART();
	USART_Async_config(USART0_SFR);//全双工异步8bit 115200波特率
#endif
	while(1)
    {
		if(i2c_stop_flg)
		{
			i2c_stop_flg = 0; //清除软件stop标志
#if Usart_Print
			//串口打印接收到的数组数据
			USART_Send(USART0_SFR,i2c_buffer_read,sizeof(i2c_buffer_read));
#endif
			if(I2C_OK == I2C_Data_test())//判断读到I2C数组与预设的数据是否匹配。
			{
				GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//匹配后PB13翻转
			}
		}

    }

}


/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	  while(1)
	  {
		  ;
	  }
}

