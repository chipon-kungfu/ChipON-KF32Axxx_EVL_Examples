/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了串口同步主机收发应用例程参考
  *
 ****************************************************************************************/
#include "system_init.h"
#include "Usart.h"
//ChipON_KF32A151 ASCII
uint8_t USART_Array_Tansmit[]={0x43,0x68,0x69,0x70,0x4F,0x4E,0x5F,0x4B,0x46,0x33,0x32,0x41,0x31,0x35,0x31,0x0D,0x0A};

void GPIO_USART1();//USART1引脚重映射

/**
  * 描述    程序延时
  * 输入   a：延时参数
  *      b：延时参数
  * 返回   无
  */
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	uint32_t i;
   /* 用户可参考该例程实现USART1的半双工同步发送功能，波特率9600，配合USART_Sync_Slave同
    * 步串口从机接收例程（TX接TX，RX接RX），可实现同步串口主从收发功能，该例程主模式发送
    * “ChipON_KF32A151”*/

	//系统时钟120M,外设高频时钟16M
	SystemInit();
	//配置USART1引脚重映射，PB0-RX，PB1-TX0
	GPIO_USART1();
	//半双工同步（主模式发送，9bit 9600波特率）
	USART_Sync_config(USART1_SFR);

	while(1)
	{
		for(i=0;i<500;i++)
		{
			Delay(500);//配合USART_Sync_Slave上电同步（实际应用建议使用同步字符方式处理）
		}
    	//发送"ChipON_KF32A151"
    	USART_Send(USART1_SFR,USART_Array_Tansmit,sizeof(USART_Array_Tansmit));
	}
}

/**
  * 描述    USART1引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART1()
{
	/* 端口重映射AF5 */
	//USART1_RX		PB0
	//USART1_TX0	PB1
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_0, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_1, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);	  //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_1, GPIO_RMP_AF5_USART1);     //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_0, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_1, TRUE);                  //配置锁存
}
/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}



