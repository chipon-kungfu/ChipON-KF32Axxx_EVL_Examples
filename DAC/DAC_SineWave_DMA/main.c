/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: DAC_SineWave_DMA
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了DAC正弦波输出应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"

#define DAC0_DAHD_Address      0x40000704      //DAC0_DAHD的寄存器地址
#define DAC1_DAHD_Address      0x40000784      //DAC1_DAHD的寄存器地址
#define TriggerEINT9_ENABLE     0             //触发源中断EINT9 开关 1：开 0：关
//DMA使用变量
DMA_SFRmap * DMA_CHOOSE;


// 用于输出DAC数据数组
const uint16_t Sine12bit[32] = {
                      2047, 2447, 2831, 3185, 3498, 3750, 3939, 4056, 4094, 4056,
                      3939, 3750, 3495, 3185, 2831, 2447, 2047, 1647, 1263, 909,
                      599, 344, 155, 38, 0, 38, 155, 344, 599, 909, 1263, 1647};


uint32_t DualSine12bit[32];//生成正弦波数组

uint32_t Idx = 0;




/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{


	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	 //先设置为高电平

}

#if TriggerEINT9_ENABLE
/**
  * 描述  GPIOx 输入初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Input_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{


	/* 配置 Pxy作为输入模式 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Mode = GPIO_MODE_IN;                      //初始化 GPIO方向为输入
	GPIO_InitStructure.m_PullUp = GPIO_PULLUP;                     //初始化 GPIO上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;                   //初始化 GPIO不下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);
}

/**
  * 描述  外部中断(EINT)中断源配置
  * 输入 :  EintNum: 外部中断编号，
  *              取值为宏INT_EXTERNAL_INTERRUPT_0至
  *             INT_EXTERNAL_INTERRUPT_31中的一个 即0~15。
  *      PeripheralSource: 外设中断线的中断输入源， INT_EXTERNAL_SOURCE_PA~INT_EXTERNAL_SOURCE_PF
  * 返回  无。
  */
void EXIT_INTx_Config(uint32_t EintNum,uint32_t PeripheralSource)
{
	EINT_InitTypeDef EXIT_InitStructure;
	INT_External_Struct_Init(&EXIT_InitStructure);
	EXIT_InitStructure.m_Line = EintNum;                   //设置外部中断编号
	EXIT_InitStructure.m_Mask = TRUE;                      //设置外部中断使能控制
	EXIT_InitStructure.m_Rise = FALSE;                     //设置外部中断上升沿中断不使能
	EXIT_InitStructure.m_Fall = TRUE;                      //设置外部中断下降沿中断使能
	EXIT_InitStructure.m_Source = PeripheralSource;        //设置外部中断的中断源选择
	INT_External_Configuration(&EXIT_InitStructure);
	INT_External_Source_Enable(EintNum,PeripheralSource);


    if(EintNum==INT_EXTERNAL_INTERRUPT_0)
    {
    	INT_Interrupt_Enable(INT_EINT0,TRUE);								//使能中断EINT0
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_1)
    {
    	INT_Interrupt_Enable(INT_EINT1,TRUE);								//使能中断EINT1
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_2)
    {
    	INT_Interrupt_Enable(INT_EINT2,TRUE);								//使能中断EINT2
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_3)
    {
    	INT_Interrupt_Enable(INT_EINT3,TRUE);								//使能中断EINT3
    }else if(EintNum==INT_EXTERNAL_INTERRUPT_4)
    {
    	INT_Interrupt_Enable(INT_EINT4,TRUE);								//使能中断EINT4
    }else if((EintNum>=INT_EXTERNAL_INTERRUPT_5)&&(EintNum<=INT_EXTERNAL_INTERRUPT_9))
    {
    	INT_Interrupt_Enable(INT_EINT9TO5,TRUE);								//使能中断EINT5~EINT9
    }else if((EintNum>=INT_EXTERNAL_INTERRUPT_10)&&(EintNum<=INT_EXTERNAL_INTERRUPT_15))
    {
    	INT_Interrupt_Enable(INT_EINT15TO10,TRUE);								//使能中断EINT10~EINT15
    }

	INT_All_Enable (TRUE);      //使能总中断
}
#endif


/**
  * 描述  DACx 初始化配置并选择触发源。
  * 输入 :  DACx: 指向DAC内存结构的指针，取值为DAC0_SFR、DAC1_SFR。
   TriggerEvent: 触发事件选择：     DAC_TRIGGER_T1_TRGO
	*					        DAC_TRIGGER_T3_TRGO
	*							DAC_TRIGGER_T5_TRGO
	*							DAC_TRIGGER_T9_TRGO
	*							DAC_TRIGGER_T14_TRGO
	*							DAC_TRIGGER_T15_TRGO
	*							DAC_TRIGGER_EINT9_TRGO
	*							DAC_TRIGGER_SOFTWARE_TRGO
	*							DAC_TRIGGER_T0_TRGO
	*							DAC_TRIGGER_T2_TRGO
	*							DAC_TRIGGER_T4_TRGO
	*							DAC_TRIGGER_T18_TRGO
	*							DAC_TRIGGER_T19_TRGO
	*							DAC_TRIGGER_T20_TRGO
	*							DAC_TRIGGER_T21_TRGO
  * 返回  无。
  */
void DAC_Init_Config(DAC_SFRmap* DACx, uint32_t TriggerEvent)
{
//DAC选用内部参考电压并选主时钟源
	static uint32_t ReferenceVoltage=0;

	DAC_Reset(DACx);    //复位DAC，并使能DAC时钟
	DAC_InitTypeDef DAC_InitStructure;
	DAC_Struct_Init(&DAC_InitStructure);
	DAC_InitStructure.m_TriggerEnable=TRUE;               //DAC通道触发使能
	DAC_InitStructure.m_TriggerEvent = TriggerEvent;      //DAC通道触发事件
	DAC_InitStructure.m_TriggerDMAEnable = FALSE;         //先关DAC触发DMA
	DAC_InitStructure.m_Wave = DAC_WAVE_NOISE;            //关闭波形发生器
	DAC_InitStructure.m_Mas = DAC_LFSR_UNMASK_BITS0_0;    //幅值选择
	DAC_InitStructure.m_Clock = DAC_CLK_SCLK;             //DAC工作时钟源SCLK
	DAC_InitStructure.m_ReferenceVoltage = DAC_RFS_2V;    //DAC参考电压
	DAC_InitStructure.m_OutputBuffer = FALSE;             //禁止输出缓冲
	if(DACx==DAC0_SFR)
	{
		DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_1;     //DAC0 引脚为 DAC_OUTPUT_PIN_1 PA4
	}else
	{
		DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_0;     //DAC1 引脚为 DAC_OUTPUT_PIN_0 PA0
	}
	DAC_InitStructure.m_Output = 4094;                    //初始化输出数据
    DAC_Configuration(DACx,&DAC_InitStructure);

    ReferenceVoltage =DAC_InitStructure.m_ReferenceVoltage;

	if((ReferenceVoltage = DAC_RFS_2V)||(ReferenceVoltage = DAC_RFS_1P2V))   //判断是否使用内部参考电压
	{
		OSC_Backup_Write_Read_Enable (TRUE);               //允许配置备份区寄存器读写
		PM_Reference_Voltage_Enable (TRUE);                //使能参考电压模块
	}

}

/**
  * 描述  配置DMA及通道初始化参数。
  * 输入 : DMAx: 指向DMA内存结构的指针，取值为DMA0_SFR和DMA1_SFR。
  *       Channel: DMA通道，取值为DMA_CHANNEL_1~DMA_CHANNEL_7
  * 返回  无。
  */
void DAC_DMA_Config(DMA_SFRmap * DMA_CHOOSE,uint8_t Channel)
{


	DMA_InitTypeDef dmaNewStruct;

	    /* DMA功能参数配置 */

		/* 配置 传输数据个数: 32 */
		dmaNewStruct.m_Number = 32;
		/* 配置 DMA传输方向：外设到内存 */
		dmaNewStruct.m_Direction = DMA_MEMORY_TO_PERIPHERAL;
		/* 配置 DMA通道优先级：高优先级 */
		dmaNewStruct.m_Priority = DMA_CHANNEL_HIGHER;
		/* 配置 外设数据位宽：32位宽 */
		dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
		/* 配置 存储器数据位宽:32位宽 */
		dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
		/* 配置 外设地址增量模式使能: 禁止 */
		dmaNewStruct.m_PeripheralInc = FALSE;
		/* 配置 存储器地址增量模式使能: 允许 */
		dmaNewStruct.m_MemoryInc = TRUE;
		/* 配置 DMA通道选择:通道 */
		dmaNewStruct.m_Channel = Channel;   //通道配置
		/* 配置 数据块传输模式： */
		dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;//字节传输
		/* 配置 循环模式使能: 允许 */
		dmaNewStruct.m_LoopMode = TRUE;
		/* 配置 外设起始地址：等待发送的数据的起始地址 */
		if(Channel==DMA_CHANNEL_5)
		{
			dmaNewStruct.m_PeriphAddr = DAC0_DAHD_Address ;         //外设地址为DAC0地址
		}else if(Channel==DMA_CHANNEL_1)
		{
			dmaNewStruct.m_PeriphAddr = DAC1_DAHD_Address ;         //外设地址为DAC1地址
		}

		/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
		dmaNewStruct.m_MemoryAddr = (uint32_t)&DualSine12bit;

		DMA_Configuration (DMA_CHOOSE,&dmaNewStruct);

		/* 使能通道  DMA */
		DMA_Channel_Enable (DMA_CHOOSE, Channel, TRUE);
}



/**
  * 描述  通用定时器初始化参数。
  * 输入 : GPTIMx: 指向定时器内存结构的指针，
  *               取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *               T19_SFR/T20_SFR/T21_SFR。
  *       Period：新的周期值，T20 T21取值32位数据。其他定时器取值 为16位数据
  * 返回  无。
  */
void GENERAL_TIMER_Tx_Config(GPTIM_SFRmap* GPTIMx,uint32_t Period)
{
    //定时器时钟源选用SCLK,不分频，TXIF作为触发信号

	TIM_Reset(GPTIMx);												//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);					//立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);								//配置更新使能
	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);				//定时模式选择

	GPTIM_Master_Mode_Config(GPTIMx,GPTIM_MASTER_TXIF_SIGNAL);      //配置主模式选择TXIF作为触发

	GPTIM_Set_Counter(GPTIMx,0);									//定时器计数值
	GPTIM_Set_Period(GPTIMx,Period);								//定时器周期值
	GPTIM_Set_Prescaler(GPTIMx,0);				    				//定时器预分频值1：1
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_DOWN_OUF);		//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_SCLK);							//选用SCLK时钟
	GPTIM_Cmd(GPTIMx,TRUE);											//定时器启动控制使能

}

//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现DAC1 输出正弦波配置功能。
	    例程中用DAC0、DAC1输出正弦波，触发源选择定时器T20、T21触发应用,
	 *如果用中断EINT9触发需要打开TriggerEINT9_ENABLE中断使能,并且在PB9输入下降沿触发中断
     *挂示波器可以测量到PA0、PA4为正弦波
     *注意选用PA0、PA4作为DAC不能再重映射其他功能，用内部参考电压时要打开备份域读写功能
	 */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化
	GPIO_Reset(GPIOA_SFR);//复位GPIOA，并开启GPIOA时钟DAC IO输出时钟
	/*初始化复位GPIOB外设，使能GPIOB外设时钟*/
	GPIO_Reset(GPIOB_SFR);
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//PB8初始化输出高电平

#if TriggerEINT9_ENABLE
	GPIOInit_Input_Config(GPIOB_SFR,GPIO_PIN_MASK_9);                 //PB9初始化为输入
	//外部中断EINT9 中断源PB9配置
	EXIT_INTx_Config(INT_EXTERNAL_INTERRUPT_9,INT_EXTERNAL_SOURCE_PB);  //下降沿信号触发中断EINT9
	DAC_Init_Config(DAC0_SFR,DAC_TRIGGER_EINT9_TRGO);//初始化DAC0参数，并选择触发源EINT9
#else
	GENERAL_TIMER_Tx_Config(T20_SFR,200);    //配置定时器T20，并设置其周期值200
	GENERAL_TIMER_Tx_Config(T21_SFR,100);    //配置定时器T21，并设置其周期值100
	DAC_Init_Config(DAC0_SFR,DAC_TRIGGER_T20_TRGO);//初始化DAC0参数，并选择触发源T20
	DAC_Init_Config(DAC1_SFR,DAC_TRIGGER_T21_TRGO);//初始化DAC1参数，并选择触发源T21
#endif
	/* 装载 正弦波32个数据 */
	for (Idx = 0; Idx < 32; Idx++)
	{
	  DualSine12bit[Idx] = (Sine12bit[Idx] << 16) + (Sine12bit[Idx]);
	}
	DMA_Reset(DMA0_SFR);// DMA复位 ,并开启DMA时钟
	DAC_DMA_Config(DMA0_SFR,DMA_CHANNEL_1);  //DAC1 DMA配置
	DAC_DMA_Cmd(DAC1_SFR,TRUE);//DMA使能
	DAC_Cmd(DAC1_SFR,TRUE);//DAC1使能

	DAC_DMA_Config(DMA0_SFR,DMA_CHANNEL_5);  //DAC0 DMA配置
	DAC_DMA_Cmd(DAC0_SFR,TRUE);//DMA使能
	DAC_Cmd(DAC0_SFR,TRUE);//DAC0使能

    while(1)
    {
    }

}



/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

