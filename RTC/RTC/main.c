/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  RTC时间通过串口输出，RTC设置节拍中断、秒中断、闹钟中断
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "RTC_user.h"

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit(); //在操作备份域之前，需要将系统时钟降低到48M
	OSC_LFCK_Source_Config(LFCK_INPUT_INTLF); //LFCK时钟源选择内部低频晶振
	OSC_LFCK_Enable(TRUE); //LFCK使能，准备为RTC提供时钟

	GPIO_Write_Mode_Bits (GPIOB_SFR ,GPIO_PIN_MASK_13 ,GPIO_MODE_OUT); //LED灯引脚设置为输出

	//配置USART2引脚重映射
	GPIO_USART();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);

	Set_rtc_time(); //设置初始时间
	Set_rtc_Alarm(); //设置闹钟
	Set_rtc_int();  //配置中断

	while(1)
	{
		Delay(0xFFFFF);
		Print_time_to_uart(); //把RTC时间通过串口发出来
	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
