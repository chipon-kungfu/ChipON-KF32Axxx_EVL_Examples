/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了ADC连续转换的使用方法。ADC使用软件触发后，连续转换，不会停止。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "DMA_user.h"
#include "ADC_user.h"
unsigned int buf_1[32] ={0};

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	uint16_t buf_16 = 0;

	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13,GPIO_MODE_OUT);  //STATUS灯配置为输出模式
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_1,GPIO_MODE_AN);//ADC_CHANNEL_1

	//配置USART2引脚重映射
	GPIO_USART();
	//全双工异步8bit 115200波特率
	USART_Async_config(USART2_SFR);

	Init_dma(); //dma配置
	Init_adc(); //配置ADC0位单通道、单次采样、关闭外部触发

	ADC_Regular_Channel_DMA_Cmd(ADC0_SFR,TRUE); //使能ADC的DMA功能
	cfg_dma_intrupt(); //配置dma的中断
	DMA_Channel_Enable(DMA0_SFR, DMA_CHANNEL_5, TRUE); //DMA0_ch5 对应AD0
	INT_All_Enable(TRUE);//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

	ADC_Software_Start_Conv(ADC0_SFR); //软件触发转换

	while(1)
	{
		Delay(0xFFFFF);
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13); //状态灯翻转
	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
