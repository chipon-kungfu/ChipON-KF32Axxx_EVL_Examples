/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"

extern volatile uint32_t PWM_VLAUE;
extern volatile uint32_t HUXI_COUNT;
volatile uint32_t time_T18;




//*****************************************************************************************
//                              T18中断函数
//*****************************************************************************************
void __attribute__((interrupt))_T18_exception (void)
{
	if(GPTIM_Get_Overflow_INT_Flag(T18_SFR)!=RESET)
	{
		GPTIM_Clear_Updata_INT_Flag(T18_SFR);										//清更新时间标志位
		GPTIM_Clear_Overflow_INT_Flag (T18_SFR);									//清T18溢出中断标志位
		time_T18++;
		HUXI_COUNT++;
		if(time_T18 ==PWM_VLAUE)
		{
			GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_SET);		    //PB13设置为高电平

		}
		if(time_T18==100)
		{
			time_T18 =0;
			GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_RESET);		    //PB13设置为低电平

		}

	}



}


