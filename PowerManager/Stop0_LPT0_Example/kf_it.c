/****************************************************************************************
 *
 * File Name: kf_it.c
 * Project Name: Stop0_Wakeup_Example_ok
 * Version: v1.0
 * Date: 2022-03-20- 22:26:01
 * Author:
 * 
 ****************************************************************************************/
//#include<KF32A151MQV.h>
#include "system_init.h"
//asm(".include		\"KF32A151MQV.inc\"	");	 

//Note:
//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	

void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}
//*****************************************************************************************
//                           _T0_exception Interrupt Course  
//*****************************************************************************************

void __attribute__((interrupt)) _T0_exception (void)
{
	if(T0_SFR->CTL1 & GPTIM_CTL1_TXIF)
	{
		/*Clear Timer0 Overflow INT Flag*/
		SFR_SET_BIT_ASM(T0_SFR->CCPXSRIC, CCP_SRIC_TXIC_POS);
		while((T0_SFR->CTL1 & GPTIM_CTL1_TXIF)>>GPTIM_CTL1_TXIF_POS);
		SFR_CLR_BIT_ASM(T0_SFR->CCPXSRIC, CCP_SRIC_TXIC_POS);
	}
}

//*****************************************************************************************
//                           _T0_exception Interrupt Course  
//                            For Stop0 only
//*****************************************************************************************

void __attribute__((interrupt))__attribute__((section(".indata"))) _Stop0_T0_exception (void)
{
	if(T0_SFR->CTL1 & GPTIM_CTL1_TXIF)
	{
		/*Clear Timer0 Overflow INT Flag*/
		SFR_SET_BIT_ASM(T0_SFR->CCPXSRIC, CCP_SRIC_TXIC_POS);
		while((T0_SFR->CTL1 & GPTIM_CTL1_TXIF)>>GPTIM_CTL1_TXIF_POS);
		SFR_CLR_BIT_ASM(T0_SFR->CCPXSRIC, CCP_SRIC_TXIC_POS);
	}
}

//*****************************************************************************************
//                             _WKP5TO0_exception Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _WKP5TO0_exception (void)
{
	BKP_Write_And_Read_Enable(TRUE);
	if(PM_STA1 & PM_STA1_WKP4STA)
	{
		/* Clear WKUP flag. */
		PM_STAC |= PM_STA1_WKP4STA;
		while(PM_STA1 & PM_STA1_WKP4STA);
		PM_STAC &= ~PM_STA1_WKP4STA;

		/* Clear WKUP interrupt flag. */
		INT_EIF2 &= ~INT_EIF2_WKPIF;

	}
	BKP_Write_And_Read_Enable(FALSE);
}

//*****************************************************************************************
//                             _WKP5TO0_exception Interrupt Course
//                             For Stop0 only
//*****************************************************************************************
void __attribute__((interrupt)) __attribute__((section(".indata"))) _Stop0_WKP5TO0_exception (void)
{
	if(PM_STA1 & PM_STA1_WKP4STA)
	{
		/* Disable WKUP Interrupt */
    	INT_EIE2 &= ~(1u << 25u);

		/* Clear WKUP flag. */
		PM_STAC |= PM_STA1_WKP4STA;
		while(PM_STA1 & PM_STA1_WKP4STA);
		PM_STAC &= ~PM_STA1_WKP4STA;

		/* Clear WKUP interrupt flag. */
		INT_EIF2 &= ~INT_EIF2_WKPIF;

	}
}

//*****************************************************************************************
//                             _WKP19TO17_exception Interrupt Course  PMC INT
//*****************************************************************************************
void __attribute__((interrupt)) _EINT19TO17_exception (void)
{
	if(INT_EINTF & (1 << INT_EXTERNAL_INTERRUPT_19))
	{
		/* Clear EINT interrupt flag. */
		INT_EINTF &= ~(1 << INT_EXTERNAL_INTERRUPT_19);
		/* Clear EINT19TO17 interrupt flag. */
		INT_EIF2 &= ~INT_EIF2_EINT19TO17IF;

		BKP_Write_And_Read_Enable(TRUE);
		/* Clear PMC error interrupt flag. */
		PM_CTL2 |= (1 << 18);
		while((PM_STA1 & (1<<18)) || (PM_STA1 & (1<<19)));
		/* No clear PMC error interrupt flag. */
		PM_CTL2 &= ~(1 << 18);
		BKP_Write_And_Read_Enable(FALSE);
	}
	//USER Code
}

//*****************************************************************************************
//                             _WKP19TO17_exception Interrupt Course  
//                             For Stop0 only
//*****************************************************************************************
void __attribute__((interrupt))  __attribute__((section(".indata")))_Stop0_EINT19TO17_exception (void)
{
	if(INT_EINTF & (1 << INT_EXTERNAL_INTERRUPT_19))
	{
		/* Clear EINT interrupt flag. */
		INT_EINTF &= ~(1 << INT_EXTERNAL_INTERRUPT_19);
		/* Clear EINT19TO17 interrupt flag. */
		INT_EIF2 &= ~INT_EIF2_EINT19TO17IF;

		/* Clear PMC error interrupt flag. */
		PM_CTL2 |= (1 << 18);
		while((PM_STA1 & (1<<18)) || (PM_STA1 & (1<<19)));
		/* No clear PMC error interrupt flag. */
		PM_CTL2 &= ~(1 << 18);
	}
	//USER Code
}
