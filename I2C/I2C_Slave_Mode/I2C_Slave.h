/****************************************************************************************
 * 文件名: I2C_Slave.h
 * 项目名: I2C_Slave_Mode
 * 版 本:  V2.4
 * 日 期:  2021年7月29日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述
 *
 ****************************************************************************************/

#ifndef I2C_Slave_H_
#define I2C_Slave_H_


#include "system_init.h"

#define I2C_OK                 1
#define I2C_FAIL               0
#define I2C_SLAVE_ADDRESS      0xA0   //设置接收匹配的I2C 7位地址

void I2C_GPIO_init(void);
void I2C_init_Slave(I2C_SFRmap* I2Cx,uint16_t I2C_ADDRESS);

#endif /* I2C_Slave.h */
