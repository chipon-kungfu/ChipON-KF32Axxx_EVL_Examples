/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了ADC单次采样的用法。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "ADC_user.h"
char flag =0;
unsigned short buf_16 =0;

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13,GPIO_MODE_OUT);  //STATUS灯配置为输出模式
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_2,GPIO_MODE_AN);//ADC_CHANNEL_2
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_1,GPIO_MODE_AN);//ADC_CHANNEL_1

	//配置USART3引脚重映射，PB14_Tx，PB15-RX
	GPIO_USART();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);

	Init_adc(); //配置ADC0位单通道、单次采样、关闭外部触发
	while(1)
	{
		if(flag)
		{
			ADC_Regular_Channel_Config (ADC0_SFR, ADC_CHANNEL_2, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_2
			flag =0;
		}
		else
		{
			ADC_Regular_Channel_Config (ADC0_SFR, ADC_CHANNEL_1, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_1
			flag =1;
		}

		ADC_Software_Start_Conv(ADC0_SFR);		//软件触发转换
		while(!ADC_Get_INT_Flag(ADC0_SFR,ADC_INT_EOC)); //等待转换结束，
		buf_16 =ADC_Get_Conversion_Value(ADC0_SFR); //读取ADC数据寄存器,同时硬件会自动清零EOCIF

		//串口发送ADC采样结果高4位
		USART_SendData(USART2_SFR,(unsigned char)(buf_16>>8));
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USART2_SFR));
		//串口发送ADC采样结果低8位
		USART_SendData(USART2_SFR,(unsigned char)buf_16);
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USART2_SFR));

		Delay(0xFFFFF);
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13); //状态灯翻转
	}		
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
