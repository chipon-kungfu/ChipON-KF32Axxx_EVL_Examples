/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: CCPx_Capture_Mode
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了CCP捕捉模式应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"

volatile uint16_t UP_PLUSE_WIDTH=0;//捕捉到PWM占空比变量
volatile uint16_t DW_PLUSE_WIDTH=0;//捕捉到PWM周期变量



//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=5000;
		while(j--);
	}

}



/**
  * 描述  CCPx 捕捉功能重映配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *     GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  *    PinRemap: 引脚重映射选择
  * 返回  无。
  */
void CCPx_Capture_GPIO_Init(GPIO_SFRmap* GPIOx,uint16_t GpioPin,uint8_t PinRemap)
{
	 static uint16_t GpioPinNum;


//	//////////////////////////////////////////////////////////////
	GPIO_Write_Mode_Bits(GPIOx,GpioPin,GPIO_MODE_RMP);  //配置GPIO为重映射功能

	if(GpioPin==GPIO_PIN_MASK_0)
	{
		GpioPinNum =GPIO_Pin_Num_0;
	}else if(GpioPin==GPIO_PIN_MASK_1)
	{
		GpioPinNum =GPIO_Pin_Num_1;
	}else if(GpioPin==GPIO_PIN_MASK_2)
	{
		GpioPinNum =GPIO_Pin_Num_2;
	}else if(GpioPin==GPIO_PIN_MASK_3)
	{
		GpioPinNum =GPIO_Pin_Num_3;
	}else if(GpioPin==GPIO_PIN_MASK_4)
	{
		GpioPinNum =GPIO_Pin_Num_4;
	}else if(GpioPin==GPIO_PIN_MASK_5)
	{
		GpioPinNum =GPIO_Pin_Num_5;
	}else if(GpioPin==GPIO_PIN_MASK_6)
	{
		GpioPinNum =GPIO_Pin_Num_6;
	}else if(GpioPin==GPIO_PIN_MASK_7)
	{
		GpioPinNum =GPIO_Pin_Num_7;
	}else if(GpioPin==GPIO_PIN_MASK_8)
	{
		GpioPinNum =GPIO_Pin_Num_8;
	}else if(GpioPin==GPIO_PIN_MASK_9)
	{
		GpioPinNum =GPIO_Pin_Num_9;
	}else if(GpioPin==GPIO_PIN_MASK_10)
	{
		GpioPinNum =GPIO_Pin_Num_10;
	}else if(GpioPin==GPIO_PIN_MASK_11)
	{
		GpioPinNum =GPIO_Pin_Num_11;
	}else if(GpioPin==GPIO_PIN_MASK_12)
	{
		GpioPinNum =GPIO_Pin_Num_12;
	}else if(GpioPin==GPIO_PIN_MASK_13)
	{
		GpioPinNum =GPIO_Pin_Num_13;
	}else if(GpioPin==GPIO_PIN_MASK_14)
	{
		GpioPinNum =GPIO_Pin_Num_14;
	}else if(GpioPin==GPIO_PIN_MASK_15)
	{
		GpioPinNum =GPIO_Pin_Num_15;
	}

	GPIO_Pin_RMP_Config(GPIOx,GpioPinNum,PinRemap);  //配置GPIO重映射外设功能
}

/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}



/**
  * 描述  CCPx捕捉功能配置初始化参数。
  * 输入 : CCPx: 指向CCP或通用定时器内存结构的指针，
  *               取值CCP0_SFR/CCP1_SFR/CCP2_SFR/CCP3_SFR/CCP4_SFR/
  *               CCP18_SFR/CCP19_SFR/CCP20_SFR/CCP21_SFR/CCP22_SFR/CCP23_SFR。
  * 返回  无。
  */
void CCPx_Capture_Mode_init(CCP_SFRmap* CCPx)
{
	/*设置定时器的预分频值 以及捕捉通道的模式*/
	TIM_Reset(CCPx);										//定时器外设复位，使能外设时钟
	CCP_PWM_Input_Measurement_Config(CCPx,TRUE);            //PWM输入测量模式使能
	GPTIM_Slave_Mode_Config(CCPx,GPTIM_SLAVE_RESET_MODE);   //设置从模式：复位模式
	GPTIM_Trigger_Select_Config(CCPx,GPTIM_TRIGGER_CCPXCH1);  //选择触发源为CH1
	CCP_Capture_Mode_Config(CCPx, CCP_CHANNEL_1,CCP_CAP_FALLING_EDGE);      ///设置捕捉通道 模式:每个下降沿发生捕捉
	CCP_Capture_Mode_Config(CCPx, CCP_CHANNEL_2,CCP_CAP_RISING_EDGE);       ///设置捕捉通道 模式:每个上升沿发生捕捉


	GPTIM_Updata_Immediately_Config(CCPx,TRUE);				//立即更新控制
	GPTIM_Updata_Enable(CCPx,TRUE);							//配置更新使能
	GPTIM_Work_Mode_Config(CCPx,GPTIM_TIMER_MODE);			//定时模式选择
	GPTIM_Set_Counter(CCPx,0);								//定时器计数值

	GPTIM_Set_Prescaler(CCPx,119);							//定时器预分频值 预分频为119+1=120分频，主时钟120M,1us计数一次
	GPTIM_Counter_Mode_Config(CCPx,GPTIM_COUNT_UP_OF);		//向上,上溢产生中断标志
	GPTIM_Clock_Config(CCPx,GPTIM_SCLK);					//选用SCLK时钟为定时器时钟源
	GPTIM_Cmd(CCPx,TRUE);                                   //使能通用定时器

}





//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现捕捉模式功能配置。
	 * 当对应的 CCPxCHy 引脚发生事件时，CCPx_Cy 寄存器捕捉 Tx 寄存器的值。
	 * 例程中使用CCP0模块实现捕捉模式1KHZ 占空比为50%的PWM信号，在PE1 口输入1KHZ的PWM信号，
	 * CCP0通道1捕捉到正确的1KHZ PWM周期信号后PB13翻转,DEMO板上D4灯闪烁,映射到通道2捕捉正确的占空比PB14翻转
	 * 注：软件上可以选择触发源，因此选择了触发源为CH1时可以映射功能到CH2，不需要再输入信号到CH2,就可以触发CH2捕捉功能
	 */
	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//初始化PB13、PB14输出高电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13|GPIO_PIN_MASK_14);
//	 //重映射捕捉输入IO设置  PE1为CCP0_CH1   PE2为CCP0_CH2
	GPIO_Reset(GPIOE_SFR);            //复位GPIOE，并打开GPIOE时钟
	CCPx_Capture_GPIO_Init(GPIOE_SFR,GPIO_PIN_MASK_1,GPIO_RMP_AF1_T0);  //PE1重映射为捕捉
	CCPx_Capture_GPIO_Init(GPIOE_SFR,GPIO_PIN_MASK_2,GPIO_RMP_AF1_T0);

	//CCP0 通道 1 与通道2 捕捉模式初始化，配置通道1触发源
	CCPx_Capture_Mode_init(CCP0_SFR);// 主时钟120M,1us计数一次

    while(1)
    {
    	delay_ms(200);
    	delay_ms(200);

		//输入1KHZ PWM信号的计数
    	UP_PLUSE_WIDTH	= CCP_Get_Capture_Result(CCP0_SFR,CCP_CHANNEL_1);			//捕捉占空比
		if((UP_PLUSE_WIDTH >480)&&(UP_PLUSE_WIDTH<520))                             //1KHZ的PWM信号，占空比为50%
			GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);              //PB13翻转 D4闪烁

		DW_PLUSE_WIDTH	= CCP_Get_Capture_Result(CCP0_SFR,CCP_CHANNEL_2);			//捕捉周期
		if((DW_PLUSE_WIDTH >980)&&(DW_PLUSE_WIDTH<1020))                            //1KHZ的PWM信号计数值
			GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_14);              //PB14翻转
    }


}




void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

