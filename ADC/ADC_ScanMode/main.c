/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了ADC单次采样的用法。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "ADC_user.h"
#include "stdio.h"
#include "DMA_user.h"

#define ADC_DMA_ENABLE

uint32_t ADC_buf[2];

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	uint8_t flag =0;

	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13,GPIO_MODE_OUT);  //STATUS灯配置为输出模式

	//配置USART引脚重映射，PB14_Tx，PB15-RX
	GPIO_USART();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);

	Init_adc(); //配置ADC位单通道、单次采样、关闭外部触发
	ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_1, 0x01); //ADC常规通道1的采样源设置为ADC_CHANNEL_1
	ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_2, 0x02); //ADC常规通道1的采样源设置为ADC_CHANNEL_2
#ifdef ADC_DMA_ENABLE
	Init_dma(); //dma配置
	ADC_Regular_Channel_DMA_Cmd(ADC_UNIT,TRUE); //使能ADC的DMA功能
	cfg_dma_intrupt();
#else
	Init_adc_INT();
#endif
	INT_All_Enable (TRUE);//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断
	ADC_Software_Start_Conv(ADC_UNIT);		//软件触发转换
	while(1)
	{
		Delay(0xFFFFF);
		Delay(0xFFFFF);
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13); //状态灯翻转
		ADC_Software_Start_Conv(ADC_UNIT);		//单次转换,软件触发转换
	}		
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
