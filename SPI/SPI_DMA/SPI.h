/**
  ******************************************************************************
  * 文件名  SPI.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了SPI DMA的相关功能使用的变量定义与声明
  *
  ******************************************************************************/
#ifndef _SPI_H_
#define _SPI_H_

#define  SPI_MASTER  0   //SPI模式选择，1=主模式
#define  Command     0xAA //命令字
#define  Dummy       0x00 //dummy符
#define  BufferSize  37   //Tx长度
#define  SPI_COM     SPI1_SFR//SPI1寄存器入口地址
/* DMA ------------------------------------------------------- */
#define  DMA_COMx    DMA0_SFR
#define  DMA_COMy    DMA1_SFR
#define  DMA_Rx    	 DMA_CHANNEL_4
#define  DMA_Tx      DMA_CHANNEL_3

/* 变量 ------------------------------------------------------- */
extern volatile uint8_t  Command_flag;//命令标志位
extern volatile uint8_t  Dummy_flag;  //dummy标志位
extern volatile uint8_t  Uart_flag;   //串口标志位
extern volatile uint32_t Tx_length;   //发送长度变量
extern volatile uint32_t Rx_length;   //发送长度变量
extern uint8_t Tx_master[BufferSize]; //主机Tx
extern uint8_t Tx_slave[BufferSize];  //从机Tx
extern uint8_t Read_data[BufferSize]; //接收缓存
/* 功能函数 ------------------------------------------------------- */
void SPI_Init_Configuration(SPI_SFRmap* SPIx); //SPI配置
void SPI_DMA_Configuration(SPI_SFRmap* SPIx);  //SPI_DMA配置
#endif /* SPI_H_ */
