/**
  ********************************************************************
  * 文件名  flash.h
  * 作  者   ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2024-04-16
  * 描  述  该文件提供了flash功能的相关读写函数功能定义
  *
  *********************************************************************
*/

#ifndef FLASH_H_
#define FLASH_H_

#include "__driver_Flash_API.h"

void FlashReadNByte(unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t __FlashEraseNPage(volatile unsigned key,unsigned int Address, unsigned int Length);
uint32_t __FlashWriteNKBytes(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t __FlashWrite8NBytesNoErase(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers);

#define FlashEraseNPage(Address,Length)             __FlashEraseNPage(Function_Parameter_Validate, Address, Length)
#define FlashWriteNKBytes(Address,Length,Buffers)           __FlashWriteNKBytes(Function_Parameter_Validate, (Address), (Length), (Buffers))
#define FlashWrite8NBytesNoErase(Address,Length,Buffers)   __FlashWrite8NBytesNoErase(Function_Parameter_Validate, (Address), (Length), (Buffers))

#endif /* FLASH_H_ */
