/**
  ******************************************************************************
  * 文件名  DMA_user.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了DMA用户驱动的源程序
  *
  *********************************************************************
  */
#include "system_init.h"
#include "DMA_user.h"
#ifndef DMA_USER_C_
#define DMA_USER_C_

uint32_t buf_1[20] ={1,2,3,4,5,6,7,8,9,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x20};
uint32_t buf_2[20] ={0};

/**
  * 描述   初始化DMAx，并配置DMAx
  * 输入   DMA_SFRmap *DMAx
  * 返回   无
  */
void DMA_init(DMA_SFRmap *DMAx)
{
	DMA_InitTypeDef dmaNewStruct;
	/* DMA复位 */
	DMA_Reset (DMAx);
	/* DMA功能配置 */

	/* 配置 传输数据个数: 24 */
	dmaNewStruct.m_Number = 20;
	/* 配置 DMA传输方向：外设到内存 */
	dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
	/* 配置 DMA通道优先级：低优先级 */
	dmaNewStruct.m_Priority = DMA_CHANNEL_LOWER;
	/* 配置 外设数据位宽：32位宽 */
	dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
	/* 配置 存储器数据位宽:32位宽 */
	dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
	/* 配置 外设地址增量模式使能: 使能 */
	dmaNewStruct.m_PeripheralInc = TRUE;
	/* 配置 存储器地址增量模式使能: 使能 */
	dmaNewStruct.m_MemoryInc = TRUE;
	/* 配置 DMA通道选择:通道1 */
	dmaNewStruct.m_Channel = DMA_CHANNEL_1;
	/* 配置 数据块传输模式： */
	dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BLOCK;//
	/* 配置 循环模式使能: 禁止 */
	dmaNewStruct.m_LoopMode = TRUE;
	/* 配置 外设起始地址：等待发送的数据的起始地址 */
	dmaNewStruct.m_PeriphAddr = (uint32_t)buf_1 ; //T1
	/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
	dmaNewStruct.m_MemoryAddr = (uint32_t)buf_2;

	/* 配置DMA功能函数 */
	DMA_Configuration (DMAx, &dmaNewStruct); //将参数写入DMAx寄存器
	/* 使能通道1 DMA */
	DMA_Channel_Enable (DMAx, DMA_CHANNEL_1, TRUE);
}

#endif /* DMA_USER_C_ */
