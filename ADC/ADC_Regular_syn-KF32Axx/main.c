/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了ADC常规通道同步触发的使用方法。选择T1的溢出信号作为T1TRGO信号源，配置T1TRGO作为ADC的触发信号。
  * 	每次T1溢出的时候。ADC0和ADC1同步执行一次转换。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ADC_user.h"
#include "DMA_user.h"
#include "GPTIMER_user.h"
#include "USART_user.h"
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13,GPIO_MODE_OUT);  //STATUS灯配置为输出模式
	//配置USART2引脚重映射
	GPIO_USART();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART2_SFR);

	Init_T1();

	/* DMA复位 */
	DMA_Reset (DMA0_SFR); //复位指令
	Init_dma0();	//配置DMA0的ch3
	Init_dma0_it();
	Init_dma1();	//配置DMA0的ch4
	Init_dma1_it();

	INT_Interrupt_Priority_Config(INT_DMA0,3,0);//抢占优先级4,子优先级0
	INT_Interrupt_Enable(INT_DMA0,TRUE);//外设中断使能
	INT_Clear_Interrupt_Flag(INT_DMA0);//清中断标志

	Init_adc0();
	ADC_Regular_Channel_DMA_Cmd(ADC0_SFR,TRUE); //使能ADC的DMA功能

	Init_adc1();
	ADC_Regular_Channel_DMA_Cmd(ADC1_SFR,TRUE); //使能ADC的DMA功能

	ADC_Double_Mode_Config(ADC_REGULAR_SYNC); //不能使用混合模式，高低优先级的通道必须分开

 	Delay(0xFFF);  //ADC使能后需要一段短暂的时间，等待电路充电完毕后，才能去采样，否者可能存在较大的采样误差

 	INT_All_Enable (TRUE);//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断
	while(1)
	{
		Delay(0xFFFFF);
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13); //状态灯翻转
	}		
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
