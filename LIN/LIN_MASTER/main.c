/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了LIN_MASTER参考代码
  *
 ****************************************************************************************/
#include "system_init.h"
#include "lin_master.h"
#define LIN_SLP_N()  GPIO_Set_Output_Data_Bits(GPIOG_SFR,GPIO_PIN_MASK_6,Bit_RESET)//TJA1021休眠
#define LIN_SLP_H()  GPIO_Set_Output_Data_Bits(GPIOG_SFR,GPIO_PIN_MASK_6,Bit_SET)//TJA1021唤醒

void GPIO_LIN();//USART1_LIN引脚重映射

/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程实现LIN_MASTER收发，使用串口兼容LIN模式，，定时器T14每20ms进
	* 入一次中断，循环发送2个LIN标识，LIN的间隔符使用T15定时13个bit,9600波特率*/

	//系统时钟72M,外设高频时钟16M
	SystemInit();

	/*初始化复位GPIOG外设，使能GPIOG外设时钟,LIN_SLP*/
    GPIO_Reset(GPIOG_SFR);
    GPIO_Write_Mode_Bits(GPIOG_SFR,GPIO_PIN_MASK_6,GPIO_MODE_OUT);

	//配置USART1_LIN引脚重映射，PC7-RX,PC8-TX
	GPIO_LIN();
	//全双工异步8bit 9600波特率
	USART_LIN_config(USART1_SFR);
	//TJA1021唤醒
	LIN_SLP_H();
	//串口接收中断使能
	USART_LIN_INT_config(USART1_SFR,INT_USART1);
	//初始化xxx，定时器中断循环计数
	xxx=0;
	//配置使能定时器T14
	BASIC_TIMER_Config(T14_SFR,INT_T14); //定时20ms进一次中断

	//定时器15定时间隔符 9600=104us 13个T=1.352ms  16M不分频
	TIM_Reset(T15_SFR);												//定时器外设复位，使能外设时钟
	BTIM_Work_Mode_Config(T15_SFR,BTIM_TIMER_MODE);					//定时模式选择
	BTIM_Set_Counter(T15_SFR,0);									//定时器计数值
	BTIM_Set_Period(T15_SFR,21632);							        //定时器周期值
	BTIM_Set_Prescaler(T15_SFR,0);								    //定时器预分频值0+1=1（不分频）
	BTIM_Clock_Config(T15_SFR,BTIM_HFCLK);            				//选用HFCLK时钟
	BTIM_Counter_Mode_Config(T15_SFR,BTIM_COUNT_UP_OF);				//向上计数,上溢产生中断标志

	while(1)
	{


	}
}

/**
  * 描述   USART1_LIN引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_LIN()
{
	/* 端口重映射AF5 */
	//USART1_RX_LIN		PC7
	//USART1_TX0_LIN	PC8
	GPIO_Write_Mode_Bits(GPIOC_SFR ,GPIO_PIN_MASK_7, GPIO_MODE_RMP);           //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOC_SFR ,GPIO_PIN_MASK_8, GPIO_MODE_RMP);           //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOC_SFR, GPIO_Pin_Num_7, GPIO_RMP_AF5_USART1);	   //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOC_SFR,GPIO_Pin_Num_8, GPIO_RMP_AF5_USART1);       //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOC_SFR ,GPIO_PIN_MASK_7, TRUE);                   //配置锁存
	GPIO_Pin_Lock_Config (GPIOC_SFR ,GPIO_PIN_MASK_8, TRUE);                   //配置锁存
}


/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}



