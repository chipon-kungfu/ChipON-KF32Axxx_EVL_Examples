/*
 * adc.h
 *
 *  Created on: 2019-11-6
 *      Author: charles_cai
 */

#ifndef ADC_H_
#define ADC_H_
typedef union Adc_value_name
{
	unsigned short buf_16;
	unsigned char buf_8[2];
}Adc_value;
void Init_adc0();
void Init_adc1();
void Init_adc0_it();
void Init_adc1_it();
#endif /* ADC_H_ */
