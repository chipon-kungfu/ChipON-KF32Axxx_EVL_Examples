/****************************************************************************************
 *
 * 文件名 kf_it.c
 * 作  者	ChipON_AE/FAE_Group
 * 版  本	V2.1
 * 日  期	2019-11-16
 * 描  述  该文件提供了例程中断入口地址，在串口中断中实现了LIN_SLAVE收发功能
 *
 ****************************************************************************************/
#include"system_init.h"
#include "lin_slave.h"
//*****************************************************************************************
//                                 USART1_LIN中断函数
//*****************************************************************************************	
void __attribute__((interrupt))_USART1_exception (void)
{
	GPIO_Toggle_Output_Data_Config(GPIOD_SFR,GPIO_PIN_MASK_6);
	USART_Clear_Receive_Overflow_INT_Flag(USART1_SFR);
	USART_Clear_Frame_ERROR_INT_Flag(USART1_SFR);
	//if(USART_Get_WeakUP_Flag(USART1_SFR))//自动唤醒中断标志
	if(USART_Get_Blank_Flag(USART1_SFR))//自动唤醒中断标志
	{
		if(l_sys_mode==0)//间隔场
		{
//			while(USART_Get_WUEN_Flag(USART1_SFR));  //唤醒使能，接收引脚检测到下降沿进入中断，检测到上升沿之后WUEN清零
			USART_Clear_WeakUP_INT_Flag (USART1_SFR);//清自动唤醒中断
			USART_Clear_Blank_INT_Flag (USART1_SFR);//清BRIF 发送间隔标志
			l_sys_mode=1;	 //间隔场完毕
		}
	}
	if(USART_Get_Receive_BUFR_Ready_Flag(USART1_SFR))
	{
		uint8_t i;
		if(!lin_re_state)
		{
		 	 if(l_sys_mode==1)
		 	 {
		 		Rev_Temp=USART_ReceiveData(USART1_SFR);//读寄存器值，清除接收中断标志
				if(Rev_Temp==0x55)  //同步场
		 		{
		 		 	l_sys_mode=2;//同步场接收完毕
		 		}
		 	 }
		 	 else if(l_sys_mode==2)
		 	 {
		 		 Rev_Temp=USART_ReceiveData(USART1_SFR); //接收受保护的标识符0x8F-->0xCF
		 		 if(Rev_Temp==0x8E)//0xCF  主机读
		 		 {
#if 1       //增强型校验    USART_Send函数长度应该从1开始
		 			 l_Tx_data[0]=0x8E;  //赋值，启动发送
		 			 l_Tx_data[1]=0x02;  //赋值，启动发送
		 			 l_Tx_data[2]=0x03;
		 			 l_Tx_data[3]=Check_Sum(l_Tx_data,0,3);//标准校验和
		 			 USART_Send(USART1_SFR,l_Tx_data,4); //发送数据
#endif
#if 0       //基础型校验   USART_Send函数长度应该从0开始
		 			 l_Tx_data[0]=0x02;  //赋值，启动发送
		 			 l_Tx_data[1]=0x03;
		 			 l_Tx_data[2]=0x04;
		 			 l_Tx_data[3]=0x05;
		 			 l_Tx_data[4]=Check_Sum(l_Tx_data,0,4);//标准校验和
		 			 USART_Send(USART1_SFR,l_Tx_data,5); //发送数据
#endif
		 			 Rev_Temp=USART_ReceiveData(USART1_SFR);//读寄存器值，清除接收中断标志

		 			 //重置等待唤醒
		 			 l_sys_mode=0;
//		 			 USART_WeakUP_Enable(USART1_SFR,TRUE);//唤醒使能

		 		 }
		 		 else if(Rev_Temp==0xCF)//主机写  0x0E-->0x8E ,接收8个数据
		 		 {
		 			 lin_re_state=1;
		 			 xxx=0;
		 		 }
				 else //未知的标识符，添加用户代码
				 {
					//重置等待唤醒
					l_sys_mode=0;
//					USART_WeakUP_Enable(USART1_SFR,TRUE);//唤醒使能
				 }
		 	 }
		}
		else //主机写，接收数据
		{
			l_rec_data[xxx]=USART_ReceiveData(USART1_SFR);//读寄存器值，清除接收中断标志
			xxx++;
			if(xxx==2)//接收8个数据
			{
				xxx=0;
				lin_re_state=0;
				//用户校验代码
				//
				//
				//重置等待唤醒
				l_sys_mode=0;
//				USART_WeakUP_Enable(USART1_SFR,TRUE);//唤醒使能
			}
		}

	}
	Rev_Temp_Clear=USART_ReceiveData(USART1_SFR);//读寄存器值，清除接收中断标志
//	GPIO_Toggle_Output_Data_Config(GPIOD_SFR,GPIO_PIN_MASK_6);
}


