/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该项目演示了CFGL模块的用法。CFGL1_IN0引脚悬空时，CFGL1_OUT输出HFCK和LFCK的逻辑或。
  * 	CFGL1_IN0接入低电平，CFGL1_OUT输出LFCK时钟。PA8反应CFGL中断响应的
  *
  *********************************************************************
  */
#include "system_init.h"
#include "CFGL_user.h"
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}


/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	/*上电默认时钟为16M的128分频，如初始化RAM时间过长，系统时钟初始化可放在startup.c
	 * 的startup()函数初始化RAM之前*/
	//系统时钟,外设高频时钟配置
	SystemInit();
	/*******LFCK配置******/
	OSC_LFCK_Division_Config(LFCK_DIVISION_1); //LFCK1分频
	OSC_LFCK_Source_Config(LFCK_INPUT_INTLF); //LFCK输入时钟选择内部低频
	OSC_LFCK_Enable(TRUE); //使能内部LFCK
	while(OSC_Get_INTLF_INT_Flag() != SET); //等待LFCK稳定

	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_MODE_OUT); //STATUS灯配置为输出模式
	GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_8,GPIO_MODE_OUT); //PA8配置为输出模式

	GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_3,GPIO_MODE_RMP); //PA3重映射为CFGL1_OUT
	GPIO_Pin_RMP_Config(GPIOA_SFR,GPIO_Pin_Num_3,GPIO_RMP_AF12_CFGL);

	GPIO_Write_Mode_Bits(GPIOH_SFR,GPIO_PIN_MASK_13,GPIO_MODE_RMP); //PH13重映射为CFGL1_IN0
	GPIO_Pin_RMP_Config(GPIOH_SFR,GPIO_Pin_Num_13,GPIO_RMP_AF12_CFGL);

	Init_Cfgl(); //配置CFGL模块
	CFGL1_OUT_SYNCHRO_Enable (FALSE); //不使能中断的时钟同步功能
	CFGL1_FALLINT_Enable(TRUE); //使能CFGL的上升沿中断功能，当输出信号变化时，产生中断
	CFGL1_RISEINT_Enable(TRUE); //使能CFGL的下降沿中断功能，当输出信号变化时，产生中断

	INT_Interrupt_Enable(INT_CFGL,TRUE);//外设中断使能
	INT_All_Enable (TRUE);//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断
	while(1)
	{
		Delay(0xFFFFF);
		GPIO_Toggle_Output_Data_Config (GPIOB_SFR, GPIO_PIN_MASK_13); //PB13翻转
	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
