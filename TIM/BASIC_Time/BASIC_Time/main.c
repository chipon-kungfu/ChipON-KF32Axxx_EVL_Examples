/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: BASIC_Time
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了基本定时器应用
 *
 ****************************************************************************************/
#include "system_init.h"

/*/*描述基本定时器初始化配置
* 输入  BTIMx:取值为T14_SFR/T15_SFR
*    Peripheral：取值为INT_T14/INT_T15
*/
void BASIC_TIMER_Config(BTIM_SFRmap* BTIMx, InterruptIndex Peripheral)
{
	//定时器时钟源选用SCLK  设周期为50000 设预分频23+1=24分频 120M主频 定时10ms进一次中断

	TIM_Reset(BTIMx);												//定时器外设复位，使能外设时钟
	BTIM_Updata_Immediately_Config(BTIMx,TRUE);						//立即更新控制
	BTIM_Updata_Enable(BTIMx,TRUE);									//配置更新使能
	BTIM_Work_Mode_Config(BTIMx,BTIM_TIMER_MODE);					//定时模式选择
	BTIM_Set_Counter(BTIMx,0);										//定时器计数值
	BTIM_Set_Period(BTIMx,50000);									//定时器周期值50000
	BTIM_Set_Prescaler(BTIMx,23);								    //定时器预分频值23+1=24
	BTIM_Counter_Mode_Config(BTIMx,BTIM_COUNT_UP_OF);				//向上计数,上溢产生中断标志
	BTIM_Clock_Config(BTIMx,BTIM_SCLK);								//选用SCLK时钟
	INT_Interrupt_Priority_Config(Peripheral,4,0);					//抢占优先级4,子优先级0
	BTIM_Overflow_INT_Enable(BTIMx,TRUE);							//计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);						    //外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);							//清中断标志
	BTIM_Cmd(BTIMx,TRUE);											//定时器启动控制使能
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);					//中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);											//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

}

/*描述GPIOx 输出初始化配置
/*输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
*       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
* 返回：无
*/
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
	GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_HIGH_SPEED;         //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}



//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现基本定时器14、 基本定时器15功能
	 * 例程中控制定时器的定时时间为10ms进一次中断，PB13在T14中断函数里翻转
	 * 对应PB13的D4灯100ms闪烁.
                    定时器时钟用SCLK 设周期为50000 设预分频值23+1=24分频 120M主频 定时10ms进一次中断
     */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//初始化PB13输出高
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);

	//配置使能定时器T14
	BASIC_TIMER_Config(T14_SFR,INT_T14); //定时10ms进一次中断
	//配置使能定时器T15
	BASIC_TIMER_Config(T15_SFR,INT_T15); //定时10ms进一次中断
	while(1)
	{
	}
}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

