/*
 * Adv_timer.h
 *
 *  Created on: 2019-12-5
 *      Author: charles_cai
 */

#ifndef ADV_TIMER_H_
#define ADV_TIMER_H_

void cfg_Tz(ATIM_SFRmap* ATIMx);
void cfg_Tx(ATIM_SFRmap* ATIMx);

#endif /* ADV_TIMER_H_ */
