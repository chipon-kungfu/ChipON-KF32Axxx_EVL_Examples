/****************************************************************************************
 * 文件名: main.c
 * 项目名: GPTimeBase_DMA
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了通用定时器与DMA应用例程参考
 ****************************************************************************************/
#include "system_init.h"
//软件使能触发DMA宏定义
#define   Software_TRIGGER     0       //1：软件使能触发 0：其他定时器触发


//外设和内存指针定义
uint32_t *u32_POINT_PAD;
uint32_t *u32_POINT_MAD;

//外设址定义
#define   Peripheraladdress  0x40000210    //指定一个外设地址
//内存地址
#define   Memoryaddress      0X10010000    //指定一个内存地址



//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=7000;
		while(j--);
	}

}


/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
		/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
			GPIO_Reset(GPIOx);

		/* 配置 Pxy作为输出模式参数 */
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_Struct_Init(&GPIO_InitStructure);
		GPIO_InitStructure.m_Pin = GpioPin;
		GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
		GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
		GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
		GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
		GPIO_Configuration(GPIOx,&GPIO_InitStructure);

		GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平
}


/**
  * 描述 DMA 参数初始化及通道选用。
  * 输入 : DMA_CHOOSE: 指向DMA内存结构的指针，取值为DMA0_SFR和DMA1_SFR。
  *          Channel: DMA通道选择，取值范围为:
  *                   DMA_CHANNEL_1: 通道1
  *                   DMA_CHANNEL_2: 通道2
  *                   DMA_CHANNEL_3: 通道3
  *                   DMA_CHANNEL_4: 通道4
  *                   DMA_CHANNEL_5: 通道5
  *                   DMA_CHANNEL_6: 通道6
  *                   DMA_CHANNEL_7: 通道7
  * 返回  无。
  */
void DMA_init(DMA_SFRmap * DMA_CHOOSE,uint8_t Channel)
{
	*(uint32_t *)Peripheraladdress =0x00005555;    //传输前外设初始化任一个数据

	/* DMA复位开启DMA时钟 */
	DMA_Reset (DMA_CHOOSE);///
	DMA_InitTypeDef dmaNewStruct;

	/* DMA功能参数配置 */
	{

		/* 配置 传输数据个数: 65535 */
		dmaNewStruct.m_Number = 65535;
		/* 配置 DMA传输方向：外设到内存 */
		dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
		/* 配置 DMA通道优先级：低优先级 */
		dmaNewStruct.m_Priority = DMA_CHANNEL_LOWER;
		/* 配置 外设数据位宽：32位宽 */
		dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
		/* 配置 存储器数据位宽:32位宽 */
		dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
		/* 配置 外设地址增量模式使能: 使能 */
		dmaNewStruct.m_PeripheralInc = FALSE;
		/* 配置 存储器地址增量模式使能: 使能 */
		dmaNewStruct.m_MemoryInc = FALSE;
		/* 配置 DMA通道选择:通道 */
		dmaNewStruct.m_Channel = Channel;
		/* 配置 数据块传输模式： */
		dmaNewStruct.m_BlockMode = DMA_TRANSFER_BLOCK;
		/* 配置 循环模式使能: 禁止 */
		dmaNewStruct.m_LoopMode = FALSE;//FALSE
		/* 配置 外设起始地址：等待发送的数据的起始地址 */
		dmaNewStruct.m_PeriphAddr = Peripheraladdress ;         //外设地址
		/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
		dmaNewStruct.m_MemoryAddr = Memoryaddress;
		/* 配置DMA功能函数 */
		DMA_Configuration (DMA_CHOOSE, &dmaNewStruct);
	}

	   /*清DMA中断标志位*/
		DMA_Clear_INT_Flag (DMA_CHOOSE, dmaNewStruct.m_Channel, DMA_INT_FINISH_TRANSFER);

		/* 使能DMA通道   */
		DMA_Channel_Enable(DMA_CHOOSE,dmaNewStruct.m_Channel,TRUE);
}


/**
  * 描述 定时器主动与从动模式 参数初始化配置。
  * 输入 : Timer_Trigger（主动）:  取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *               T19_SFR/T20_SFR/T21_SFR/T19_SFR/T22_SFR/T23_SFR。
  *      Timer_DMA（从动）: 取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *               T19_SFR/T20_SFR/T21_SFR/T19_SFR/T22_SFR/T23_SFR。
  * 返回  无。
  * 注意从动定时器所选的触发源
  */
void DMA_TRIGGER_Timer(GPTIM_SFRmap* Timer_Trigger,GPTIM_SFRmap* Timer_DMA)
{
	//////************Timer_Trigger触发Timer_DMA定时器后再由 Timer_DMA触发DMA CH1通道************************//////////////////
	//主频120M，经过定时器T2 24分频，0.2us执行计数一次，计数5000次1ms进一次中断
	//------------------------主动触发定时器 Timer_Trigger配置-----------------------------//
	        TIM_Reset(Timer_Trigger);											//定时器外设复位，使能外设时钟
			GPTIM_Updata_Immediately_Config(Timer_Trigger,TRUE);				//立即更新控制使能
			GPTIM_Updata_Enable(Timer_Trigger,TRUE);							//配置更新使能
			GPTIM_Master_Mode_Config(Timer_Trigger,GPTIM_MASTER_TXIF_SIGNAL);	//主模式配置
			GPTIM_Master_Slave_Snyc_Config(Timer_Trigger,TRUE);					//主从同步配置
			GPTIM_Work_Mode_Config(Timer_Trigger,GPTIM_TIMER_MODE);				//定时模式选择
			GPTIM_Set_Counter(Timer_Trigger,0);									//定时器计数值
			GPTIM_Set_Period(Timer_Trigger,5000);							    //定时器周期值
			GPTIM_Set_Prescaler(Timer_Trigger,23);							    //定时器预分频值
			GPTIM_Counter_Mode_Config(Timer_Trigger,GPTIM_COUNT_UP_DOWN_OUF);   //向上-向下计数,上溢和下溢产生中断标志
			GPTIM_Clock_Config(Timer_Trigger,GPTIM_SCLK);						//选用SCLK时钟
			GPTIM_Cmd(Timer_Trigger,TRUE);										//定时器启动控制使能


			//------------------------从动触发DMA定时器 Timer_DMA配置-----------------------------//
			TIM_Reset(Timer_DMA);												//定时器外设复位，使能外设时钟
			GPTIM_Updata_Immediately_Config(Timer_DMA,TRUE);					//立即更新控制
			GPTIM_Updata_Enable(Timer_DMA,TRUE);								//配置更新使能
			GPTIM_Work_Mode_Config(Timer_DMA,GPTIM_TIMER_MODE);					//定时模式选择

			GPTIM_Trigger_Select_Config(Timer_DMA,GPTIM_TRIGGER_T2);			//触发源选择T2
			GPTIM_Slave_Mode_Config(Timer_DMA,GPTIM_SLAVE_TRIGGER_MODE);		//选择触发模式
			GPTIM_Master_Slave_Snyc_Config(Timer_DMA,TRUE);						//主从同步配置

			GPTIM_Set_Counter(Timer_DMA,0);										//定时器计数值
			GPTIM_Set_Period(Timer_DMA,0X7FFF);									//定时器周期值
			GPTIM_Set_Prescaler(Timer_DMA,119);									//定时器预分频值
			GPTIM_Counter_Mode_Config(Timer_DMA,GPTIM_COUNT_UP_OF);			    //向上-向下计数,上溢和下溢产生中断标志
			GPTIM_Clock_Config(Timer_DMA,GPTIM_SCLK);							//选用SCLK时钟

			GPTIM_Cmd(Timer_DMA,TRUE);											//定时器启动控制使能
			GPTIM_Trigger_DMA_Enable(Timer_DMA,TRUE);                           //使能触发DMA请求

}


/**
  * 描述 软件直接触发定时器启动DMA功能。
  * 输入 : Timer_DMA:  取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *                       T19_SFR/T20_SFR/T21_SFR/T19_SFR/T22_SFR/T23_SFR。
  *
  * 返回  无。
  * 注意从动定时器所选模式
  */
void DMA_TRIGGER_Software(GPTIM_SFRmap* Timer_DMA)
{
	//////////////************************软件使能定时器触发DMA************************************8/////////////

	TIM_Reset(Timer_DMA);											//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(Timer_DMA,TRUE);				//立即更新控制
	GPTIM_Updata_Enable(Timer_DMA,TRUE);							//配置更新使能
	GPTIM_Work_Mode_Config(Timer_DMA,GPTIM_TIMER_MODE);				//定时模式选择
	GPTIM_Set_Counter(Timer_DMA,0);									//定时器计数值
	GPTIM_Set_Period(Timer_DMA,0xFFFF);							   //定时器周期值
	GPTIM_Set_Prescaler(Timer_DMA,0);				    		   //定时器预分频值
	GPTIM_Counter_Mode_Config(Timer_DMA,GPTIM_COUNT_UP_DOWN_OUF);  //向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(Timer_DMA,GPTIM_SCLK);					   //选用SCLK时钟

	GPTIM_Trigger_DMA_Enable(Timer_DMA,TRUE);                      //使能触发DMA请求
	GPTIM_Slave_Mode_Config(Timer_DMA,GPTIM_SLAVE_TRIGGER_MODE);   //选择触发模式//从模式：触发模式
	GPTIM_Master_Slave_Snyc_Config(Timer_DMA,TRUE);				   //主从同步配置
	GPTIM_Cmd(Timer_DMA,TRUE);                                     //使能定时器


	/* 软件触发，开始DMA传输 */
	GPTIM_Generate_Trigger_Config(Timer_DMA,TRUE);                 //TXTRG位置1 ，软件触发DMA
}

/**
  * 描述 测试DMA传输是否完成，并且传输后的数据是否匹配。
  * 输入 : 无
  *
  * 返回  无。
  * 注意从动定时器所选的触发源
  */
void DMA_Finsh_Test(void)
{
	uint32_t i, j;
	i = 0;
	j = 0;
	while (0 == DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR,DMA_CHANNEL_2))//等待DMA传输完数据
	{
		;
	}

	/*  传输完成后的检验  */
	u32_POINT_PAD=(uint32_t *)Peripheraladdress;   //外设地址
	u32_POINT_MAD=(uint32_t *)Memoryaddress;          //内存地址


	while(i<30)
	{
		if(*u32_POINT_PAD==*u32_POINT_MAD)  //外设与内存地址内容相等,PB13翻转
		{
			while(1)
			{
				GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//PB13翻转
				delay_ms(200);
				delay_ms(200);
			}

		}
		else   ///外设与内存地址内容不相等,PE0翻转
		{
			while(1)
			{
				delay_ms(200);
			}
		}
		i++;
		u32_POINT_PAD++;
		u32_POINT_MAD++;
		delay_ms(2);

	}
}



//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现通用定时器触发DMA功能配置。
	 * 例程中定时器T2作为主模式触发定时器T1后，再由T1触发DMA0 CH2通道传输数据，传输完数据后做数据对比测试,
	 * 数据匹配后PB13对应控制D4闪烁，否则D4熄灭
	 *
     *  宏定义 Software_TRIGGER为1时使能软件触发DMA，不需要启动定时
	 *
	 *注意：1、T22和T23共用一个使能位以及中断标志位，只能用其中一个定时器 中断标志
	 *
     */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//初始化PB13输出高电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);
	DMA_init(DMA0_SFR,DMA_CHANNEL_2);//初始化DMA0 通道2配置外设与内存地址等参数
#if Software_TRIGGER
	DMA_TRIGGER_Software(T1_SFR);   //配置T1初始化参数，并选择软件触发
#else
	DMA_TRIGGER_Timer(T2_SFR,T1_SFR); //配置T1初始化参数，并选T2触发
#endif

	DMA_Finsh_Test();                 //测试DMA传输是否完成，判断传输后的数据与传输前的是否匹配
    while(1)
    {
    }

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

