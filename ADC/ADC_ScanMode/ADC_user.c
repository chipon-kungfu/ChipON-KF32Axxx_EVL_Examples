/**
  ******************************************************************************
  * 文件名  .c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *ADC_user
  *********************************************************************
  */
#include "system_init.h"
#include "ADC_user.h"
#ifndef ADC_C_
#define ADC_C_
//#define ADC_VREFINT_DEF   //开启内部参考电压
void Init_adc()
{
	ADC_InitTypeDef adcStruct;

	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_2,GPIO_MODE_AN);//ADC_CHANNEL_2
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_1,GPIO_MODE_AN);//ADC_CHANNEL_1
	/*  使能内部参考电压  */
#ifdef ADC_VREFINT_DEF
	BKP_Write_And_Read_Enable(TRUE);
	PM_VREF_Software_Enable(TRUE);
	PM_VREF_SELECT(PM_VREF_VOLTAGE_2P5V);  //参考电压选择
	BKP_Write_And_Read_Enable(FALSE);
#endif
	ADC_Reset (ADC_UNIT); //复位ADC外设并打开ADC的时钟

	/* 初始化 ADC时钟源选择 */
	adcStruct.m_Clock = ADC_HFCLK;
	/* 初始化 ADC时钟分频 */
	adcStruct.m_ClockDiv = ADC_CLK_DIV_32; //选择32分频
	/* 初始化 ADC扫描模式使能 */
	adcStruct.m_ScanMode = TRUE;  //扫描模式/不使用扫描模式，一次触发只转换第一个通道
	/* 初始化 ADC连续转换模式 */
	adcStruct.m_ContinuousMode = ADC_SINGLE_MODE;//ADC_SINGLE_MODE or ADC_CONTINUOUS_MODE; //不使用连续转换功能，只对设置的通道采样一遍
	/* 初始化 ADC转换结果输出格式 */
	adcStruct.m_DataAlign = ADC_DATAALIGN_RIGHT; //转换的结果右对齐
	/* 初始化 ADC常规通道外部触发转换模式使能 */
	adcStruct.m_ExternalTrig_EN = FALSE; //常规通道失能外部条件触发ADC转换，
	/* 初始化 ADC常规通道外部触发事件 */
	adcStruct.m_ExternalTrig = ADC_EXTERNALTRIG_T1TRGO; //常规通道外部触发信号源配置为T1TRGO
	/* 初始化 ADC高优先级通道外部触发转换模式使能 */
	adcStruct.m_HPExternalTrig_EN = FALSE; //高优先级通道失能外部条件触发ADC转换，
	/* 初始化 高优先级通道外部触发事件 */
	adcStruct.m_HPExternalTrig = ADC_HPEXTERNALTRIG_CCP1_CH1; //高优先级通道触发信号源配置为CCP1的通道1
	/* 参考电压选择，取值为宏“ADC参考电压选择”中的一个。 */
#ifdef ADC_VREFINT_DEF
	adcStruct.m_VoltageRef=ADC_REF_INTERNAL;  //转换的参考电压源为内部参考电压
#else
	adcStruct.m_VoltageRef=ADC_REF_AVDD; //转换的参考电压源配置为VDDA
#endif
	/* 初始化 ADC常规通道扫描长度 */
	adcStruct.m_NumOfConv = 2; //扫描的长度配置为1个通道                      需要设置扫描通道的个数
	/* 初始化 ADC高优先级通道扫描长度 */
	adcStruct.m_NumOfHPConv = 0; //高优先级通道的扫描长度
	ADC_Configuration (ADC_UNIT, &adcStruct);

	ADC_Cmd (ADC_UNIT, TRUE); //ADC使能

	Delay(0xFF);  //ADC使能后需要一段短暂的时间，等待电路充电完毕后，才能去采样，否者可能存在较大的采样误差
}

void Init_adc_INT(void)
{
	ADC_Set_INT_Enable (ADC_UNIT,ADC_INT_EOC, TRUE);  //ADC_INT_EOC为单次中断，ADC_INT_END为常规通道转换完成中断使能
	INT_Interrupt_Priority_Config(ADC_INTx,1,0);    //优先级
	INT_Clear_Interrupt_Flag(ADC_INTx);
	INT_Interrupt_Enable(ADC_INTx,TRUE);            //使能ADC中断
}
#endif /* ADC_C_ */
