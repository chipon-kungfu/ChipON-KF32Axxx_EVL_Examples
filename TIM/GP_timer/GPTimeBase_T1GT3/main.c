/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: GPTimeBase_T1GT3
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了实现通用定时器使能另一个通用定时器的功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
		/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
			GPIO_Reset(GPIOx);

		/* 配置 Pxy作为输出模式参数 */
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_Struct_Init(&GPIO_InitStructure);
		GPIO_InitStructure.m_Pin = GpioPin;
		GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
		GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
		GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
		GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
		GPIO_Configuration(GPIOx,&GPIO_InitStructure);

		GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平
}



/**
  * 描述  主动触发定时器初始化配置功能。
  * 输入 : GPTIMx:  取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *                       T19_SFR/T20_SFR/T21_SFR/T19_SFR/T22_SFR/T23_SFR。
  *    Peripheral:  取值：枚举类型InterruptIndex中的外设中断向量编号。
  * 返回  无。
  * 注意主动定时器所选模式
  */
void GPTime_Master(GPTIM_SFRmap* GPTIMx,InterruptIndex Peripheral)
{
	//------------------------主动触发定时器 配置-----------------------------//

	TIM_Reset(GPTIMx);													//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);						//立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);									//配置更新使能

	GPTIM_Master_Mode_Config(GPTIMx,GPTIM_MASTER_TXEN_SIGNAL);			//主模式配置成TXEN作为触发

	GPTIM_Master_Slave_Snyc_Config(GPTIMx,TRUE);						//主从同步配置
	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);					//定时模式选择
	GPTIM_Set_Counter(GPTIMx,0);										//定时器计数值
	GPTIM_Set_Period(GPTIMx,65535);								    	//定时器周期值
	GPTIM_Set_Prescaler(GPTIMx,11);									    //定时器预分频值
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_DOWN_OUF);			//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_HFCLK);								//选用外设高频时钟
	GPTIM_Overflow_INT_Enable(GPTIMx,TRUE);                             //使能溢出中断
	GPTIM_Cmd(GPTIMx,TRUE);												//定时器启动控制使能

	INT_Interrupt_Priority_Config(Peripheral,4,0);					    //抢占优先级4,子优先级0
	INT_Interrupt_Enable(Peripheral,TRUE);								//外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);								//清中断标志


}

/**
  * 描述  从动触发定时器初始化配置功能。
  * 输入 :  GPTIMx:  取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *                       T19_SFR/T20_SFR/T21_SFR/T19_SFR/T22_SFR/T23_SFR。
  *    Peripheral: 取值：枚举类型InterruptIndex中的外设中断向量编号。
  * TriggerSelect：触发源选择
  * 返回  无。
  * 注意从动定时器所选门控模式
  */
void GPTime_Slave(GPTIM_SFRmap* GPTIMx,InterruptIndex Peripheral,FunctionalState TriggerSelect)
{
	//------------------------从动触发定时器 配置-----------------------------//
	TIM_Reset(GPTIMx);													//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);						//立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);									//配置更新使能

	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);					//定时模式选择
	GPTIM_Trigger_Select_Config(GPTIMx,TriggerSelect);			        //触发源选择
	GPTIM_Slave_Mode_Config(GPTIMx,GPTIM_SLAVE_GATED_MODE);				//选择触发模式：门控模式

	GPTIM_Master_Slave_Snyc_Config(GPTIMx,TRUE);						//主从同步配置
	GPTIM_Set_Counter(GPTIMx,0);										//定时器计数值
	GPTIM_Set_Period(GPTIMx,50000);									    //定时器周期值
	GPTIM_Set_Prescaler(GPTIMx,119);									//定时器预分频值120
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_OF);			    //向上计数,上溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_HFCLK);								//选用SCLK时钟

	GPTIM_Overflow_INT_Enable(GPTIMx,TRUE);								//计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);								//外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);								//清中断标志
	INT_Interrupt_Priority_Config(Peripheral,5,0);						//抢占优先级5,子优先级0
	GPTIM_Cmd(GPTIMx,TRUE);												//定时器启动控制使能

	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);						//中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);                                              //开总中断
}


//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现使用一个定时器使能另一个定时器。
	 * 例程中定时器T1作为主模式使能触发定时器T3，
	 * T1设置：/定时器T1使用主时钟120M为主频时钟，1：12分频，0.1us计数一次，计数65536次门控模式触发使能T3
	 * T3设置：/定时器T3使用主时钟120M为主频时钟，1：120分频，1us计数一次，计数50000次50ms进一次中断
	 * PB13翻转D4灯以400mS闪烁
	 *
	 *
     */
	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化
	/*PB13初始化设为输出高电平*/
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);
	GPTime_Master(T1_SFR,INT_T1);//配置T1为主模式触发
    GPTime_Slave(T3_SFR,INT_T3,GPTIM_TRIGGER_T1);//配置T3为从模式，并选T1为使能信号

    while(1)
    {
    }

}




void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

