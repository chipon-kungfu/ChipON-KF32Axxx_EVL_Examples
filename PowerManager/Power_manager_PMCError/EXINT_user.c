/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */
#include "system_init.h"


void PMC_Error_Init()
{
	//外部中断(EINT)配置信息结构体
	EINT_InitTypeDef EINT_19To17;

	INT_External_Struct_Init(&EINT_19To17);
	//********外部中断(EINT)配置信息结构体
	EINT_19To17.m_Mask = TRUE; 	//对应外部中断使能
	EINT_19To17.m_Rise = TRUE; 	//上升沿触发
	EINT_19To17.m_Line = INT_EXTERNAL_INTERRUPT_19; //外部中断线选择
	EINT_19To17.m_Fall = FALSE;    //下降沿触发
	INT_External_Configuration (&EINT_19To17);

	INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_19);

	BKP_Write_And_Read_Enable(TRUE);
	PM_CTL2  |= (1 << 19);  //PMC错误中断使能
	BKP_Write_And_Read_Enable(FALSE);
	INT_Interrupt_Enable(INT_EINT19TO17,TRUE);//使能外部中断
	INT_Clear_Interrupt_Flag(INT_EINT19TO17);//清中断标志
	INT_All_Enable (TRUE);
}

