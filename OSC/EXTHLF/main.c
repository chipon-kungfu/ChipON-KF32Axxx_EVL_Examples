/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: EXTHLF
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了外部晶振应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=5000;
		while(j--);
	}

}



typedef enum
{
  LED0 = 0,
  LED1 = 1,
  LED2 = 2,

} Led_TypeDef;


#define LEDn                             3
#define LED0_PIN                         GPIO_PIN_MASK_13
#define LED0_GPIO_PORT                   GPIOB_SFR


#define LED1_PIN                         GPIO_PIN_MASK_9
#define LED1_GPIO_PORT                   GPIOB_SFR


#define LED2_PIN                         GPIO_PIN_MASK_10
#define LED2_GPIO_PORT                   GPIOB_SFR



GPIO_SFRmap* GPIO_PORT[LEDn] = {LED0_GPIO_PORT, LED1_GPIO_PORT, LED2_GPIO_PORT};
const uint16_t GPIO_PIN[LEDn] = {LED0_PIN, LED1_PIN, LED2_PIN};



/**
  * 描述  打开LED灯功能函数。
  * 输入 : Led: LED0 LED1 LED2
  *
  * 返回  无。
  */
void Demo_LEDOn(Led_TypeDef Led)
{
  GPIO_Set_Output_Data_Bits(GPIO_PORT[Led],GPIO_PIN[Led],Bit_SET);					//设置为高电平
}

/**
  * 描述  熄灭LED灯功能函数。
  * 输入 : Led:  LED0 LED1 LED2  *
  * 返回  无。
  */
void Demo_LEDOff(Led_TypeDef Led)
{
	GPIO_Set_Output_Data_Bits(GPIO_PORT[Led],GPIO_PIN[Led],Bit_RESET);				//设置为低电平
}

/**
  * 描述  DEMO板上LED灯初始化配置。
  * 输入 : Led:  LED0 LED1 LED2
  *
  * 返回  无。
  */
void Demo_LEDInit(Led_TypeDef Led)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_Struct_Init(&GPIO_InitStructure);
  /*配置 GPIO_LED 引脚 */
    GPIO_InitStructure.m_Pin = GPIO_PIN[Led];

	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
    GPIO_Configuration(GPIO_PORT[Led], &GPIO_InitStructure);
}


/**
  * 描述 低功耗定时器T0定时时间配置。
  * 输入  Period：新的周期值，取值16位数据
  *     Prescaler：对INTLF的预分频值
  * 返回  无。
  */
void LPTIMER_T0_Config(uint32_t Period,uint32_t Prescaler)
{

	OSC_SCK_Source_Config(SCLK_SOURCE_INTHF);                 //选择INTHF作为系统时钟

	OSC_Backup_Write_Read_Enable(TRUE);                       //使能备份域读写功能

	PM_CCP0CLKLPEN_Enable(TRUE);                              //CCP0低功耗时钟使能

	TIM_Reset(T0_SFR);										  //定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(T0_SFR,TRUE);			  //立即更新控制
	GPTIM_Updata_Enable(T0_SFR,TRUE);						  //配置更新使能
	GPTIM_Work_Mode_Config(T0_SFR,GPTIM_TIMER_MODE);		  //定时模式选择
	GPTIM_Set_Counter(T0_SFR,0);							  //定时器计数值
	GPTIM_Set_Period(T0_SFR,Period);						  //定时器周期值
	GPTIM_Set_Prescaler(T0_SFR,Prescaler);				      //定时器预分频值
	GPTIM_Counter_Mode_Config(T0_SFR,GPTIM_COUNT_UP_DOWN_OUF);//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(T0_SFR,GPTIM_LFCLK);					  //选用LFCLK时钟

	INT_Interrupt_Priority_Config(INT_T0,4,0);				  //抢占优先级4,子优先级0
	GPTIM_Overflow_INT_Enable(T0_SFR,TRUE);					  //计数溢出中断使能
	INT_Interrupt_Enable(INT_T0,TRUE);						  //外设中断使能
	INT_Clear_Interrupt_Flag(INT_T0);						  //清中断标志
	GPTIM_Cmd(T0_SFR,TRUE);									  //定时器启动控制使能
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);			  //中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);									  //全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

	OSC_Backup_Write_Read_Enable(FALSE);                      //禁止备份域读写功能

	OSC_SCK_Source_Config(SCLK_SOURCE_PLL);                   //选择PLL作为系统时钟


}

/**
  * description:EXTHF&EXTLF init config
  * in: none
  *
  * return: none
  */
void OSC_EXTHLF_Config(void)
{
	volatile unsigned int delay_C;
	/* enable INTLF  */
	OSC_INTLF_Software_Enable(TRUE);
	/* enable INTHF  */
	OSC_INTHF_Software_Enable(TRUE);  		  //INTHF need 200us starting time 
	FLASH_CFG = 0xC7;                         //Flash confige default setting 
    delay_C = 0xc8;
	while (delay_C--); //delay 200us
	OSC_SCK_Division_Config(SCLK_DIVISION_1); // clock division 1:1
	OSC_SCK_Source_Config(SCLK_SOURCE_INTHF); //slect INTHF to be system clock;
	while (OSC_Get_INTHF_INT_Flag() != SET);

	/* Config the BKP area is enable, BKP register and data can be write and read */
	SFR_SET_BIT_ASM(OSC_CTL0, OSC_CTL0_PMWREN_POS);  
	SFR_SET_BIT_ASM(PM_CTL0, PM_CTL0_BKPREGCLR_POS);
	SFR_SET_BIT_ASM(PM_CTL0, PM_CTL0_BKPWR_POS);

	/************set OSCIN io to be low level signal ,improve EXTHF operation time***********/
    GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_9,GPIO_MODE_OUT);  //Set PD9 OUT 
	GPIO_Set_Output_Data_Bits(GPIOD_SFR,GPIO_PIN_MASK_9,Bit_RESET);	 //Set PD9 OUT LOW
	delay_C = 0xc8;
	while (delay_C--); //delay 200us
	/************over***********/

	GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_9,GPIO_MODE_AN);  //Set PD9 anloge mode in
	OSC_High_Speed_Enable (1);  //osc<20Mhz;OSC_HFOSCCAL1_HSFSEN=1;
	PM_EXTHF_PIN_Selection_Config(PM_EXTHF_PIN2_IO_PORT); //slect PD9/PD10 OSC IO;
	OSC_EXTHF_Software_Enable(TRUE); //enable EXTHF 
	OSC_EXTHF_Start_Delay_Config(EXT_START_DELAY_256);   //EXTHF start delay cycle;at lest 256
	
	/************add soft delay 4ms***********/
	delay_C = 0xA39;
	while (delay_C--); 
	while(OSC_Get_EXTHF_INT_Flag() != SET);    //wait EXTHF Flag
	/************over***********/
	
	/************system clock must be PLL clock source!!!!***********/
	OSC_SCK_Source_Config(SCLK_SOURCE_EXTHF);  //slect EXTHF to be system clock;
    OSC_PLL_Input_Source_Config(PLL_INPUT_EXTHF);        //slect EXTHF(16M) to be PLL clock;
    /************system clock must be PLL clock source!!!!***********/

	/* PLL frequency multiplier clock */
    OSC_PLL_Multiple_Value_Select(30,2,2);               //120MHZ

	OSC_PLL_Start_Delay_Config(PLL_START_DELAY_8192);
	OSC_PLL_Software_Enable(TRUE);
	OSC_PLL_RST();
	delay_C = 0x27f;
	while (delay_C--) ; //wait 1ms   
	while (OSC_Get_PLL_INT_Flag() != SET);
	/* system clock */
	OSC_SCK_Division_Config(SCLK_DIVISION_1);   //DIVISION 1:1
	OSC_SCK_Source_Config(SCLK_SOURCE_PLL);     //slect PLL to be system clock

	OSC_Clock_Failure_Check_Enable(TRUE);       //enable osc clock failure check;

	/* peripheral clock ;HFCLK*/
	OSC_HFCK_Division_Config(HFCK_DIVISION_1);
	OSC_HFCK_Source_Config(HFCK_SOURCE_EXTHF);    //slect EXTHF to be HFCLK
	OSC_HFCK_Enable(TRUE);                        //enable HFCLK

	PM_EXTLF_PIN_Selection_Config(PM_EXTLF_PIN2_IO_PORT);
	OSC_EXTLF_Software_Enable(TRUE);
	OSC_EXTLF_Start_Delay_Config(EXT_START_DELAY_256);

	OSC_LFCK_Division_Config(LFCK_DIVISION_1);	// Low  frequency LFCK DIVISIN 1:1 
	OSC_LFCK_Source_Config (LFCK_INPUT_EXTLF);  //slect EXTLF to be LFCLK
	OSC_LFCK_Enable (TRUE);					    //enable LFCLK


	OSC_SCLK_Output_Division_Config(SCLK_DIVISION_128);					//DIVISION 1/128
	OSC_SCLK_Output_Enable(TRUE);										//system clock out 
	GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_0,GPIO_MODE_RMP);      //PA0 mask map 
	GPIO_Pin_RMP_Config(GPIOA_SFR,GPIO_Pin_Num_0,GPIO_RMP_AF0_SYSTEM);	//output system clock from PA0 

}

//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现外部高频低频晶振的配置功能。
	 * 例程中使用外部的16M晶振作为系统时钟源，外部的32.768KHZ晶振作为通用定时器T0时钟源
	 * 定时器T0计数到1秒进一次中断
	 * PB13对应控制D4在主函数时里循环开关，D4闪烁
	 * 同时启动外部时钟故障检测一旦有故障系统自动切换到内部时钟，系统正常工作。
	 *
	 * 注意:需要使能备份域，并让备份域退了复位状态，才能使能外部晶振应用。
	 */
	/*外部高频与低频晶振配置*/
	OSC_EXTHLF_Config();  //外部 高频与低频晶振配置，主时钟配置成120MHZ

	GPIO_Reset(GPIOB_SFR);//复位GPIOB，使能GPIOB时钟
	Demo_LEDInit(LED0);
	//低功耗定时器设置周期值
	LPTIMER_T0_Config(512,63); //时钟源32.768KHZ, 预分频值1:64   512HZ计数512次，1秒进一次中断
	while(1)
	{
		Demo_LEDOn(LED0);
		delay_ms(1000);
		Demo_LEDOff(LED0);
		delay_ms(1000);
	}

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

