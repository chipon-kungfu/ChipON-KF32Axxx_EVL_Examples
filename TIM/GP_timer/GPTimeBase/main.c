/****************************************************************************************
 * 文件名: main.c
 * 项目名: GPTimeBase
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了通用定时器应用例程参考
 ****************************************************************************************/
#include "system_init.h"



/**
  * 描述  通用定时器GPTIMx 初始化配置。
  * 输入  GPTIMx:取值为T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/ T18_SFR/T19_SFR/T20_SFR/T21_SFR/T22_SFR/T23_SFR。
  *  Peripheral：取值为INT_T0/INT_T1/INT_T2/INT_T3/INT_T4/INT_T18/INT_T19/INT_T20/INT_T21/INT_T22/INT_T23
  *
  * 返回  无。
  */
void GENERAL_TIMER_Tx_Config(GPTIM_SFRmap* GPTIMx,InterruptIndex Peripheral)
{
	//定时器的时钟源选主时钟120M，经过定时器24分频，0.2us执行计数一次，计数5000次1ms进一次中断
	TIM_Reset(GPTIMx);																//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);									//立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);												//配置更新使能
	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);								//定时模式选择
	GPTIM_Set_Counter(GPTIMx,0);													//定时器计数值
	GPTIM_Set_Period(GPTIMx,5000);												    //定时器周期值
	GPTIM_Set_Prescaler(GPTIMx,23);				    								//定时器预分频值
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_DOWN_OUF);						//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_SCLK);											//选用SCLK时钟
	INT_Interrupt_Priority_Config(Peripheral,4,0);									//抢占优先级4,子优先级0
	GPTIM_Overflow_INT_Enable(GPTIMx,TRUE);											//计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);											//外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);											//清中断标志
	GPTIM_Cmd(GPTIMx,TRUE);															//定时器启动控制使能
													//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

}


/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
		/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
			GPIO_Reset(GPIOx);

		/* 配置 Pxy作为输出模式参数 */
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_Struct_Init(&GPIO_InitStructure);
		GPIO_InitStructure.m_Pin = GpioPin;
		GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
		GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
		GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
		GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
		GPIO_Configuration(GPIOx,&GPIO_InitStructure);

		GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);					//先设置为高电平
}



//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现通定时器功能配置
	 *例程中通用定时器T0 T1 T20 T22，定时器时钟用SCLK 设周期为50000 设预分频值23+1=24分频
	 *          定时器T0 T1 T20 T22，时钟源为120M主频 定时10ms进一次中断
	 *
	 *注意： 1、T20/21与 通用定时器 T0/1/2/3/4/18/19/22/23 除 Tx_CNT、Tx_PRSC、Tx_PPX 寄存器位数不一样外，其他都一致。
	 *      2、T20/21是32位定时器， T0/1/2/3/4/18/19/22/23是16位定时器。
	 *      3、T22和T23共用一个使能位以及中断标志位
	 *
     */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//初始化PB13输出高电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);

	//定时器T0配置
	GENERAL_TIMER_Tx_Config(T0_SFR,INT_T0);
	//定时器T1配置
	GENERAL_TIMER_Tx_Config(T1_SFR,INT_T1);
	//定时器T20配置
	GENERAL_TIMER_Tx_Config(T20_SFR,INT_T20);
	//定时器T22配置
	GENERAL_TIMER_Tx_Config(T22_SFR,INT_T22);

	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);				//中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);		                               //使能总中断
    while(1)
    {
    }

}




void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

