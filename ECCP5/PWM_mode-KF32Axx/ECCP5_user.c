/**
  ******************************************************************************
  * 文件名  ECCP5_user.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了关于ECCP的配置方法
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ECCP5_user.h"
/**
  * 描述   ECCP5的输出管脚配置
  * 输入   无
  * 返回   无
  *
  * 管脚分配
  * 	ECCP5CH1L —— PB14
  * 	ECCP5CH1H —— PB15
  * 	ECCP5CH2L —— PB2
  * 	ECCP5CH2H —— PF1
  * 	ECCP5CH3L —— PF2
  * 	ECCP5CH3H —— PF3
  * 	ECCP5CH4L —— PF4
  * 	ECCP5CH4H —— PA8
  * 	ECCP5BKIN —— PB0
  */
void cfg_gpio_for_ECCP5()
{
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_11,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOH_SFR,GPIO_PIN_MASK_15,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_2,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOF_SFR,GPIO_PIN_MASK_1,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOF_SFR,GPIO_PIN_MASK_2,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOF_SFR,GPIO_PIN_MASK_3,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOF_SFR,GPIO_PIN_MASK_4,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_8,GPIO_MODE_RMP);
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_0,GPIO_MODE_RMP);

	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_11,GPIO_RMP_AF2_T5);//CCP5CH1L
	GPIO_Pin_RMP_Config(GPIOH_SFR,GPIO_Pin_Num_15,GPIO_RMP_AF2_T5);//CCP5CH1H
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_2,GPIO_RMP_AF2_T5);//CCP5CH2L
	GPIO_Pin_RMP_Config(GPIOF_SFR,GPIO_Pin_Num_1,GPIO_RMP_AF2_T5);//CCP5CH2H
	GPIO_Pin_RMP_Config(GPIOF_SFR,GPIO_Pin_Num_2,GPIO_RMP_AF2_T5);//CCP5CH3L
	GPIO_Pin_RMP_Config(GPIOF_SFR,GPIO_Pin_Num_3,GPIO_RMP_AF2_T5);//CCP5CH3H
	GPIO_Pin_RMP_Config(GPIOF_SFR,GPIO_Pin_Num_4,GPIO_RMP_AF2_T5);//CCP5CH4L
	GPIO_Pin_RMP_Config(GPIOA_SFR,GPIO_Pin_Num_8,GPIO_RMP_AF2_T5);//CCP5CH4H
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_0,GPIO_RMP_AF2_T5);//CCP5BKIN

	GPIO_Pull_Up_Enable(GPIOB_SFR,GPIO_PIN_MASK_0,TRUE);  //将CCP5BKIN引脚使用内部上拉，默认是不关断PWM输出的
}

/**
  * 描述   配置ECCP5
  * 输入   无
  * 返回   无
  */
void cfg_ECCP5()
{
	ECCP_PWM_Mode_Config(ECCP5_SFR,ECCP_PWM_FREE);									//PWM自由模式，
	ECCP_Channel_Output_Mode(ECCP5_SFR,ECCP_CHANNEL_1,ECCP_OUTPUT_INDEPENDENT);		//独立输出模式
	ECCP_Channel_Output_Mode(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_OUTPUT_COMPLEMENTARY);	//互补输出模式
	ECCP_Channel_Output_Mode(ECCP5_SFR,ECCP_CHANNEL_3,ECCP_OUTPUT_COMPLEMENTARY);	//互补输出模式
	ECCP_Channel_Output_Mode(ECCP5_SFR,ECCP_CHANNEL_4,ECCP_OUTPUT_INDEPENDENT);		//独立输出模式

	ECCP_Channel_Shutdown_Signal(ECCP5_SFR,ECCP_CHANNEL_1,ECCP_CHANNEL_SHUTDOWN_FORBID);	//禁止自动关断
	ECCP_Channel_Shutdown_Signal(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_CHANNEL_BKIN_INACTIVE);		//使能自动关断，ECCPx_BKIN引脚上的低电平
	ECCP_Channel_Shutdown_Signal(ECCP5_SFR,ECCP_CHANNEL_3,ECCP_CHANNEL_SHUTDOWN_FORBID);	//禁止自动关断
	ECCP_Channel_Shutdown_Signal(ECCP5_SFR,ECCP_CHANNEL_4,ECCP_CHANNEL_BKIN_INACTIVE);		//使能自动关断，ECCPx_BKIN引脚上的低电平

	ECCP_PWM_Restart_Enable(ECCP5_SFR,TRUE);	//使能自动重启

	ECCP_PWM_Move_Phase_Enable(ECCP5_SFR,TRUE);	//使能移相

	ECCP_Channel_Pin_Ctl(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_PORT_LOW,PIN_INACTIVE);				//关闭状态为低电平
	ECCP_Channel_Pin_Ctl(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_PORT_HIGH,PIN_ACTIVE);				//关闭状态为高电平
	ECCP_Channel_Pin_Ctl(ECCP5_SFR,ECCP_CHANNEL_4,ECCP_PORT_LOW,PIN_INACTIVE);				//关闭状态为低电平
	ECCP_Channel_Pin_Ctl(ECCP5_SFR,ECCP_CHANNEL_4,ECCP_PORT_HIGH,PIN_TRISTATE);				//关闭状态为三态

	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_1,ECCP_PORT_LOW,ECCP_CHANNEL_OUTPUT_ACTIVE);				//强制输出高电平
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_1,ECCP_PORT_HIGH,ECCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//PWM输出，高有效
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_PORT_LOW,ECCP_CHANNEL_OUTPUT_PWM_ACTIVE);			//PWM输出，高有效
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_2,ECCP_PORT_HIGH,ECCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//PWM输出，高有效
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_3,ECCP_PORT_LOW,ECCP_CHANNEL_OUTPUT_PWM_ACTIVE);			//PWM输出，高有效
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_3,ECCP_PORT_HIGH,ECCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//PWM输出，高有效
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_4,ECCP_PORT_LOW,ECCP_CHANNEL_OUTPUT_PWM_INACTIVE);		//PWM输出，低有效
	ECCP_Channel_Output_Control(ECCP5_SFR,ECCP_CHANNEL_4,ECCP_PORT_HIGH,ECCP_CHANNEL_OUTPUT_PWM_ACTIVE);		//PWM输出，高有效

	ECCP_Dead_Time_Config(ECCP5_SFR,ECCP_CHANNEL_2,0x10);			//通道死区延时时间

	ECCP_Set_Compare_Result(ECCP5_SFR,ECCP_CHANNEL_1,0x2000);		//PWM占空比
	ECCP_Set_Compare_Result(ECCP5_SFR,ECCP_CHANNEL_2,0x4000);		//PWM占空比
	ECCP_Set_Compare_Result(ECCP5_SFR,ECCP_CHANNEL_3,0x6000);		//PWM占空比
	ECCP_Set_Compare_Result(ECCP5_SFR,ECCP_CHANNEL_4,0x800);		//PWM占空比
}

