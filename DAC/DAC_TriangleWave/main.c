/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: DAC_TriangleWave
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了DAC三角波输出应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  通用定时器初始化参数。
  * 输入 : GPTIMx: 指向定时器内存结构的指针，
  *               取值T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/T18_SFR/
  *               T19_SFR/T20_SFR/T21_SFR。
  *       Period：新的周期值，T20 T21取值32位数据。其他定时器取值 为16位数据
  * 返回  无。
  */
void GENERAL_TIMER_Tx_Config(GPTIM_SFRmap* GPTIMx,uint32_t Period)
{
//配置通用定时器 为主模式 TXIF触发，选用SCLK时钟源，预分频设为3
	TIM_Reset(GPTIMx);											  //定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);				  //立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);							  //配置更新使能
	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);			  //定时模式选择
	GPTIM_Master_Mode_Config(GPTIMx,GPTIM_MASTER_TXIF_SIGNAL);    //配置主模式选择TXIF作为触发

	GPTIM_Set_Counter(GPTIMx,0);								  //定时器计数值
	GPTIM_Set_Period(GPTIMx,Period);							  //定时器周期值
	GPTIM_Set_Prescaler(GPTIMx,2);				    			  //定时器预分频值
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_DOWN_OUF);	  //向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_SCLK);						  //选用SCLK时钟
	GPTIM_Cmd(GPTIMx,TRUE);										  //定时器启动控制使能

}

/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}


/**
  * 描述  DACx 初始化配置并选择触发源。
  * 输入 :  DACx: 指向DAC内存结构的指针，取值为DAC0_SFR、DAC1_SFR。
   TriggerEvent: 触发事件选择：     DAC_TRIGGER_T1_TRGO
	*					        DAC_TRIGGER_T3_TRGO
	*							DAC_TRIGGER_T5_TRGO
	*							DAC_TRIGGER_T9_TRGO
	*							DAC_TRIGGER_T14_TRGO
	*							DAC_TRIGGER_T15_TRGO
	*							DAC_TRIGGER_SOFTWARE_TRGO
	*							DAC_TRIGGER_T0_TRGO
	*							DAC_TRIGGER_T2_TRGO
	*							DAC_TRIGGER_T4_TRGO
	*							DAC_TRIGGER_T18_TRGO
	*							DAC_TRIGGER_T19_TRGO
	*							DAC_TRIGGER_T20_TRGO
	*							DAC_TRIGGER_T21_TRGO
  * 返回  无。
  */
void DAC_Init_Config(DAC_SFRmap* DACx, uint32_t m_TriggerEvent)
{
//DAC使能三角波发生器，选择SCLK为时钟源，用内部参考电压模块作为参考电压
	static uint32_t ReferenceVoltage=0;
	DAC_Reset(DACx);                                //复位DAC，并使能DAC时钟

	DAC_InitTypeDef DAC_InitStructure;
	DAC_Struct_Init(&DAC_InitStructure);
	DAC_InitStructure.m_TriggerEnable=TRUE;                 //DAC通道触发使能
	DAC_InitStructure.m_TriggerEvent = m_TriggerEvent;      //DAC通道触发事件
	DAC_InitStructure.m_TriggerDMAEnable = FALSE;           //DAC触发DMA使能
	DAC_InitStructure.m_Wave = DAC_WAVE_TRIANGLE;           //使能三角波发生器
	DAC_InitStructure.m_Mas = DAC_TRIANGLE_AMPLITUDE_4095;  //幅值选择
	DAC_InitStructure.m_Clock = DAC_CLK_SCLK;               //DAC工作时钟源
	DAC_InitStructure.m_ReferenceVoltage = DAC_RFS_1P2V;    //DAC参考电压
	DAC_InitStructure.m_OutputBuffer = TRUE;                //输出缓冲
	if(DACx==DAC0_SFR)
	{
		DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_1;     //DAC0 引脚为 DAC_OUTPUT_PIN_1 PA4
	}else
	{
		DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_0;     //DAC1 引脚为 DAC_OUTPUT_PIN_0 PA0
	}
	DAC_InitStructure.m_Output = 4095;
    DAC_Configuration(DACx,&DAC_InitStructure);

    ReferenceVoltage =DAC_InitStructure.m_ReferenceVoltage;

	if((ReferenceVoltage = DAC_RFS_2V)||(ReferenceVoltage = DAC_RFS_1P2V))
	{
		OSC_Backup_Write_Read_Enable (TRUE);    //允许配置备份区寄存器读写
		PM_Reference_Voltage_Enable (TRUE);     //使能参考电压模块
	}
	DAC_Cmd(DACx,TRUE);//DAC使能
}


//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现DAC 输出三角波发生器配置功能。
	    例程中  1、DAC0 三角波输出，触发源选择定时器T0
            2、DAC1三角波输出，触发源选择定时器T20
                  定时器的触发周期决定三角波生成的时间
     * 挂示波器可以测量到PA0、PA4输出三角波
     *注意选用PA0 PA4作为DAC不能再重映射其他功能，用内部参考电压时要打开备份域读写功能
	 */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//PB8初始化输出高电平

	GENERAL_TIMER_Tx_Config(T0_SFR,4000);  //使能配置定时器T0 周期4000
	DAC_Init_Config(DAC0_SFR,DAC_TRIGGER_T0_TRGO);//配置DAC0 三角波初始化参数，并选择T0触发源

	GENERAL_TIMER_Tx_Config(T20_SFR,2000); //使能配置定时器T20  周期2000
	DAC_Init_Config(DAC1_SFR,DAC_TRIGGER_T20_TRGO);//配置DAC1 三角波初始化参数，并选择T20触发源
    while(1)
    {
    }

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

