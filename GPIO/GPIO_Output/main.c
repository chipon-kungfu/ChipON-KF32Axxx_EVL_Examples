/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: GPIO_Output
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了GPIO输出应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"


#define Demo_LEDON()         GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_SET)  //设置为高电平
#define Demo_LEDOff()        GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_RESET)//设置为低电平
#define Demo_LED_ON_OFF()    GPIO_Toggle_Output_Data_Config (GPIOB_SFR, GPIO_PIN_MASK_0)   //LED电平取反


void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=10000;
		while(j--);
	}

}



/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	 //先设置为高电平

}




//主函数
void main()
{
	/*用户可参考该例程在KF32A151demo板上实现GPIO输出配置功能。
	 * 例程中使用GPIO  PB13作为控制D4灯,PB13作为输出
	 * D4灯间隔500ms 闪烁
	 */

    	//系统时钟120M,外设高频时钟16M
		SystemInit();//系统时钟初始化
		GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);  //PB13初始化输出高电平

		while(1)
		{
			Demo_LEDON(); //D4灯点亮
			delay_ms(500);
			Demo_LEDOff();//D4灯熄灭
			delay_ms(500);
		}
}






void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

