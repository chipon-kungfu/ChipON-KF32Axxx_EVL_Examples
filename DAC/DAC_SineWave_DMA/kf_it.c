/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception (void)
{

}

//*****************************************************************************************
//                               硬件错误中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                                堆栈错误中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception (void)
{

}


//*****************************************************************************************
//                             EINT5~EINT9中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _EINT9TO5_exception (void)
{
	GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);  // PB8翻转一次
	INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_9);

}
