/**
  ********************************************************************
  * 文件名  I2C_DMA.h
  * 作  者   ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了I2C DMA 头文件与DMA GPIO初始化等相关函数宏定义。
  *
  *********************************************************************
*/
#ifndef I2C_DMA_H_
#define I2C_DMA_H_

#include <string.h>
#include <stdint.h>
#include "system_init.h"



#define	IIC_10BIT_Address		0	//0——7位地址模式，1——10位地址模式

#define I2C_OK                 1
#define I2C_FAIL               0

//#define IIC_100K	100000
#define IIC_400K	400000
//#define IIC_1M	1000000

void DMA_I2C_Enable(FunctionalState NewState);
void I2C_GPIO_init(void);
void I2C_init_Slave(I2C_SFRmap* I2Cx,uint16_t I2C_ADDRESS);
void I2C_init_Master(I2C_SFRmap* I2Cx);
void I2C_Byte_Write(uint16_t Write_i2c_Addr,uint32_t I2C_data);
void I2C_Buffer_write(uint16_t Write_i2c_Addr,uint8_t *p_buffer,uint16_t number_of_byte);
void I2C_byte_read(uint16_t Read_I2C_Addr,uint32_t I2C_data,uint16_t number_of_byte);
void I2C_Buffer_read(uint16_t Read_I2C_Addr,uint8_t *p_buffer,uint16_t number_of_byte);


#endif /* I2C_DMA_H_ */
