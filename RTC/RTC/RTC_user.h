/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  RTC配置头文件
  *
  *********************************************************************
  */

#ifndef RTC_H_
#define RTC_H_

void Set_rtc_time();
void Print_time_to_uart();
void Set_rtc_Alarm();
void Set_rtc_int();

#endif /* RTC_H_ */
