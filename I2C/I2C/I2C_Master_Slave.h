/*
 * I2C_Master_Slave.h
 *
 */

#ifndef I2C_Master_Slave_H_
#define I2C_Master_Slave_H_


#include "system_init.h"

#define	MODE_Master	1					 //主机模式
#define	MODE_Slave	0                    //从机模式
#define	IIC_MODE	MODE_Slave			//0——从机模式，1——主机模式

#define	IIC_10BIT_Address		0	    //0——7位地址模式，1——10位地址模式


//主机模式下波特率的选择
//#define IIC_100K	100000
#define IIC_400K	400000
//#define IIC_1M	1000000

#define I2C_OK                 1
#define I2C_FAIL               0



void I2C_GPIO_init(void);
void I2C_init_Slave(I2C_SFRmap* I2Cx,uint16_t I2C_ADDRESS);
void I2C_init_Master(I2C_SFRmap* I2Cx);
void I2C_Byte_Write(uint16_t Write_i2c_Addr,uint32_t I2C_data);
void I2C_Buffer_write(uint16_t Write_i2c_Addr,uint8_t *p_buffer,uint16_t number_of_byte);
void I2C_byte_read(uint16_t Read_I2C_Addr,uint32_t I2C_data,uint16_t number_of_byte);
void I2C_Buffer_read(uint16_t Read_I2C_Addr,uint8_t *p_buffer,uint16_t number_of_byte);

#endif /* I2C_Master_Slave.h */
