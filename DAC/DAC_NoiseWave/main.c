/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: DAC_NoiseWave
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了DAC噪声发生器输出应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  DACx 输出功能初始化参数。
  * 输入 : DACx: 指向DAC内存结构的指针取值为DAC0_SFR、DAC1_SFR。
  *  m_TriggerEvent:  DAC通道触发事件
  *
  * 返回  无。
  */
void DAC_Init_Config(DAC_SFRmap* DACx, uint32_t m_TriggerEvent)
{
	static uint32_t ReferenceVoltage=0;

	DAC_Reset(DACx);                                     //复位并打开DAC时钟

	DAC_InitTypeDef DAC_InitStructure;
	DAC_Struct_Init(&DAC_InitStructure);
	DAC_InitStructure.m_TriggerEnable=TRUE;               //DAC通道触发使能
	DAC_InitStructure.m_TriggerEvent = m_TriggerEvent;    //选择DAC通道触发事件
	DAC_InitStructure.m_TriggerDMAEnable = FALSE;         //DAC触发DMA使能
	DAC_InitStructure.m_Wave = DAC_WAVE_NOISE;            //使能噪声波发生器
	DAC_InitStructure.m_Mas = DAC_LFSR_UNMASK_BITS11_0;   //不屏蔽LFSR位选择
	DAC_InitStructure.m_Clock = DAC_CLK_SCLK;             //DAC工作时钟源
	DAC_InitStructure.m_ReferenceVoltage = DAC_RFS_2V;    //DAC参考电压
	DAC_InitStructure.m_OutputBuffer = TRUE;              //使能输出缓冲
	if(DACx==DAC0_SFR)
	{
		DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_1;     //DAC0 引脚为 DAC_OUTPUT_PIN_1 PA4
	}else
	{
		DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_0;     //DAC1 引脚为 DAC_OUTPUT_PIN_0 PA0
	}


	DAC_InitStructure.m_Output = 4095;                    //输出数据
    DAC_Configuration(DACx,&DAC_InitStructure);           //调用配置初始化

    ReferenceVoltage =DAC_InitStructure.m_ReferenceVoltage;

	if((ReferenceVoltage = DAC_RFS_2V)||(ReferenceVoltage = DAC_RFS_1P2V))
	{
		OSC_Backup_Write_Read_Enable (TRUE);               //允许配置备份区寄存器读写
		PM_Reference_Voltage_Enable (TRUE);                //使能参考电压模块
	}
	 DAC_Cmd(DACx,TRUE);                                  //DAC使能
}



void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现DAC 噪声发生器配置功能。
	    例程中用DAC0、DAC1输出噪声发生器，触发源选择软件触发应用
     *挂示波器可以测量到PA0、PA4口为噪声波
     *注意选用PA0作为DAC不能再重映射其他功能，用内部参考电压时要打开备份域读写功
	 */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//时钟初始化
	GPIO_Reset(GPIOA_SFR);
	DAC_Init_Config(DAC0_SFR,DAC_TRIGGER_SOFTWARE_TRGO);//配置DAC0 噪声输出初始化参数，并选择软件触发
	DAC_Init_Config(DAC1_SFR,DAC_TRIGGER_SOFTWARE_TRGO);//配置DAC1 噪声输出初始化参数，并选择软件触发
    while(1)
    {
    	DAC_Software_Trigger_Cmd(DAC0_SFR,TRUE);    //软件使能触发DAC0功能
    	DAC_Software_Trigger_Cmd(DAC1_SFR,TRUE);    //软件使能触发DAC1功能
    }

}




void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

