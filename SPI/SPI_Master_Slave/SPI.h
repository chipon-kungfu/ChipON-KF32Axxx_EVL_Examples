/**
  ******************************************************************************
  *
  * 文件名  SPI.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了SPI主从模式例程相关的函数与变量定义
  *
  ******************************************************************************/
#ifndef _SPI_H_
#define _SPI_H_

#define  SPI_MASTER  1    //SPI模式选择，1=主模式  0=从模式
#define  Command     0xAA //命令字
#define  Dummy       0x00 //dummy符
#define  BufferSize  36   //Tx长度
#define  SPI_COM     SPI1_SFR//SPI1寄存器入口地址
/* 变量 ------------------------------------------------------- */
extern volatile uint8_t  Command_flag;//命令标志位
extern volatile uint8_t  Dummy_flag;  //dummy标志位
extern volatile uint8_t  Uart_flag;   //串口标志位
extern volatile uint32_t Tx_length;   //发送长度变量
extern volatile uint32_t Rx_length;   //发送长度变量
extern uint8_t Tx_master[BufferSize]; //主机Tx
extern uint8_t Tx_slave[BufferSize];  //从机Tx
extern uint8_t Read_data[BufferSize]; //接收缓存
/* 功能函数 ------------------------------------------------------- */
void SPI_Init_Configuration(SPI_SFRmap* SPIx); //SPI配置

#endif /* SPI_H_ */
