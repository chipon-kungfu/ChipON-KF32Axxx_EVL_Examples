/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"

volatile char CCP0_Flag =0;



//*****************************************************************************************
//                              T0中断函数
//*****************************************************************************************

void __attribute__((interrupt))_T0_exception (void)
{
	GPTIM_Clear_Updata_INT_Flag(T0_SFR);										//清更新时间标志位
	GPTIM_Clear_Overflow_INT_Flag (T0_SFR);										//清T0溢出中断标志位

	if(CCP0_Flag)
	{
		CCP0_Flag =0;
		CCP_Compare_Mode_Config(CCP0_SFR,CCP_CHANNEL_1,CCP_CMP_ACTIVE_LEVEL);		//比较模式，匹配时输出高电平
		CCP_Set_Compare_Result(CCP0_SFR,CCP_CHANNEL_1,0x8000);						//设置比较值，
		GPTIM_Set_Period(T0_SFR,0xF000);											//更新周期值
		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_13);                 //PB13翻转
	}
	else
	{
		CCP0_Flag =1;
		CCP_Compare_Mode_Config(CCP0_SFR,CCP_CHANNEL_1,CCP_CMP_INACTIVE_LEVEL);	//比较模式，匹配时输出低电平
		CCP_Set_Compare_Result(CCP0_SFR,CCP_CHANNEL_1,0x8000);					//设置比较值
		GPTIM_Set_Period(T0_SFR,0xF000);										//更新周期值
		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_13);             //PB13翻转
	}

	GPTIM_Updata_Immediately_Config(T0_SFR,FALSE);			//禁止立即更新控制位
	GPTIM_Updata_Immediately_Config(T0_SFR,TRUE);           //允许立即更新控制位


}
