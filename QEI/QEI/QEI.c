/**
  ******************************************************************************
  * 文件名  QEI.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了SPI控制外置FLASH的相关功能，包括
  *          + QEI配置
  *          + T0配置（用于模拟正交编码）
  ******************************************************************************/
#include "system_init.h"
#include "QEI.h"

/**
  * 描述    T0配置（用于模拟正交编码）
  * 输入   无
  * 返回   无
  */
void QT0_Configuration()
{
	//定时器T0为时基模拟正交编码器信号
	TIM_Reset(T0_SFR);													//定时器外设复位，使能外设时钟
	GPTIM_Updata_Enable(T0_SFR,TRUE);									//周期、预分频寄存器允许更新
	GPTIM_Work_Mode_Config(T0_SFR,GPTIM_TIMER_MODE);					//定时模式

	GPTIM_Set_Counter(T0_SFR,0);										//计数值初始化=0
	GPTIM_Set_Period(T0_SFR,0xFFFF);								    //定时器周期=0xFFFF
	GPTIM_Set_Prescaler(T0_SFR,0);										//定时器预分频值

	GPTIM_Counter_Mode_Config(T0_SFR,GPTIM_COUNT_DOWN_UF);				//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(T0_SFR,GPTIM_SCLK);								//选用外设高频时钟
	INT_Interrupt_Priority_Config(INT_T0,4,0);							//抢占优先级4,子优先级0
	GPTIM_Overflow_INT_Enable(T0_SFR,TRUE);								//计数溢出中断使能
	INT_Interrupt_Enable(INT_T0,TRUE);									//外设中断使能
	INT_Clear_Interrupt_Flag(INT_T0);									//清中断标志
	GPTIM_Cmd (T0_SFR, TRUE);
}


/**
  * 描述    QEI配置
  * 输入   QEIx：指向QEI内存结构的指针，取值为QEI0_SFR、QEI1_SFR
  * 返回   无
  */
void QEI_Init_Configuration(QEI_SFRmap* QEIx)
{
	QEI_InitTypeDef qeiInitStruct;

    qeiInitStruct.m_IndexReset = FALSE; //位置计数器复位:禁止, 索引脉冲QEI_INDEX不复位位置计数器
    qeiInitStruct.m_DirectionEn = TRUE; //位置计数器方向状态输出:使能
    qeiInitStruct.m_WorkClock = QEI_SOURCE_SCLK;//QEI时钟:系统时钟
    qeiInitStruct.m_DigitalFilterEn = TRUE;     //QEI数字滤波器输出:使能
    qeiInitStruct.m_DigitalFilterPrescaler = QEI_DIGITAL_FILTER_CLK_DIV_2;//QEI数字滤波器时钟2分频
    qeiInitStruct.m_SwapABEn = FALSE;    // A相和B相输入交换:禁止
    qeiInitStruct.m_Counter = 0;         //定时器计数值=0
    qeiInitStruct.m_Period = 3;          //定时器周期值=0  Tx==PPx时，下次Tx脉冲Tx清零
    qeiInitStruct.m_Prescaler = 0;       //定时器预分频=0
    QEI_Reset(QEIx);                     //QEI复位
    QEI_Configuration(QEIx,&qeiInitStruct);//写入配置
    QEI_Encoder_Mode_Config(QEIx,QEI_X2_MATCH_RESET);//x2模式匹配复位

    QEI_Clear_Timer_Overflow_INT_Flag(QEIx); //清T7溢出标志
    QEI_Timer_Overflow_INT_Enable(QEIx,TRUE);//T7溢出中断使能
	INT_Interrupt_Enable(INT_T7,TRUE);       //T7中断使能
	INT_All_Enable (TRUE);                   //中断使能

}
