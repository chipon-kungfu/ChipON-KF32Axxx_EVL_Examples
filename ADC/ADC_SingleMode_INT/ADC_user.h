/*
 * adc.h
 *
 *  Created on: 2019-11-5
 *      Author: charles_cai
 */

#ifndef ADC_H_
#define ADC_H_
#define ADC_UNIT   ADC0_SFR
#define ADC_INTx   INT_ADC0
void Init_adc();
void Init_adc_INT(void);
extern void Delay(uint32_t cnt);

#endif /* ADC_H_ */
