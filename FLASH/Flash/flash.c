/**
  ********************************************************************
  * 文件名  flash.c
  * 作  者   ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2024-04-16
  * 描  述  该文件提供了flash应用读写函数。
  *
  *********************************************************************
*/


#include "system_init.h"
#include "flash.h"

static unsigned int Flash_Address = 0x10000;
static unsigned int Flash_Length = 0;


/**
 *  @brief :Flash continue reads n bytes
 *  @param[in]
 *              Address :Starting  address
 *                 Length :Read length
 *  @param[out] Buffers :Read data to Buffers
 *  @retval :None
 */
__attribute__((noinline, optimize("-O0"))) void FlashReadNByte(unsigned int Address, unsigned int Length, unsigned char *Buffers)
{
    for (int i = 0; i < Length; i++)
    {
        Buffers[i] = *(unsigned char *)Address++;
    }
}

/**
 *  @brief :Erase N pages. The minimum erasure unit is 1K.
 *             During this period, the whole play will be close !!!
 *             Recovery interrupt at the end !!!
 *  @param[in]
 *              Address :address aligned by 1K
 *                 Length :length like 1024 2048 3072 4096...
 *  @param[out] None
 *  @retval :    CMD_SUCCESS                                    0x00
 *                BUSY                                          0x0B
 *                SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION       0x09
 *                INVALID_SECTOR                                0x07
 */
__attribute__((noinline, optimize("-O0"))) uint32_t __FlashEraseNPage(volatile unsigned key,unsigned int Address, unsigned int Length)
{
   uint32_t FlashRetVal, IntState;

   if (((Length & 0x3FF) > 0) || (0 == Length))
   {
	   return PARAM_ERROR;
   }

   IntState = (INT_CTL0 & INT_CTL0_AIE);
   SFR_CLR_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
   for (int i = 0; i < Length; i += 0x400)
   {
	   FlashRetVal = __FLASH_Erase__(key, (Address+i), 0x400);
	   if (FlashRetVal != CMD_SUCCESS)
	   {
		   break;
	   }
   }
   if (IntState != 0)
       SFR_SET_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
   return FlashRetVal;
}

/**
 *  @brief :Write N Kbytes.
 *             Erase first and write then.
 *             During this period, the whole play will be close !!!
 *             Recovery interrupt at the end !!!
 *  @param[in]
 *              Address :address aligned by 1K
 *                 Length :length like 1024 2048 3072 4096...
 *                 Buffers:input data
 *  @param[out] None
 *  @retval :    CMD_SUCCESS                                 0x00
 *               SRC_ADDR_ERROR                              0x02
 *               DST_ADDR_ERROR                              0x03
 *               SRC_ADDR_NOT_MAPPED                         0x04
 *               DST_ADDR_NOT_MAPPED                         0x05
 *               COUNT_ERROR(not bytes 1K 2K 3K 4K)          0x06
 *               INVALID_SECTOR                              0x07
 *               SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION     0x09
 *               BUSY                                        0x0B
 */
__attribute__((noinline, optimize("-O0"))) uint32_t __FlashWriteNKBytes(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers)
{
    uint32_t i, FlashRetVal, IntState;

    if (((Length & 0x3FF) > 0) || (0 == Length))
    {
 	   return PARAM_ERROR;
    }

    IntState = (INT_CTL0 & INT_CTL0_AIE);
    SFR_CLR_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    for (int i = 0; i < Length; i += 0x400)
    {
        FlashRetVal = __FLASH_Erase__(key, (Address+i), 0x400);
        if (FlashRetVal == CMD_SUCCESS)
        {
            FlashRetVal = __FLASH_Program__(key, (Address+i), 0x400, (unsigned int *)(&Buffers[i]));
        }
 	   if (FlashRetVal != CMD_SUCCESS)
 	   {
 		   break;
 	   }
    }
    if (IntState != 0)
        SFR_SET_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    return FlashRetVal;
}

/**
 *  @brief :Write 8*N bytes.
 *             No Erase .
 *             During this period, the whole play will be close !!!
 *             Recovery interrupt at the end !!!
 *  @param[in]
 *              Address :
 *                 Length :must align by 8,like 8 16 24 32 ... 8*N, or aligned by 16*N
 *                 Buffers:input data
 *  @retval :    CMD_SUCCESS                                     0x00
 */
__attribute__((optimize("-O0"))) uint32_t __FlashWrite8NBytesNoErase(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers)
{
    uint32_t FlashRetVal, IntState;

    if (((Length & 0x7) > 0)  || (0 == Length))
    {
 	   return PARAM_ERROR;
    }

    IntState = (INT_CTL0 & INT_CTL0_AIE);
    SFR_CLR_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    for (int i = 0; i < Length; i += 0x8)
    {
    	FlashRetVal = __FLASH_Program_NBytes__(key, (Address+i), 0x8, (unsigned int *)(&Buffers[i]));
 	   if (FlashRetVal != CMD_SUCCESS)
 	   {
 		   break;
 	   }
    }
    if (IntState != 0)
        SFR_SET_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    return FlashRetVal;
}
