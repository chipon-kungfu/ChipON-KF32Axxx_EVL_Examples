/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  示范CFGL的使用方法。
  *
  *********************************************************************
  */
#include "system_init.h"
#include "CFGL_user.h"
/******************************************
 * CFGL的信号源配置：
 *  信号源1：CFGL1_REGIN
 *  信号源2：LFCLK
 *  信号源3：HFCLK
 *  信号源4：CFGL1IN0
 * CFGL的数据门配置：
 * 	门1：CFGL1_REGIN 信号的正向输入，其他信号都屏蔽
 * 	门2：LFCLK 信号的正向输入，其他信号都屏蔽
 * 	门3：HFCLK 信号的正向输入，其他信号都屏蔽
 * 	门4：CFGL1IN0 信号的正向输入，其他信号都屏蔽
 * CFGL逻辑功能的配置：
 * 	配置为与或逻辑 - (G1 && G2) || (G3 && G4)
 * 实际功能是：软件寄存器-CFGL1_REGIN控制LFCLK是否进入下级，PH13管脚-CFGL1IN0控制HFCLK是否进入下级
 * 		         以上二者的信号进行或运算，运算结果通过PA3-CFGL1_OUT输出。
 * 		   CFGL1_REGIN配置为1，LFCLK是可以输出的。PH13为低电平的时候，HFCK不能输出，PA3输出LFCLK
 * 		   PH13为高电平的时候，HFCK可以输出，PA3输出LFCLK和HFCK或运算的结果。
 *****************************************/
void Init_Cfgl()
{
	CFGL_InitTypeDef CFGLStruct;
	CFGL_Reset ();

	CFGLStruct.m_Module_EN=CFGL_ENABLE;                 /*CFGL 逻辑单元使能
	                                 取值为宏“CFGL 逻辑单元使能禁止选择”中的一个*/
	CFGLStruct.m_Output_EN=CFGL_OUTPUT_ENABLE;         /*CFGL 输出使能选择
	                                 取值为宏“CFGL 输出使能”中的一个*/
	CFGLStruct.m_RaiseINT_EN=CFGL_RISEINT_ENABLE;            /*CFGL 上升边沿中断选择
	                                 取值为宏“CFGL 上升边沿中断选择”中的一个*/
	CFGLStruct.m_FallINT_EN=CFGL_FALLINT_ENABLE;             /*CFGL 下降边沿中断选择
	                                 取值为宏“CFGL 下降边沿中断选择”中的一个*/
	CFGLStruct.m_Mode_Sel=CFGL_AND_OR;         /*CFGL 可配置逻辑单元功能模式选择
	                                 取值为宏“CFGL 可配置逻辑单元功能模式”中的一个*/
	CFGLStruct.m_Output_Pol=CFGL_OUT_INPHASE;     /*CFGL输出极性选择位
	                                 取值为宏“CFGL输出极性”中的一个*/
	CFGLStruct.m_G4Output_POL=CFGL_G4POL_INPHASE;     /*CFGL门4输出极性选择
	                                 取值为宏“CFGL门4输出极性”中的一个*/
	CFGLStruct.m_G3Output_POL=CFGL_G3POL_INPHASE;     /*CFGL门3输出极性选择
	                                 取值为宏“CFGL门3输出极性”中的一个*/
	CFGLStruct.m_G2Output_POL=CFGL_G2POL_INPHASE;     /*CFGL门2输出极性选择
	                                 取值为宏“CFGL门2输出极性”中的一个*/
	CFGLStruct.m_G1Output_POL=CFGL_G1POL_INPHASE;     /*CFGL门1输出极性选择
	                                 取值为宏“CFGL门1输出极性”中的一个*/
	CFGLStruct.m_G4Input_Sel=CFGL_G4INPUT_CFGL1CH0;     /*CFGL门4输入数据通道选择	(CFGL1IN0)
	                                 取值为宏“CFGL门4输入数据”中的一个*/
	CFGLStruct.m_G3Input_Sel=CFGL_G3INPUT_CFGL1CH9;     /*CFGL门3输入数据通道选择	(HFCLK)
	                                 取值为宏“CFGL门3输入数据”中的一个*/
	CFGLStruct.m_G2Input_Sel=CFGL_G2INPUT_CFGL1CH8;     /*CFGL门2输入数据通道选择	(LFCLK)
	                                 取值为宏“CFGL门2输入数据”中的一个*/
	CFGLStruct.m_G1Input_Sel=CFGL_G1INPUT_CFGL1CH4;     /*CFGL门1输入数据通道选择位	(CFGL1_REGIN)
	                                 取值为宏“CFGL门1输入数据”中的一个*/
	CFGLStruct.m_CH4Data_Sel=CFGL_CH4_INPUT_1;     /*CFGL CH4通道输入数据位选择		(CFGL1_REGIN=1)
	                                 取值为宏“CFGL CH4通道输入数据位”中的一个*/
	CFGLStruct.m_CH5CH6Data_Sel=CFGL_CH5_T0TRGO_CH6_CCP0CH1OUT;     /*CH5\CH6通道选择
	                                 取值为宏“CFGLCH5\CH6通道选择”中的一个*/

	CFGLStruct.m_G4D4_Inphase_EN=TRUE;/*门4数据4同向允许通道选择
	                                 取值为宏“门4数据4同向允许选择”中的一个*/
	CFGLStruct.m_G4D4_Inverse_EN=FALSE;/*门4数据4反向允许通道选择
	                                 取值为宏“门4数据4反向允许选择”中的一个*/
	CFGLStruct.m_G4D3_Inphase_EN=FALSE;/*门4数据3同向允许通道选择		//门4选择HFCLK
	                                 取值为宏“门4数据3同向允许选择”中的一个*/
	CFGLStruct. m_G4D3_Inverse_EN=FALSE;/*门4数据3反向允许通道选择
	                                 取值为宏“门4数据3反向允许选择”中的一个*/

	CFGLStruct. m_G4D2_Inphase_EN=FALSE;/*门4数据2同向允许通道选择
	                                 取值为宏“门4数据2同向允许选择”中的一个*/
	CFGLStruct. m_G4D2_Inverse_EN=FALSE;/*门4数据2反向允许通道选择
	                                 取值为宏“门4数据2反向允许选择”中的一个*/
	CFGLStruct. m_G4D1_Inphase_EN=FALSE;/*门4数据1同向允许通道选择
	                                 取值为宏“门4数据1同向允许选择”中的一个*/
	CFGLStruct. m_G4D1_Inverse_EN=FALSE;/*门4数据1反向允许通道选择
	                                 取值为宏“门4数据1反向允许选择”中的一个*/

	CFGLStruct. m_G3D4_Inphase_EN=FALSE;/*门3数据4同向允许通道选择
	                                 取值为宏“门3数据4同向允许选择”中的一个*/
	CFGLStruct. m_G3D4_Inverse_EN=FALSE;/*门3数据4反向允许通道选择
	                                 取值为宏“门3数据4反向允许选择”中的一个*/
	CFGLStruct. m_G3D3_Inphase_EN=TRUE;/*门3数据3同向允许通道选择		//门3选择HFCLK
	                                 取值为宏“门3数据3同向允许选择”中的一个*/
	CFGLStruct. m_G3D3_Inverse_EN=FALSE;/*门3数据3反向允许通道选择
	                                 取值为宏“门3数据3反向允许选择”中的一个*/

	CFGLStruct. m_G3D2_Inphase_EN=FALSE;/*门3数据2同向允许通道选择
	                                 取值为宏“门4数据2同向允许选择”中的一个*/
	CFGLStruct. m_G3D2_Inverse_EN=FALSE;/*门3数据2反向允许通道选择
	                                 取值为宏“门3数据2反向允许选择”中的一个*/
	CFGLStruct. m_G3D1_Inphase_EN=FALSE;/*门3数据1同向允许通道选择
	                                 取值为宏“门3数据1同向允许选择”中的一个*/
	CFGLStruct. m_G3D1_Inverse_EN=FALSE;/*门3数据1反向允许通道选择
	                                 取值为宏“门3数据1反向允许选择”中的一个*/

	CFGLStruct. m_G2D4_Inphase_EN=FALSE;/*门2数据4同向允许通道选择
	                                 取值为宏“门2数据4同向允许选择”中的一个*/
	CFGLStruct. m_G2D4_Inverse_EN=FALSE;/*门2数据4反向允许通道选择
	                                 取值为宏“门2数据4反向允许选择”中的一个*/
	CFGLStruct. m_G2D3_Inphase_EN=FALSE;/*门2数据3同向允许通道选择
	                                 取值为宏“门2数据3同向允许选择”中的一个*/
	CFGLStruct. m_G2D3_Inverse_EN=FALSE;/*门2数据3反向允许通道选择
	                                 取值为宏“门2数据3反向允许选择”中的一个*/

	CFGLStruct. m_G2D2_Inphase_EN=TRUE;/*门2数据2同向允许通道选择
	                                 取值为宏“门2数据2同向允许选择”中的一个*/
	CFGLStruct. m_G2D2_Inverse_EN=FALSE;/*门2数据2反向允许通道选择
	                                 取值为宏“门2数据2反向允许选择”中的一个*/
	CFGLStruct. m_G2D1_Inphase_EN=FALSE;/*门2数据1同向允许通道选择		//门2选择CFGL1_REGIN
	                                 取值为宏“门2数据1同向允许选择”中的一个*/
	CFGLStruct. m_G2D1_Inverse_EN=FALSE;/*门2数据1反向允许通道选择
	                                 取值为宏“门2数据1反向允许选择”中的一个*/

	CFGLStruct. m_G1D4_Inphase_EN=FALSE;/*门1数据4同向允许通道选择
	                                 取值为宏“门1数据4同向允许选择”中的一个*/
	CFGLStruct. m_G1D4_Inverse_EN=FALSE;/*门1数据4反向允许通道选择
	                                 取值为宏“门1数据4反向允许选择”中的一个*/
	CFGLStruct. m_G1D3_Inphase_EN=FALSE;/*门1数据3同向允许通道选择
	                                 取值为宏“门1数据3同向允许选择”中的一个*/
	CFGLStruct. m_G1D3_Inverse_EN=FALSE;/*门1数据3反向允许通道选择
	                                 取值为宏“门1数据3反向允许选择”中的一个*/

	CFGLStruct. m_G1D2_Inphase_EN=FALSE;/*门1数据2同向允许通道选择
	                                 取值为宏“门1数据2同向允许选择”中的一个*/
	CFGLStruct. m_G1D2_Inverse_EN=FALSE;/*门1数据2反向允许通道选择
	                                 取值为宏“门1数据2反向允许选择”中的一个*/
	CFGLStruct. m_G1D1_Inphase_EN=TRUE;/*门1数据1同向允许通道选择		//门1选择CFGL1_REGIN
	                                 取值为宏“门1数据1同向允许选择”中的一个*/
	CFGLStruct. m_G1D1_Inverse_EN=FALSE;/*门1数据1反向允许通道选择
	                                 取值为宏“门1数据1反向允许选择”中的一个*/

	CFGL_Configuration (CFGL1_SFR, &CFGLStruct);
}
