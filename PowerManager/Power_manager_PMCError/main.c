/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2022-03-16
  *
  * 本例程实现在 WKPxSTA 位为1的情况下，执行SLEEP指令时，
  * 系统进入普通休眠模式从而无法唤醒MCU的一种处理机制。
  * 使用的外设为：串口2：PB14/PB15, 115200
  *              按键：PD7（按下系统进入休眠）
  *              Wakeup引脚：PC8
  * 请参考对应应用笔记理解和使用

  *********************************************************************
  */
#include "system_init.h"
#include "Power_manage_user.h"
#include "EXINT_user.h"
#include "USART_user.h"
#include <stdio.h>

#define Read_user_key() GPIO_Read_Input_Data_Bit(GPIOD_SFR, GPIO_PIN_MASK_7)

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}

void System_User_Init(void)
{
	//系统时钟,外设高频时钟配置
	SystemInit();

	BKP_Write_And_Read_Enable(TRUE); //备份域读写使能
	PM_CTL0 &= (~PM_CTL0_IOLATCH);
	BKP_Write_And_Read_Enable(FALSE);

	//PD7配置为输入模式,user按键检测
	GPIO_Write_Mode_Bits(GPIOD_SFR,GPIO_PIN_MASK_7,GPIO_MODE_IN);

	//Wakeup 引脚配置
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_8,GPIO_MODE_IN);
	GPIO_Pull_Up_Enable(GPIOC_SFR,GPIO_PIN_MASK_8, TRUE);

	//配置USART2引脚重映射
	GPIO_USART();
	//全双工异步8bit 115200波特率
	USART_Async_config(USART2_SFR);

}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{

	System_User_Init();

	fprintf(USART2_STREAM, "Power On\r\n");

	while(1)
	{
		Delay(0x1E0000);

		fprintf(USART2_STREAM, "Running\r\n");

		if(Read_user_key())    //按键按下
		{
			Delay(0xFFFF);     //延时用于确认按键
			if(Read_user_key())//确认按键按下
			{
				while(Read_user_key()); //等待按键释放
				fprintf(USART2_STREAM, "Go to standby...\r\n");
				PMC_Error_Init();
				/*
				 * 执行休眠，如果休眠成功，则唤醒时代码从头跑
				 * 如果休眠失败，则会进入_EINT19TO17_exception，然后代码接着跑
				 */
				Goto_StandBy();
				fprintf(USART2_STREAM, "Wake up by PMC error\r\n");
			}
		}
	}
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
};
