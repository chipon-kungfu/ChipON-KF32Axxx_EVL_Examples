/**
  ******************************************************************************
  *
  * 文件名  lin_slave.h
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了LIN_SLAVE例程相关配置函数
  *
  ******************************************************************************/

#ifndef _LIN_SLAVE_H_
#define _LIN_SLAVE_H_

extern volatile uint8_t l_sys_mode;     //从机接收模式标志
extern volatile uint8_t Rev_Temp;       //接收数据（清接收中断标志
extern volatile uint8_t Rev_Temp_Clear;       //接收数据（清接收中断标志
extern volatile uint8_t l_rec_data[10]; //接收数据
extern uint8_t l_Tx_data[10]; 	 		 //发送数据
extern uint8_t lin_re_state;            //主机写，从机接收数据状态
extern uint8_t xxx;             	     //计数

extern void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//串口发送函数
extern uint8_t Check_Sum(uint8_t* Databuf,uint32_t point,uint32_t length);  //标准校验和
extern void USART_LIN_config(USART_SFRmap *USARTx);//串口异步全双工配置，唤醒使能
extern void USART_LIN_INT_config(USART_SFRmap *USARTx,InterruptIndex Peripheral);//串口接收中断使能，自动唤醒中断使能

#endif /* _LIN_SLAVE_H_ */
