/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */

#include "system_init.h"
#include "GPTIMER_user.h"
#ifndef GPTIMER_C_
#define GPTIMER_C_

/**
  * 描述   初始化T1
  * 输入   无
  * 返回   无
  */
void Init_T1()
{
	 //	T1触发使能配置
	TIM_Reset(T1_SFR);//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(T1_SFR,TRUE);//立即更新控制   TxUR, 立刻强制更新生效此刻的寄存器
	GPTIM_Updata_Enable(T1_SFR,TRUE);//配置更新使能   TXUDEN
	GPTIM_Work_Mode_Config(T1_SFR,GPTIM_TIMER_MODE);//定时模式选择  TxCS
	GPTIM_Master_Mode_Config(T1_SFR,GPTIM_MASTER_TXIF_SIGNAL);//配置主模式选择TXIF作为触发
	GPTIM_Set_Counter(T1_SFR,0);//定时器计数值
	GPTIM_Set_Period(T1_SFR,0X1000);//定时器周期值
	GPTIM_Set_Prescaler(T1_SFR,0x0200);//定时器预分频值
	GPTIM_Counter_Mode_Config(T1_SFR,GPTIM_COUNT_UP_DOWN_OUF);//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(T1_SFR,GPTIM_HFCLK);//选用HFCLK时钟
	GPTIM_Overflow_INT_Enable(T1_SFR,TRUE);//计数溢出中断使能
	GPTIM_Cmd(T1_SFR,TRUE);//定时器启动控制使能
}

/**
  * 描述  T1中断的初始化
  * 输入   无
  * 返回   无
  */
void Init_T1_it()
{
 	INT_Interrupt_Priority_Config(INT_T1,4,0);//抢占优先级4,子优先级0
 	INT_Interrupt_Enable(INT_T1,TRUE);//外设中断使能
 	INT_Clear_Interrupt_Flag(INT_T1);//清中断标志
}

#endif /* GPTIMER_C_ */
