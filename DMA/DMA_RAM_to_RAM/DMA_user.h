/**
  ******************************************************************************
  * 文件名  DMA_user.h
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了DMA用户驱动头文件声明
  *
  *********************************************************************
  */

#ifndef DMA_USER_H_
#define DMA_USER_H_

void DMA_init(DMA_SFRmap *DMAx);
extern uint32_t buf_1[20];
extern uint32_t buf_2[20];

#endif /* DMA_USER_H_ */
