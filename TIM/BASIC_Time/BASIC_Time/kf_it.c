/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
volatile uint8_t Time14_CNT;
volatile uint8_t Time15_CNT;

//*****************************************************************************************
//                              T14中断函数
//*****************************************************************************************	//

void __attribute__((interrupt))_T14_exception (void)
{

	BTIM_Clear_Updata_INT_Flag(T14_SFR);										//清更新时间标志位
	BTIM_Clear_Overflow_INT_Flag (T14_SFR);										//清T14溢出中断标志位

	Time14_CNT++;
	if(Time14_CNT>=10)//100ms进行一次翻转
	{
		Time14_CNT =0;
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);               //PB13翻转 LED0闪烁
	}


}

//*****************************************************************************************
//                              T15中断函数
//*****************************************************************************************	//

void __attribute__((interrupt))_T15_exception (void)
{

	BTIM_Clear_Updata_INT_Flag(T15_SFR);									 //清更新时间标志位
	BTIM_Clear_Overflow_INT_Flag (T15_SFR);									//清T15溢出中断标志位

	Time15_CNT++;
	if(Time15_CNT>=10)  //100ms进行一
	{
		Time15_CNT =0;
	}


}

