/*
 * T6_user.h
 *
 *  Created on: 2019-12-4
 *      Author: charles_cai
 */

#ifndef T6_USER_H_
#define T6_USER_H_

void Tz_init(ATIM_SFRmap* ATIMx);
void Tz_INT_int(InterruptIndex Peripheral);

#endif /* T6_USER_H_ */
