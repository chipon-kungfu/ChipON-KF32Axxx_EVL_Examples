/**
  ******************************************************************************
  * 文件名  Usart.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了串口例程相关配置函数，包括
  *          + 串口发送
  *          + 串口异步配置
  *          + 串口同步配置
  *          + 串口接收中断使能
  ******************************************************************************/
#include "system_init.h"
#include "Usart.h"

void USART_Async_config(USART_SFRmap *USARTx);//串口异步全双工配置
void USART_Sync_config(USART_SFRmap* USARTx); //串口半双工同步配置
void USART_ReceiveInt_config(USART_SFRmap *USARTx,InterruptIndex Peripheral);//串口接收中断使能
void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//串口发送函数
volatile uint8_t USART_Array_rec[256]={0};
/**
  * 描述   串口发送
  * 输入   USARTx:   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Databuf：   指向发送数据的指针
  *      length：      发送的长度
  * 返回   无
  */
void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length)
{
	uint32_t i;
	for(i=0;i<length;i++)
	{
		//串口发送
		USART_SendData(USARTx,Databuf[i]);
		if(USART_Get_Receive_BUFR_Ready_Flag(USARTx) == 1)
		{
			USART_ReceiveData(USARTx);
		}
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	}
}
void DMA_USART2Rx_Init(DMA_SFRmap *DMAx)
{
	DMA_InitTypeDef dmaNewStruct;
	/* DMA复位 */
	DMA_Reset (DMAx);
	/* DMA功能配置 */
	DMA_Struct_Init( &dmaNewStruct );
	/* 配置 传输数据个数: 256 */
	dmaNewStruct.m_Number = 256;
	/* 配置 DMA传输方向：内存 到外设*/
	dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
	/* 配置 DMA通道优先级：最高优先级 */
	dmaNewStruct.m_Priority = DMA_CHANNEL_LOWER;
	/* 配置 外设数据位宽：8位宽 */
	dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
	/* 配置 存储器数据位宽:8位宽 */
	dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;
	/* 配置 外设地址增量模式使能: 禁止 */
	dmaNewStruct.m_PeripheralInc = 0;
	/* 配置 存储器地址增量模式使能: 使能 */
	dmaNewStruct.m_MemoryInc = 1;
	/* 配置 DMA通道选择:DMA0-通道6*/
	dmaNewStruct.m_Channel = USART2_RXDMA_CHAN;
	/* 配置 数据块传输模式： */
	dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BYTE; //DMA_TRANSFER_BLOCK;//
	/* 配置 循环模式使能: 使能 */
	dmaNewStruct.m_LoopMode = 1;
	/* 配置 外设起始地址：等待接收的数据的起始地址 */
	dmaNewStruct.m_PeriphAddr = (uint32_t)&USART2_SFR->RBUFR ; //T1
	/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
	dmaNewStruct.m_MemoryAddr = (uint32_t)USART_Array_rec;

	/* 配置DMA功能函数 */
	DMA_Configuration (DMAx, &dmaNewStruct); //将参数写入DMAx寄存器
	DMA_Memory_To_Memory_Enable (DMAx, USART2_RXDMA_CHAN, FALSE);
	DMA_Finish_Transfer_INT_Enable(DMAx,USART2_RXDMA_CHAN,TRUE);

}

/**
  * 描述  串口异步全双工配置(默认8bit收发使能  全双工 115200)
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void USART_Async_config(USART_SFRmap *USARTx)
{
	USART_InitTypeDef USART_InitStructure;

	USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode=USART_MODE_FULLDUPLEXASY;                        //全双工
    USART_InitStructure.m_TransferDir=USART_DIRECTION_FULL_DUPLEX;              //传输方向
    USART_InitStructure.m_WordLength=USART_WORDLENGTH_8B;                       //8位数据
    USART_InitStructure.m_StopBits=USART_STOPBITS_1;                            //1位停止位
    USART_InitStructure.m_BaudRateBRCKS=USART_CLK_HFCLK;                        //内部高频时钟作为 USART波特率发生器时钟

    /* 波特率 =Fck/(16*z（1+x/y)) 外设时钟内部高频16M*/
    //4800    z:208    x:0    y:0
    //9600    z:104    x:0    y:0
    //19200   z:52     x:0    y:0
    //115200  z:8      x:1    y:13
    //波特率115200
    USART_InitStructure.m_BaudRateInteger=8;         //USART波特率整数部分z，取值为0~65535
    USART_InitStructure.m_BaudRateNumerator=1;         //USART波特率小数分子部分x，取值为0~0xF
    USART_InitStructure.m_BaudRateDenominator=13;       //USART波特率小数分母部分y，取值为0~0xF


	USART_Reset(USARTx);                                       //USARTx复位
	USART_Configuration(USARTx,&USART_InitStructure);          //USARTx配置
    USART_Passageway_Select_Config(USARTx,USART_U7816R_PASSAGEWAY_TX0);//UASRTx选择TX0通道
	USART_Clear_Transmit_BUFR_INT_Flag(USARTx);                //USARTx发送BUF清零
	USART_RESHD_Enable (USARTx, TRUE);						   //使能RESHD位
	USART_Cmd(USARTx,TRUE);                                    //USARTx使能
}

/**
  * 描述   串口半双工同步配置(默认主模式，9bit发送，9600波特率)
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void USART_Sync_config(USART_SFRmap* USARTx)
{
	USART_InitTypeDef USART_InitStructure;

	USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode=USART_MODE_HALFDUPLEXSYN;                        //半双工
    USART_InitStructure.m_HalfDuplexClkSource=USART_MASTER_CLOCKSOURCE_INTER;   //主模式
    USART_InitStructure.m_TransferDir=USART_DIRECTION_TRANSMIT;                 //传输方向"发送"
    USART_InitStructure.m_WordLength=USART_WORDLENGTH_9B;                       //9位数据
    USART_InitStructure.m_Parity=USART_PARITY_ODD;                              //奇校验
    USART_InitStructure.m_BaudRateBRCKS=USART_CLK_HFCLK;                        //内部高频时钟作为 USART波特率发生器时钟

    /* 波特率 =Fck/(16*z（1+x/y)) 外设时钟内部高频16M*/
    //4800    z:208    x:0    y:0
    //9600    z:104    x:0    y:0
    //19200   z:52     x:0    y:0
    //115200  z:8      x:1    y:13
    //波特率9600
    USART_InitStructure.m_BaudRateInteger=104;         //USART波特率整数部分z，取值为0~65535
    USART_InitStructure.m_BaudRateNumerator=0;         //USART波特率小数分子部分x，取值为0~0xF
    USART_InitStructure.m_BaudRateDenominator=0;       //USART波特率小数分母部分y，取值为0~0xF

	USART_Reset(USARTx);                                       //USARTx复位
	USART_Configuration(USARTx,&USART_InitStructure);          //USARTx配置
	USART_Passageway_Select_Config(USARTx,USART_U7816R_PASSAGEWAY_TX0);//UASRTx选择TX0通道
	USART_Clear_Transmit_BUFR_INT_Flag(USARTx);                //USARTx发送BUF清零
	USART_RESHD_Enable (USARTx, TRUE);						   //使能RESHD位
	USART_Cmd(USARTx,TRUE);                                    //USARTx使能
}


/**
  * 描述   串口接收中断配置
  * 输入   USARTx:指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Peripheral:外设或内核中断向量编号，取值范围为：
  *                 枚举类型InterruptIndex中的外设中断向量编号
  * 返回   无
  */
void USART_ReceiveInt_config(USART_SFRmap *USARTx,InterruptIndex Peripheral)
{

	//USART_RDR_INT_Enable(USARTx,TRUE);
	//USART_Clear_Transmit_BUFR_INT_Flag(USARTx);
	USART_Receive_Idle_Frame_Config(USARTx, TRUE);
	USART_IDLE_INT_Enable(USARTx, TRUE);
	INT_Interrupt_Enable(Peripheral,TRUE);
	//sUSART_ReceiveData(USARTx);//清接收标志位
	//INT_All_Enable(TRUE);
}
