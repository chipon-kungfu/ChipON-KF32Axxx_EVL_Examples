/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ADC_user.h"
#include "stdio.h"
#include "DMA_user.h"
extern uint32_t ADC_buf[2];
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               硬件错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                                堆栈错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//*****************************************************************************************
//                              ADC中断函数
//*****************************************************************************************
void __attribute__((interrupt))_ADC0_exception (void)
{

    static uint8_t i=0;
	if(ADC_Get_INT_Flag(ADC_UNIT,ADC_INT_EOC))  //常规通道完成中断
	{
		ADC_Clear_INT_Flag(ADC_UNIT,ADC_INT_EOC);
		ADC_buf[i]=ADC_Get_Conversion_Value(ADC_UNIT);
		i++;
		if(i>2)
		{
			i=0;
			fprintf(USART2_STREAM,"ADC0 EOC interrupt \r\n");
			fprintf(USART2_STREAM,"CH1 ADC值为: %d \r\n",ADC_buf[0]);
	        fprintf(USART2_STREAM,"CH2 ADC值为: %d \r\n",ADC_buf[1]);
		}
	}
}

void __attribute__((interrupt))_ADC1_exception (void)
{
	static uint8_t i=0;
	if(ADC_Get_INT_Flag(ADC_UNIT,ADC_INT_EOC))  //常规通道完成中断
	{
		ADC_Clear_INT_Flag(ADC_UNIT,ADC_INT_EOC);
		ADC_buf[i]=ADC_Get_Conversion_Value(ADC_UNIT);
		i++;
		if(i>2)
		{
			i=0;
			fprintf(USART2_STREAM,"ADC1 EOC interrupt \r\n");
			fprintf(USART2_STREAM,"CH1 ADC值为: %d \r\n",ADC_buf[0]);
			fprintf(USART2_STREAM,"CH2 ADC值为: %d \r\n",ADC_buf[1]);
		}
	}

}

void __attribute__((interrupt))_ADC2_exception (void)
{
	static uint8_t i=0;
	if(ADC_Get_INT_Flag(ADC_UNIT,ADC_INT_EOC))  //常规通道完成中断
	{
		ADC_Clear_INT_Flag(ADC_UNIT,ADC_INT_EOC);
		ADC_buf[i]=ADC_Get_Conversion_Value(ADC_UNIT);
		i++;
		if(i>2)
		{
			i=0;
			fprintf(USART2_STREAM,"ADC2 EOC interrupt \r\n");
			fprintf(USART2_STREAM,"CH1 ADC值为: %d \r\n",ADC_buf[0]);
			fprintf(USART2_STREAM,"CH2 ADC值为: %d \r\n",ADC_buf[1]);
		}
	}

}


//                              DMA0中断函数
//*****************************************************************************************
void __attribute__((interrupt))_DMA0_exception (void)
{
	if(DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, DMA_ADC_CH))
	{
		DMA_Clear_INT_Flag(DMA0_SFR, DMA_ADC_CH, DMA_INT_FINISH_TRANSFER); //DMA0_ch3 对应AD0
		fprintf(USART2_STREAM,"DMA0 interrupt \r\n");
		fprintf(USART2_STREAM,"CH1 ADC值为: %d \r\n",ADC_buf[0]);
		fprintf(USART2_STREAM,"CH2 ADC值为: %d \r\n",ADC_buf[1]);
	}
}

//                              DMA1中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _DMA1_exception (void)
{
	if(DMA_Get_Finish_Transfer_INT_Flag(DMA1_SFR, DMA_ADC_CH))
	{
		DMA_Clear_INT_Flag(DMA1_SFR, DMA_ADC_CH, DMA_INT_FINISH_TRANSFER); //DMA0_ch3 对应AD0
		fprintf(USART2_STREAM,"DMA1 interrupt \r\n");
		fprintf(USART2_STREAM,"CH1 ADC值为: %d \r\n",ADC_buf[0]);
		fprintf(USART2_STREAM,"CH2 ADC值为: %d \r\n",ADC_buf[1]);
	}
}


