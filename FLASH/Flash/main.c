/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: Flash
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者:  ChipON_AE/FAE_Group
 * 描  述  该文件提供了flash读写的应用例程参考
 *
 ****************************************************************************************/
#include "system_init.h"
#include "flash.h"


#define FlashDataSize            2048
#define FlashInformationDataSize 257
/* Flash Data buffer */
uint8_t  FlashDataBuffer[FlashDataSize];
uint8_t  FlashDataReadBuffer[FlashDataSize] = {0};


//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=7000;
		while(j--);
	}

}


/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
	GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_RESET);	  //先设置为低电平

}


//主函数
void main()
{

	/** 写入从0xF000开始的2KBytes长度的flash区域；
	 * 从0xF000读取2KBytes长度的flash内容；
	 * 从0xF000擦除2KBytes长度的flash区域；
	 * 再次读取从0xF000开始的2KBytes长度的flash内容；
	 * 读取从0xF000开始的16Bytes长度的flash内容；
	 * 使用`FlashWrite8NBytesNoErase`写入从0xF000开始的16Bytes长度的flash区域。
	 * 当执行结果错误时，PB13置1，LED亮起
	 * 当执行结果正确时，PB13持续翻转，LED闪烁
	 */

    uint32_t i, FlashRetVal, ExecutionOutcome = 1;

    //系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//初始化PB13，并输出低电平
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);

    /* 首先擦除，然后写入。在函数内部关闭中断，然后恢复 写入'FlashDataBuffer'到地址 0xF000 大约 2K 数据 */

    for (uint32_t i = 0U; i < FlashDataSize; i++)
    {
        FlashDataBuffer[i] = i;
    }

    memset(FlashDataBuffer, 0, FlashDataSize/2);

    FlashRetVal = FlashWriteNKBytes(0xF000, FlashDataSize, (uint8_t *)FlashDataBuffer);
    if (FlashRetVal == CMD_SUCCESS)
    {
    }
    else
    {
		/* 执行结果记为错误 */
        ExecutionOutcome = 0;
    }

    memset(FlashDataReadBuffer, 0x00, FlashDataSize);
    FlashReadNByte(0xF000, FlashDataSize, (uint8_t *)FlashDataReadBuffer);

    for (uint32_t i = 0; i < FlashDataSize; i++)
    {
		if (FlashDataReadBuffer[i] != FlashDataBuffer[i])
		{
			/* 执行结果记为错误 */
        	ExecutionOutcome = 0;
		}
    }

    FlashEraseNPage(0xF000, FlashDataSize);

    FlashReadNByte(0xF000, FlashDataSize, (uint8_t *)FlashDataReadBuffer);

    for (uint32_t i = 0; i < FlashDataSize; i++)
    {
        if (FlashDataReadBuffer[i] != 0xff)
		{
			/* 执行结果记为错误 */
        	ExecutionOutcome = 0;
		}
    }

    {
        FlashReadNByte(0xF000, 16, (uint8_t *)FlashDataReadBuffer);
        for (uint32_t i = 0; i < 16; i++)
        {
            if (FlashDataReadBuffer[i] != 0xff)
			{
				/* 执行结果记为错误 */
        		ExecutionOutcome = 0;
			}
        }
        
        for (uint32_t i = 0; i < 16; i++)
        {
            FlashDataBuffer[i] = i;
        }
        
        FlashRetVal = FlashWrite8NBytesNoErase(0xF000, 16, FlashDataBuffer);
        if (FlashRetVal == CMD_SUCCESS)
        {
        }
        else
        {
			/* 执行结果记为错误 */
        	ExecutionOutcome = 0;
        }
        
        FlashReadNByte(0xF000, 16, FlashDataReadBuffer);
        for (uint32_t i = 0; i < 16; i++)
        {
			if (FlashDataReadBuffer[i] != FlashDataBuffer[i])
			{
				/* 执行结果记为错误 */
        		ExecutionOutcome = 0;
			}
        }
        
    }

    while (1)
    {
        if (ExecutionOutcome == 1)
		{
			/* 当执行结果正确时，PB13持续翻转，LED闪烁 */
			GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);
		}
		else
		{
			/* 当执行结果错误时，PB13置1，LED亮起 */
			GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,1);
		}
		delay_ms(500);
    }
}



/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

