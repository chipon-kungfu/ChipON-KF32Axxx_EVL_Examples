/**
  ******************************************************************************
  * 文件名  CRC.h
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了CRC模块与应用相关函数，包括
  *          + CRC配置
  *          + 硬件CRC运算
  ******************************************************************************/
#ifndef _CRC_H_
#define _CRC_H_

extern void CRC_Init_Config(); //CRC配置
extern uint32_t CRC_CalcBlockCRC(uint32_t* aData,uint32_t length);//硬件CRC运算


#endif /* CRC_H_ */
