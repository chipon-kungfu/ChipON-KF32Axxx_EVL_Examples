/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
#include "USART_user.h"
#include "DMA_user.h"
#include "ADC_user.h"
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               硬件错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                                堆栈错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//                              DMA0中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _DMA0_exception (void)
{
	char i=0;
	unsigned int buf;
	
	if(DMA_Get_Half_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_5)) //实际上ADC产生数据的速度要比串口发送数据的速度快的多，
	{
		buf=0;
		DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_HALF_TRANSFER); //DMA0_ch5 对应AD0
		for(i =0; i<16; i++)
			buf += buf_1[i];
		buf = buf>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF

		//串口发送ADC采样结果高4位
		USART_SendData(USART2_SFR,(unsigned char)(buf>>8));
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USART2_SFR));
		//串口发送ADC采样结果低8位
		USART_SendData(USART2_SFR,(unsigned char)buf);
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USART2_SFR));
	}

	if(DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_5))
	{
		buf=0;
		DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_FINISH_TRANSFER); //DMA0_ch5 对应AD0
		for(i =0; i<16; i++)
			buf += buf_1[i+16];
		buf = buf>>4; //读取ADC数据寄存器,同时硬件会自动清零EOCIF

		//串口发送ADC采样结果高4位
		USART_SendData(USART2_SFR,(unsigned char)(buf>>8));
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USART2_SFR));
		//串口发送ADC采样结果低8位
		USART_SendData(USART2_SFR,(unsigned char)buf);
		//发送完成标志
		while(!USART_Get_Transmitter_Empty_Flag(USART2_SFR));
	}
}

