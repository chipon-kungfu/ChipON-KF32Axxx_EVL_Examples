/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了串口异步收发应用例程参考
  *
 ****************************************************************************************/
#include "system_init.h"
#include "Usart.h"
//USART2_REC_IDLE ASCII
uint8_t USART_Array_Tansmit[]="USART2_REC_IDLE";
uint8_t USART_DMAover_Tansmit[]="DMA_Over!";

volatile uint8_t ReceiveIdlE_flag; //接收标志位
volatile uint8_t DMAReceiveover_flag;

void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}

void GPIO_USART2();//USART2引脚重映射
void Usart_line_feed(USART_SFRmap *USARTx);//换行
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
  
void main()
{
   /* 用户可参考该例程实现USART2的全双工异步收发功能,波特率115200，串口接收
	* 配置为中断方式，接收到0x5A后，主函数发送“ChipON_KF32A151”的ASCII码*/

	//系统时钟120M,外设高频时钟16M
	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_MODE_OUT); //灯
	//配置USART2引脚重映射，PB15-RX，PB14-TX0
	GPIO_USART2();
	
	//全双工异步8bit 115200波特率
	USART_Async_config(USART2_SFR);
	//配置usart2接收DMA
	DMA_USART2Rx_Init(DMA0_SFR);
	//串口接收中断使能
	USART_Receive_Data_Enable(USART2_SFR,TRUE);
    USART_ReceiveInt_config(USART2_SFR,INT_USART2);
    USART_Receive_DMA_INT_Enable(USART2_SFR, TRUE );
	DMA_Channel_Enable(DMA0_SFR,USART2_RXDMA_CHAN,TRUE);
   	INT_Interrupt_Enable(INT_USART2, TRUE);
    INT_Interrupt_Enable(INT_DMA0, TRUE);
    INT_All_Enable(TRUE);
	USART_SendData(USART2_SFR, 0);

	ReceiveIdlE_flag=0;
	DMAReceiveover_flag=0;
	while(1)
	{
		if(ReceiveIdlE_flag)//接收空闲打印USART2_REC_IDLE
		{
		    ReceiveIdlE_flag=0;
		    //发送"USART2_REC_IDLE"
		    USART_Send(USART2_SFR,USART_Array_Tansmit,(sizeof(USART_Array_Tansmit)-1));
		    Usart_line_feed(USART2_SFR);
		}
		if(DMAReceiveover_flag)//超过DMA缓存打印DMA_Over!
		{
			DMAReceiveover_flag=0;
			USART_Send(USART2_SFR,USART_DMAover_Tansmit,(sizeof(USART_DMAover_Tansmit)-1));
		    Usart_line_feed(USART2_SFR);
		}
	}
	
}

/**
  * 描述   USART2引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART2()
{
	/* 端口重映射AF5 */
	//USART2_RX		PB15
	//USART2_TX0	PB14
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_14, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_15, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_14, GPIO_RMP_AF5_USART2);	  //重映射为USART2
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_15, GPIO_RMP_AF5_USART2);     //重映射为USART2
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_14, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_15, TRUE);                  //配置锁存
}
/**
  * 描述   串口发闪送换行符
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void Usart_line_feed(USART_SFRmap *USARTx)
{
	USART_SendData(USARTx,0x0D);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	USART_SendData(USARTx,0x0A);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}



