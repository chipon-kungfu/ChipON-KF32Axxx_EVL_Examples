/****************************************************************************************
 * 文件名: main.c
 * 项目名: I2C_DMA
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了I2C与DMA配合使用参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
#include "I2C_DMA.h"
#include "Usart.h"

#define	MODE_Master	   1					 //主机模式
#define	MODE_Slave	   0                    //从机模式
#define	IIC_MODE	   MODE_Master	        //0——从机模式，1——主机模式






#if	IIC_MODE==MODE_Master  //主机模式

#if IIC_10BIT_Address
#define BUFFER_SIZE               66   //读写数据个数
#define I2C_SLAVE_ADDRESS10B     0XF6A0   //设置发送匹配的I2C 10位地址
uint8_t i2c_buffer_write10B[BUFFER_SIZE] = {
		                            0XF6,0XA0,0X5A,0X22,0X33,0X44,0X66,0X77,0X88,0X99,0XAA,0XBB,
											  0X01,0X02,0X03,0X04,0X05,0X06,0X07,0X08,0X09,0X10,
											  0X11,0X12,0X13,0X14,0X15,0X16,0X17,0X18,0X19,0X20,
											  0X21,0X22,0X23,0X24,0X25,0X26,0X27,0X28,0X29,0X30,
											  0X31,0X32,0X33,0X34,0X35,0X36,0X37,0X38,0X39,0X40,
											  0X41,0X42,0X43,0X44,0X45,0X46,0X47,0X48,0X49,0X50,
											  0X51,0X52,0X53,0XA5};
#else
#define I2C_SLAVE_ADDRESS         0XA0   //设置发送的I2C 7位地址
#define BUFFER_SIZE               65   //读写数据个数



//主机模式DMA要写入的I2C地址及数据
uint8_t i2c_buffer_write[BUFFER_SIZE] = {
									     0XA0,0X5A,0X22,0X33,0X44,0X66,0X77,0X88,0X99,0XAA,0XBB,
											  0X01,0X02,0X03,0X04,0X05,0X06,0X07,0X08,0X09,0X10,
											  0X11,0X12,0X13,0X14,0X15,0X16,0X17,0X18,0X19,0X20,
											  0X21,0X22,0X23,0X24,0X25,0X26,0X27,0X28,0X29,0X30,
											  0X31,0X32,0X33,0X34,0X35,0X36,0X37,0X38,0X39,0X40,
											  0X41,0X42,0X43,0X44,0X45,0X46,0X47,0X48,0X49,0X50,
											  0X51,0X52,0X53,0XA5};

#endif

#else  //从机模式

#define BUFFER_SIZE               65   //读写数据个数

#if IIC_10BIT_Address
#define I2C_SLAVE_ADDRESS10B     0X3A0   //设置接收匹配的I2C 10位地址
#else
#define I2C_SLAVE_ADDRESS         0XA0   //设置接收匹配的I2C 7位地址

#endif

volatile uint8_t i2c_buffer_read[BUFFER_SIZE];//读取到I2C数据后存储数组
//用于和读到I2C 7位地址及数据的数组对比
uint8_t i2c_buffer_read_test[BUFFER_SIZE] = {
		                                 0XA0,0X5A,0X22,0X33,0X44,0X66,0X77,0X88,0X99,0XAA,0XBB,
		                                      0X01,0X02,0X03,0X04,0X05,0X06,0X07,0X08,0X09,0X10,
		                                      0X11,0X12,0X13,0X14,0X15,0X16,0X17,0X18,0X19,0X20,
		                                      0X21,0X22,0X23,0X24,0X25,0X26,0X27,0X28,0X29,0X30,
		                                      0X31,0X32,0X33,0X34,0X35,0X36,0X37,0X38,0X39,0X40,
		                                      0X41,0X42,0X43,0X44,0X45,0X46,0X47,0X48,0X49,0X50,
		                                      0X51,0X52,0X53,0XA5};


/**
  * 描述  对比I2C的数组是否与读到数组数据匹配
  * 输入
  * 返回   1：数据匹配
  *      0：数据不匹配
  */
uint8_t I2C_Data_test(void)
{
	uint16_t i;

    for(i = 0;i < BUFFER_SIZE;i++)
    {
        if(i2c_buffer_read[i] != i2c_buffer_read_test[i])
        {
           return I2C_FAIL;
        }
    }
    return I2C_OK;


}

#endif

//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=2000;
		while(j--);
	}

}

/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	 //先设置为高电平

}




/**
  * 描述  I2C DMA 初始化配置。
  * 输入 : 无
  *
  * 返回  无。
  */
void I2C_DMA_Init(void)
{
	DMA_Reset(DMA0_SFR);
	DMA_InitTypeDef I2C_DMAStruct;
	I2C_DMAStruct.m_Number = BUFFER_SIZE;/* 配置 传输数据个数: BUFFER_SIZE */
#if	IIC_MODE==MODE_Slave  //从机模式
	I2C_DMAStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;/* 配置 DMA传输方向： 外设到内存*/
	I2C_DMAStruct.m_Channel = DMA_CHANNEL_7;/* 配置 DMA通道选择:通道7 */
#else
	I2C_DMAStruct.m_Direction = DMA_MEMORY_TO_PERIPHERAL;/* 配置 DMA传输方向： 内存到外设*/
	I2C_DMAStruct.m_Channel = DMA_CHANNEL_6;/* 配置 DMA通道选择:通道6 */
#endif
	I2C_DMAStruct.m_Priority = DMA_CHANNEL_LOWER;/* 配置 DMA通道优先级：低优先级 */
	I2C_DMAStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;/* 配置 外设数据位宽：8位宽 */
	I2C_DMAStruct.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;/* 配置 存储器数据位宽:8位宽 */
	I2C_DMAStruct.m_PeripheralInc = FALSE;/* 配置 外设地址增量模式使能: 不使能 */
	I2C_DMAStruct.m_MemoryInc = TRUE;/* 配置 存储器地址增量模式使能: 使能 */

	I2C_DMAStruct.m_BlockMode = DMA_TRANSFER_BYTE; /* 配置 数据字节传输模式： */
	I2C_DMAStruct.m_LoopMode = TRUE;/* 配置 循环模式使能: 使能 */
	I2C_DMAStruct.m_PeriphAddr = 0x40000D88;/* 配置 I2C1 buff地址：等待接收或发送的数据的起始地址 */

#if	IIC_MODE==MODE_Slave  //从机模式
	I2C_DMAStruct.m_MemoryAddr = (uint32_t)&i2c_buffer_read;/* 配置 内存起始地址：接收数据的内存空间的起始地址 */
#else

#if IIC_10BIT_Address
	I2C_DMAStruct.m_MemoryAddr = (uint32_t)&i2c_buffer_write10B;/* 配置 内存起始地址：发送数据的内存空间的起始地址 */
#else
	I2C_DMAStruct.m_MemoryAddr = (uint32_t)&i2c_buffer_write;/* 配置 内存起始地址：发送数据的内存空间的起始地址 */
#endif

#endif

	DMA_Configuration(DMA0_SFR, &I2C_DMAStruct);

}



//主函数
void main()
{

	/*用户可参考该例程在KF32A151_demo板上实现使用I2C实现DMA主从收发功能。
	* 例程中使用I2C1 PB1 PB2 用于主动机通信。在 宏定义 IIC_MODE 1或0选择主机模式或从机模式，
	* 在I2C_DMA.h选择宏定义IIC_10BIT_Address是否使用10地址模式或7位地址模式。
	* 功能简述:	1.测试I2C Master DMA功能，每60ms向从机发送64个字节数据，DMA 发送完成后D4灯闪烁，否则D4灯常亮。
	* 			2.测试I2C Slave DMA 功能，将主机发送过来64个数据通过DMA读出来，并作数据对比如果匹配PB13翻转，D4灯闪烁，否则D4灯常亮。
	* 硬件说明： 使用两块KF32A151demo对接，PB1与PB1对接，PB2与PB2对接；
	*           PB13对应控制D4灯
	*           如果需要用串口接USART0_RX PA0 	USART0_TX0	PA1  并在Usart.h打开 串口宏定义Usart_Print  1
	*注： 1、DMA发送第一或第一和第二个数据为I2C为从机地址
	*    2、10位地址的配置要注意从机地址的高位是第二位和第三位有效。
	*
    */

	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化

	//初始化PB13为输出高电平
	GPIO_Reset(GPIOB_SFR);
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);
	I2C_GPIO_init(); //I2C GPIO重映射SCL:PB1   SDA:PB2



#if	IIC_MODE==MODE_Master  //主机模式
	I2C_init_Master(I2C1_SFR);  //I2C1初始化

#else
	I2C_DMA_Init();  //I2C1 DMA1初始化
#if Usart_Print

	GPIO_USART();//配置USART0引脚重映射，	//USART0_RX		PA0  //USART0_TX0	PA1
	USART_Async_config(USART0_SFR);//全双工异步8bit 9600波特率
#endif


#if IIC_10BIT_Address
	I2C_init_Slave(I2C1_SFR,I2C_SLAVE_ADDRESS10B);//配置I2C1 从机10位地址0XF3A0
#else
	I2C_init_Slave(I2C1_SFR,I2C_SLAVE_ADDRESS);//配置I2C1从机7位地址0XA0
#endif

	I2C_Receive_DMA_INT_Enable(I2C1_SFR,TRUE);	//使能I2C1接收中断
	DMA_Channel_Enable(DMA0_SFR,DMA_CHANNEL_7,TRUE); //使能DMA0 I2C1通道
	///********************I2C中断配置*****************/
		INT_Interrupt_Enable(INT_I2C1, TRUE);
		I2C_ISIE_INT_Enable(I2C1_SFR, TRUE);       //使能I2C中断
		INT_All_Enable(TRUE);

#endif


	while(1)
    {

#if	IIC_MODE==MODE_Master  //主机模式
		delay_ms(1000);
		I2C_DMA_Init();  //I2C1 DMA1初始化
		DMA_I2C_Enable(TRUE);
		GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);  // DMA 传输完数据后PB13翻转
	    I2C_Transmit_DMA_INT_Enable(I2C1_SFR,FALSE);	//关I2C0发送中断
		DMA_Channel_Enable(DMA0_SFR,DMA_CHANNEL_6,FALSE); //关DMA0 通道6
		delay_ms(200);

#else  //从机模式

		while (0 == DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_7))//等待DMA读写传输完数据
		{
            ;
		}

		I2C_Receive_DMA_INT_Enable(I2C1_SFR,FALSE);	//使能I2C1接收中断
		DMA_Channel_Enable(DMA1_SFR,DMA_CHANNEL_7,FALSE); //使能DMA1 I2C1通道
		delay_ms(200);

#if Usart_Print

			delay_ms(200);
			delay_ms(200);
			delay_ms(200);
			//串口打印接收到的数组数据
			USART_Send(USART0_SFR,i2c_buffer_read,sizeof(i2c_buffer_read));
#endif
		I2C_DMA_Init();
		I2C_Receive_DMA_INT_Enable(I2C1_SFR,TRUE);	//使能I2C1接收中断
		DMA_Channel_Enable(DMA0_SFR,DMA_CHANNEL_7,TRUE); //使能DMA0 I2C1通道

		if(I2C_OK == I2C_Data_test())//判断读到I2C数组匹配后PB13翻转
		{
			GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_13);  //PB13翻转
			delay_ms(200);
		}
#endif
    }

}



/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

