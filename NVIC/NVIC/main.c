/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: NVIC
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了实现中断优先级功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}

/**
  * 描述  GPIOx 输入初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Input_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输入模式 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;                   //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_IN;                      //初始化 GPIO方向为输入
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;                     //初始化 GPIO是否上拉 不上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;                   //初始化 GPIO是否下拉 不下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);
}





/**
  * 描述  通用定时器GPTIMx 初始化配置。
  * 输入  GPTIMx:取值为T0_SFR/T1_SFR/T2_SFR/T3_SFR/T4_SFR/ T18_SFR/T19_SFR/T20_SFR/T21_SFR/T22_SFR/T23_SFR。
  *  Peripheral：取值为INT_T0/INT_T1/INT_T2/INT_T3/INT_T4/INT_T18/INT_T19/INT_T20/INT_T21/INT_T22/INT_T23
  *
  * 返回  无。
  */
void GENERAL_TIMER_Tx_Config(GPTIM_SFRmap* GPTIMx,InterruptIndex Peripheral)
{
    //定时器时钟源为主频48M，经过定时器24分频，0.5us执行计数一次，计数40000次2ms进一次中断
	//抢占优先级6,子优先级0
	TIM_Reset(GPTIMx);											//定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(GPTIMx,TRUE);				//立即更新控制
	GPTIM_Updata_Enable(GPTIMx,TRUE);							//配置更新使能
	GPTIM_Work_Mode_Config(GPTIMx,GPTIM_TIMER_MODE);			//定时模式选择
	GPTIM_Set_Counter(GPTIMx,0);								//定时器计数值
	GPTIM_Set_Period(GPTIMx,40000);								//定时器周期值
	GPTIM_Set_Prescaler(GPTIMx,23);				    			//定时器预分频值
	GPTIM_Counter_Mode_Config(GPTIMx,GPTIM_COUNT_UP_DOWN_OUF);	//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(GPTIMx,GPTIM_SCLK);						//选用SCLK时钟
	INT_Interrupt_Priority_Config(Peripheral,6,0);				//抢占优先级6,子优先级0
	GPTIM_Overflow_INT_Enable(GPTIMx,TRUE);						//计数溢出中断使能
	INT_Interrupt_Enable(Peripheral,TRUE);						//外设中断使能
	INT_Clear_Interrupt_Flag(Peripheral);						//清中断标志
	GPTIM_Cmd(GPTIMx,TRUE);										//定时器启动控制使能

}



/**
  * 描述  SysTick定时器初始化及重加载值配置。
  * 输入  Reload：系统节拍定时器重加载值，取值为24位数据
  * 返回  无。
  */
void SysTick_Configuration(uint32_t Reload)
{

	//节拍定时器优先级为5，子优先级0
	SYSTICK_Cmd (FALSE);                                //先关节拍定时器
	SYSTICK_Reload_Config(Reload);                      //系统节拍定时器重加载值，取值为24位数据
	SYSTICK_Counter_Updata();                           //向ST_CV寄存器写任意值，以清零当前值寄存器
	SYSTICK_Clock_Config(SYSTICK_SYS_CLOCK_DIV_1);      //系统节拍定时器时钟源选择，SCLK作为时钟源
	SYSTICK_Systick_INT_Enable(TRUE);                   //节拍定时器中断使能
	SYSTICK_Cmd(TRUE);                                  //节拍定时器使能

	INT_Interrupt_Priority_Config(INT_SysTick,5,0);     //设中断SYSTICK抢占优先级5,子优先级0
    INT_Interrupt_Enable(INT_SysTick,TRUE);				//使能中断SYSTICK

}


/**
  * 描述  外部中断INT7配置，抢占优先级4,子优先级0。
  * 输入  无
  * 返回  无。
  */
void EXIT_INT7_Config(void)
{
	EINT_InitTypeDef EXIT_InitStructure;
	INT_External_Struct_Init(&EXIT_InitStructure);
	EXIT_InitStructure.m_Line = INT_EXTERNAL_INTERRUPT_7;  //设置外部中断编号
	EXIT_InitStructure.m_Mask = TRUE;                      //设置外部中断使能控制
	EXIT_InitStructure.m_Rise = TRUE;                     //设置外部中断上升沿中断使能
	EXIT_InitStructure.m_Fall = FALSE;                       //设置外部中断下降沿中断使能
	EXIT_InitStructure.m_Source = INT_EXTERNAL_SOURCE_PD;  //设置外部中断的中断源选择
	INT_External_Configuration(&EXIT_InitStructure);
	INT_External_Source_Enable(INT_EXTERNAL_INTERRUPT_7,INT_EXTERNAL_SOURCE_PD);//PD7是中断EINT7中断源
	INT_Interrupt_Priority_Config(INT_EINT9TO5,4,0);		   //抢占优先级4,子优先级0
	INT_Interrupt_Enable(INT_EINT9TO5,TRUE);				   //使能中断EINT7

}


//主函数
void main()
{

	/*用户可参考该例程在KF32A151_demo板上实现使用中断优先级功能应用。
	 * 例程中通用定时器T0中断为最低中断优先级，节拍定时器中断为次优先级，外部中断EINT7中断优先级最高
	 * 程序功能中在节拍定时器和通用定时器工作进入中断时把PB13置低，一旦有外部中断EINT7发生，马上进入外部EINT7中断应用。
	 * PB13置高，即：一旦按键KEYUSE按下， D4灯就常亮，松开按键后则恢复 D4灯熄灭。
	 *
	 *主要设置中断优级中抢占优先级与子优先级的大小，值越小优先级越高。
	 *
	 */
	//系统时钟120M,外设高频时钟16M
	SystemInit();//系统时钟初始化
	GPIOInit_Input_Config(GPIOD_SFR,GPIO_PIN_MASK_7);  //把PD7设为输入模式
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);  //把PB13设为输出，并设为高电平
	EXIT_INT7_Config();                                //外部中断EINT7，配置 抢占优先级4,子优先级0
	SysTick_Configuration(6000000);                     //节拍定时器50mS进一次中断，配置 抢占优先级5,子优先级0
	GENERAL_TIMER_Tx_Config(T0_SFR,INT_T0);             //定时器T0 2ms进一次中断，配置抢占优先级6,子优先级0
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);		//中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);								//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

    while(1)
    {
    }

}




void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

