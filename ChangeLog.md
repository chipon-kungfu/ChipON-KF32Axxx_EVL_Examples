Change Log
修改时间：2024-08-28							           
更新版本：V3.0.1								           
修改者：  ChipON_AE/FAE_Group						           
修改记录：								           
	1、删除PM_STOP1_LPTIMER_WAKUP、Power_manager例程。
	2、删除Power_manager_PMCError例程中，STOP1、Shutdown模式相关内容。
	   修改Standby和Stop0模式休眠流程，添加操作PM休眠相关特性位和清除标志位内容.
	3、更新ADC_ScanMode、ADC_SingleMode_INT、CAN_LPCAN0_CAN1、IWDT示例程序中使用备份域读写使能函数 `BKP_Write_And_Read_Enable` 的逻辑，对备份域的操作需要成对使用。

修改时间：2021-08-20							           
更新版本： V3.0.0								           
修改者：   ChipON_AE/FAE_Group						           
修改记录：								           
	1、修改例程OSC中T0计数周期以及外部晶振配置中增加外部低频使能
	

修改时间：2021-08-09							           
更新版本： V3.0.0								           
修改者：   ChipON_AE/FAE_Group						           
修改记录：								           
	1、修改例程ADC时钟分频为32分频				           	           
	2、修改CAN例程接收中断里硬件指针和软件指针会出现不匹配问题以及Can优化中断
	   仲裁重发处理机制，优化接收邮箱处理机制   
	3、新增I2C从机例程，修改了slave使用，详情可参考例程代码中的slave使用文档。	           	
	4、新增LPCAN例程。						           
	5、修改LIN取消自动波特率检测与自动唤醒功能，增加增强型校验方式。	
	6、同步gitee修改OSC例程。
	7、修改了例程ADC_continue_mode-KF32Axx里面adc采样常规通道对应的DMA通道为DMA Chanel5。
	
	
 2021-05-24
●2 更新 EXTHLF 例程 用延时替代原来的等待标志位

 2021-05-08
●1 基于标准外设库V2.62例程代码发布