/**
  ******************************************************************************
  * 文件名  ECCP5_user.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了关于ECCP的配置方法
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ECCP5_user.h"
/**
  * 描述   ECCP5的输出管脚配置
  * 输入   无
  * 返回   无
  *
  * 管脚分配
  * 	ECCP5CH1H —— PB11
  */
void cfg_gpio_for_ECCP5()
{
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_11,GPIO_MODE_RMP);
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_11,GPIO_RMP_AF2_T5);//CCP5CH1H
}

/**
  * 描述   配置ECCP5
  * 输入   无
  * 返回   无
  */
void cfg_ECCP5()
{
	ECCP_Compare_Mode_Config(ECCP5_SFR,ECCP_CHANNEL_1,ECCP_CMP_ACTIVE_LEVEL); //比较条件达成时输出高电平
	ECCP_Set_Compare_Result(ECCP5_SFR,ECCP_CHANNEL_1,0x1FFF);
}

