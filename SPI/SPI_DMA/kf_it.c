/****************************************************************************************
 *
 * 文件名 kf_it.c
 * 作  者  ChipON_AE/FAE_Group
 * 日  期  2019-11-16
 * 描  述  该文件提供了串口异步收发应用例程参考
 *       +DMA中断SPI收发功能
 *
 ****************************************************************************************/
#include"system_init.h"
#include "Usart.h"
#include "SPI.h"

//*****************************************************************************************
//                                 SPI1中断函数
//*****************************************************************************************	
void __attribute__((interrupt))_SPI1_exception (void)
{

}

//*****************************************************************************************
//                                 DMA0中断函数
//*****************************************************************************************
void __attribute__((interrupt))_DMA0_exception (void)
{
	if(DMA_Get_Finish_Transfer_INT_Flag (DMA_COMx,DMA_Rx))//SPI_Rx接收
	{
		Uart_flag=1;
		DMA_Clear_INT_Flag (DMA_COMx,DMA_Rx,DMA_INT_FINISH_TRANSFER);
	}

	if(DMA_Get_Finish_Transfer_INT_Flag (DMA_COMx,DMA_Tx))//SPI_Tx发送完成
	{
		while(SPI_Get_BUSY_Flag(SPI_COM)==SET);
		DMA_Clear_INT_Flag (DMA_COMx,DMA_Tx,DMA_INT_FINISH_TRANSFER);
#if SPI_MASTER
#else
		DMA_Channel_Enable (DMA_COMx, DMA_Tx, TRUE);//从模式启动发送
#endif
	}


}
