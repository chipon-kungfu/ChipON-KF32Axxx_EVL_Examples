/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */
#include "system_init.h"

void Init_ccp1_for_trog()
{
	CCP_CompareInitTypeDef buf;
	buf.m_Channel =CCP_CHANNEL_1;
	buf.m_CompareMode =CCP_CMP_SPECIAL_EVENT;
//	buf.m_CompareValue = 0x500;
	buf.m_CompareValue = 0x1B0;
	CCP_Compare_Configuration(CCP2_SFR, &buf);
}

