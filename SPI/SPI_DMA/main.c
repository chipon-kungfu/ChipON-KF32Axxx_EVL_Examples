/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了SPI主从收发应用例程参考
  *
 ****************************************************************************************/
#include "system_init.h"
#include "Usart.h"
#include "SPI.h"
#define LED_ON()        GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_SET)  //设置为高电平
#define LED_OFF()       GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_RESET)//设置为低电平
#define LED_ON_OFF()    GPIO_Toggle_Output_Data_Config (GPIOB_SFR, GPIO_PIN_MASK_13)   //LED电平取反

void GPIO_SPI1();//SPI1 IO口重映射
void GPIO_USART1();//USART1引脚重映射
void Usart_line_feed(USART_SFRmap *USARTx);//发送换行符
/**
  * 描述    程序延时
  * 输入   a：延时参数
  *      b：延时参数
  * 返回   无
  */
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程在两块KF32A151_demo板上实现主从中断方式的SPI通信,通过SPI.h文件的SPI_MASTER参数选择
	* 主从代码。主机通过SPI1发送Tx_master[]并闪烁LED，从机通过SPI1发送的Tx_slave[]并闪烁LED，主机与从机
	* 可通过串口1打印接收的数据。SPI协议而为：Command+Dummy+Tx[]
	* MASTER的SDO接SLAVE的SDI，MASTER的SDI接SLAVE的SDO*/

	//系统时钟72M,外设高频时钟16M（使用SPI，SPI参考的时钟输入请不要超过80M）
	SystemInit();
	//配置USART1引脚重映射，PA15-RX，PE0-TX0
	GPIO_USART1();
	//全双工异步8bit 9600波特率
	USART_Async_config(USART1_SFR);

	//初始化变量
	Command_flag=0;
	Dummy_flag=0;
	Tx_length=0;
	Rx_length=0;
	/*初始化复位GPIOB外设用于LED,使能GPIOB外设时钟*/
	GPIO_Reset(GPIOB_SFR);
    GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_MODE_OUT);

	/*SPI1_IO配置*/
	GPIO_SPI1(); //PB0=SS,PB1=SCK,PB2=SDI,PB3=SDO
	/*SPI DMA配置*/
	SPI_DMA_Configuration(SPI_COM);

#if SPI_MASTER
    //延时点灯master 等待  slave配置完成
	LED_ON();
	Delay(1000000);
	LED_OFF();
#endif
	INT_Interrupt_Enable(INT_DMA0,TRUE);//DMA0中断使能
	INT_All_Enable(TRUE);//总中断使能

	SPI_I2S_SendData8(SPI_COM, 0x00);  //第一次配置DMA，需填充数据启动DMA发送

    while(1)
	{
    	if(Uart_flag)//主机||从机接收完毕，发送接收数据
	   {
		   Uart_flag=0;

		   LED_ON_OFF();//LED电平取反

		   /*串口打印接收数据 */
		   USART_Send(USART1_SFR,Read_data,BufferSize);
		   Usart_line_feed(USART1_SFR);

#if SPI_MASTER
		   while(SPI_Get_BUSY_Flag(SPI_COM)==SET);     // 主模式SPI发送完后启动SPI
		   DMA_Channel_Enable (DMA_COMx, DMA_Tx, TRUE);
#endif
	   }
	}
}

/**
  * 描述    USART1引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART1()
{
	 /* 端口重映射AF5 */
	//USART1_RX		PA15
	//USART1_TX0	PE0
	GPIO_Write_Mode_Bits(GPIOA_SFR ,GPIO_PIN_MASK_15, GPIO_MODE_RMP);           //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOE_SFR ,GPIO_PIN_MASK_0, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_15, GPIO_RMP_AF5_USART1);	   //重映射为USART1
	GPIO_Pin_RMP_Config (GPIOE_SFR,GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);      //重映射为USART1
	GPIO_Pin_Lock_Config (GPIOA_SFR ,GPIO_PIN_MASK_15, TRUE);                   //配置锁存
	GPIO_Pin_Lock_Config (GPIOE_SFR ,GPIO_PIN_MASK_0, TRUE);                  //配置锁存
}
/**
  * 描述   GPIO_SPI1()引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_SPI1()
{
	/*SPI1_IO配置*/
	//PB0=SS,PB1=SCK,PB2=SDI,PB3=SDO
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_0  \
			                      |GPIO_PIN_MASK_1  \
			                      |GPIO_PIN_MASK_2  \
			                      |GPIO_PIN_MASK_3,GPIO_MODE_RMP);//重映射IO口功能模式


	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_0,GPIO_RMP_AF7_SPI1);//重映射为SPI1
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_1,GPIO_RMP_AF7_SPI1);//重映射为SPI1
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_2,GPIO_RMP_AF7_SPI1);//重映射为SPI1
    GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_3,GPIO_RMP_AF7_SPI1);//重映射为SPI1

}
/**
  * 描述   串口发闪送换行符
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void Usart_line_feed(USART_SFRmap *USARTx)
{
	USART_SendData(USARTx,0x0D);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
	USART_SendData(USARTx,0x0A);
	while(!USART_Get_Transmitter_Empty_Flag(USARTx));
}
/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	while(1)
	  {

	  }
}
