/****************************************************************************************
 *
 * 文件名: kf_it.c
 * 项目名: KF32F350_TimeBase
 * 版 本: v1.0
 * 日 期: 2019年10月17日 14时41分04秒
 * 作 者: 80984
 * 
 ****************************************************************************************/
#include "system_init.h"
#include "I2C_Master_Slave.h"


extern void I2C_receive_input(uint8_t Rev_Temp);
volatile uint32_t i2c_data_temp;   //接收到I2C数据的变量


//*****************************************************************************************
//                               I2C1中断函数
//*****************************************************************************************

void __attribute__((interrupt))_I2C1_exception(void)
{

	volatile uint32_t I2C1_DATA=0;
	volatile uint8_t I2C1_REDATA=0;
	volatile uint32_t I2C1_ADD=0;
	i2c_data_temp =0;

	I2C_Clear_INTERRUPT_Flag(I2C1_SFR);//清I2C1中断标志位
	I2C_Ack_DATA_Config(I2C1_SFR,I2C_ACKDATA_ACK);//回复ACK


#if	IIC_MODE==MODE_Slave  //从机模式

#if IIC_10BIT_Address//10BIT从机
	if(I2C_Get_Write_Read_Flag(I2C1_SFR))
	{
		I2C_SendData(I2C1_SFR,I2C1_DATA);
	}
	else
	{
		if(I2C_Get_HighAddress_Flag(I2C1_SFR))
		{//高位地址，跳过
			;
		}
		else
		{//不是高位地址，执行其它动作
			i2c_data_temp = I2C_ReceiveData(I2C1_SFR);
			if(I2C_Get_Data_Flag(I2C1_SFR))
			{
				I2C_receive_input(i2c_data_temp);//读取数据
				I2C1_DATA=i2c_data_temp;

			}
			else
			{
				I2C1_ADD=i2c_data_temp;
			}
		}
	}
#else//7BIT从机
	if(I2C_Get_Write_Read_Flag(I2C1_SFR))
	{//主机读状态位，从动发送
		I2C_SendData(I2C1_SFR,I2C1_DATA);
	}
	else
	{//主机写状态位，从动接收
		i2c_data_temp = I2C_ReceiveData(I2C1_SFR);


		if(I2C_Get_Data_Flag(I2C1_SFR))
		{
			I2C1_REDATA =i2c_data_temp& 0xff;
			I2C_receive_input(I2C1_REDATA);
		}
		else
		{
			I2C1_ADD=i2c_data_temp;

		}

	}
#endif

#endif

}



