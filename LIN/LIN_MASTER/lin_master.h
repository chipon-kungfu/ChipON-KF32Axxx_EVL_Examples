/**
  ******************************************************************************
  *
  * 文件名  lin_master.h
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了LIN例程相关配置函数
  *
  ******************************************************************************/

#ifndef _LIN_MASTER_H_
#define _LIN_MASTER_H_

extern volatile uint8_t Rev_Temp;		 //接收数据（清接收中断标志）
extern uint8_t l_Tx_data[10];           //发送数据
extern uint8_t xxx;             		//计数

extern void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//串口发送函数
extern uint8_t Check_Sum(uint8_t* Databuf,uint32_t point,uint32_t length);  //标准校验和
extern void LIN_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);//LIN主机发送
extern void USART_LIN_config(USART_SFRmap *USARTx);//串口配置，使能LIN唤醒
extern void USART_LIN_INT_config(USART_SFRmap *USARTx,InterruptIndex Peripheral);//串口接收中断使
extern void BASIC_TIMER_Config(BTIM_SFRmap* BTIMx, InterruptIndex Peripheral);//定时器配置

#endif /* _LIN_MASTER_H_ */
