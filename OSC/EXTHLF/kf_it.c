/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception (void)
{
//一旦外部晶振有故障便中断NMI中断，切换成内部晶振工作
   while(OSC_Get_Clock_Failure_INT_Flag()!=RESET)
	{
		OSC_INTHF_Software_Enable(TRUE);
		OSC_PLL_Input_Source_Config(PLL_INPUT_INTHF);

		OSC_PLL_Start_Delay_Config(PLL_START_DELAY_4096);
		OSC_PLL_Software_Enable(ENABLE);
		OSC_PLL_RST();
		/* 主时钟配置 */
		OSC_SCK_Division_Config(SCLK_DIVISION_1);
		OSC_SCK_Source_Config(SCLK_SOURCE_PLL);     //选择PLL作为系统时钟

		/* 外设高速时钟 */
		OSC_HFCK_Division_Config(HFCK_DIVISION_1);
		OSC_HFCK_Enable(ENABLE);                    //HFCLK时钟信号允许

		OSC_LFCK_Division_Config(LFCK_DIVISION_1);		//工作分频选择
		OSC_LFCK_Source_Config (LFCK_INPUT_INTLF);		//选择INTLF作为LFCLK时钟
		OSC_LFCK_Enable (TRUE);							//LFCLK时钟信号允许
		OSC_INTLF_Software_Enable(TRUE);

		OSC_INT  = 0x00;//清CKFIF标志
		INT_EIF0 = 0X00;//清系统中断标志位

	}
}

//*****************************************************************************************
//                              T0中断函数
//*****************************************************************************************

void __attribute__((interrupt))_T0_exception (void)
{

	GPTIM_Clear_Updata_INT_Flag(T0_SFR);										//清更新时间标志位
	GPTIM_Clear_Overflow_INT_Flag (T0_SFR);										//清T0溢出中断标志位
//	GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_13);                 //PB13翻转

}
