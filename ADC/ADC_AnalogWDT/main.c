/****************************************************************************************
 *
 * File Name: main 
 * Project Name: ADC_AnalogWDT
 * Version: v1.0
 * Date: 2022-01-05- 15:10:21
 * Author: chipon
 * 
 ****************************************************************************************/
//#include<KF32A151MQV.h>
#include "stdio.h"
#include "system_init.h"
#include "USART_user.h"
#include "ADC_user.h"
//asm(".include		\"KF32A151MQV.inc\"	");	 
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
//Main Function
int main()
{

	SystemInit();
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_13,GPIO_MODE_OUT);  //STATUS灯配置为输出模式

	//配置USART2引脚重映射
	GPIO_USART();
	//全双工异步8bit 115200波特率
	USART_Async_config(USART2_SFR);

	Init_adc0(); //配置ADC0位单通道、单次采样、关闭外部触发
	Init_ADC_Analog_Wd(ADC_WDCH_22);

	Init_adc0_INT();

	INT_All_Enable (TRUE);//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

	ADC_Software_HPStart_Conv(ADC0_SFR);      //软件启动高优先级通道转换
	fprintf(USART2_STREAM,"ADC Anolog WDT Function test \r\n");
	while(1)
	{
	

	}		
}
