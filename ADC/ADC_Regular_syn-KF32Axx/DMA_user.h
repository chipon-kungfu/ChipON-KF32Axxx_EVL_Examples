/*
 * dma.h
 *
 *  Created on: 2019-11-6
 *      Author: charles_cai
 */

#ifndef DMA_H_
#define DMA_H_


volatile extern uint32_t  buf_1[96];
volatile extern uint32_t  buf_2[96];
void Init_dma0();
void Init_dma0_it();

void Init_dma1();
void Init_dma1_it();


#endif /* DMA_H_ */
