/**
  ******************************************************************************
  *
  * 文件名  SPI.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了SPI主从模式相关功能，包括
  *          + SPI配置
  *
  ******************************************************************************/
#include "system_init.h"
#include "SPI.h"
volatile uint8_t  Command_flag;//命令标志位
volatile uint8_t  Dummy_flag;  //dummy标志位
volatile uint8_t  Uart_flag;   //串口标志位
volatile uint32_t Tx_length;   //发送长度变量
volatile uint32_t Rx_length;   //接收长度变量
uint8_t Tx_master[BufferSize] = "KF32A151-KungFu32 SPI Example master";  //主机Tx
uint8_t Tx_slave[BufferSize]  = "KF32A151-KungFu32 SPI Example slaves";  //从机Tx
uint8_t Read_data[BufferSize]; //读数据缓存

/**
  * 描述    SPI配置
  * 输入   SPIx：指向SPI内存结构的指针，取值为SPI0_SFR~SPI3_SFR
  * 返回   无
  */
void SPI_Init_Configuration(SPI_SFRmap* SPIx)
{
	SPI_InitTypeDef newStruct_SPI;
	/*SPI配置*/
#if SPI_MASTER
	newStruct_SPI.m_Mode = SPI_MODE_MASTER_CLKDIV4;                   //主模式主时钟4分频 18M
#else
	newStruct_SPI.m_Mode = SPI_MODE_SLAVE;                            //从模式
#endif
 	newStruct_SPI.m_Clock = SPI_CLK_SCLK;							  //SPI主频时钟
	newStruct_SPI.m_FirstBit = SPI_FIRSTBIT_MSB;					  //MSB
	newStruct_SPI.m_CKP = SPI_CKP_LOW;                                //SCK空闲为高
	newStruct_SPI.m_CKE = SPI_CKE_1EDGE;                              //第一个时钟开始发送数据
	newStruct_SPI.m_DataSize = SPI_DATASIZE_8BITS;                    //8bit
	newStruct_SPI.m_BaudRate = 0x59;                                  //Fck_spi=Fck/2(m_BaudRate+1)=10us
    SPI_Reset(SPIx);											      //复位模块
    SPI_Configuration(SPIx, &newStruct_SPI);					      //写入结构体配置
	SPI_Cmd(SPIx,TRUE);											      //使能
}

/**
  * 描述    SPI_DMA配置
  * 输入   SPIx：指向SPI内存结构的指针，取值为SPI0_SFR~SPI3_SFR
  * 返回   无
  */
void SPI_DMA_Configuration(SPI_SFRmap* SPIx)
{
	SPI_InitTypeDef newStruct_SPI;
	/*SPI配置*/
#if SPI_MASTER
	newStruct_SPI.m_Mode = SPI_MODE_MASTER_CLKDIV4;                   //主模式主时钟4分频 18M
#else
	newStruct_SPI.m_Mode = SPI_MODE_SLAVE;                            //从模式
#endif
 	newStruct_SPI.m_Clock = SPI_CLK_SCLK;							  //SPI主频时钟
	newStruct_SPI.m_FirstBit = SPI_FIRSTBIT_MSB;					  //MSB
	newStruct_SPI.m_CKP = SPI_CKP_LOW;                                //SCK空闲为高
	newStruct_SPI.m_CKE = SPI_CKE_1EDGE;                              //第一个时钟开始发送数据
	newStruct_SPI.m_DataSize = SPI_DATASIZE_8BITS;                    //8bit
	newStruct_SPI.m_BaudRate = 0x59;                                  //Fck_spi=Fck/2(m_BaudRate+1)=10us
    SPI_Reset(SPIx);											      //复位模块
    SPI_Configuration(SPIx, &newStruct_SPI);					      //写入结构体配置

    /*DMA配置*/
    /*接收通道*/
	DMA_InitTypeDef dmaNewStruct;
	DMA_Reset(DMA_COMx);
    dmaNewStruct.m_Number = BufferSize;                               //传输数据个数: BufferSize
    dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;              //配置 DMA传输方向：外设到内存
    dmaNewStruct.m_Priority = DMA_CHANNEL_TOP;					      //配置 DMA通道优先级：高优先级
    dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;        //配置 外设数据位宽： 8位宽
    dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;            //配置存储器数据位宽:8位宽
    dmaNewStruct.m_PeripheralInc = FALSE; 							  //配置 外设地址增量模式使能: 禁止
    dmaNewStruct.m_MemoryInc = TRUE;								  //配置 存储器地址增量模式使能: 允许
    dmaNewStruct.m_Channel = DMA_Rx;						          //配置 DMA通道选择:通道  RX
    dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;	                  //配置 数据块传输模式：字节传输
    dmaNewStruct.m_LoopMode = TRUE;//FALSE;			                  //配置 循环模式使能: 允许
    dmaNewStruct.m_PeriphAddr = (uint32_t)&(SPIx->BUFR);      	      //配置 外设起始地址
    dmaNewStruct.m_MemoryAddr = (uint32_t)&Read_data;                 //配置 内存起始地址
    DMA_Configuration (DMA_COMx,&dmaNewStruct);                       //写入配置

    SPI_Receive_DMA_INT_Enable (SPIx,TRUE);                           //接收DMA
	DMA_Channel_Enable (DMA_COMx, DMA_Rx, TRUE);					  //DMA通道使能
	DMA_Finish_Transfer_INT_Enable(DMA_COMx,DMA_Rx,TRUE);		      //接收完整数据DMA中断

	/*发送通道*/
	dmaNewStruct.m_Number = BufferSize; 							//传输数据个数: BufferSize
    dmaNewStruct.m_Direction = DMA_MEMORY_TO_PERIPHERAL;            //配置 DMA传输方向：内存到外设
    dmaNewStruct.m_Channel = DMA_Tx;						        //配置 DMA通道选择:通道  TX
    dmaNewStruct.m_LoopMode = FALSE;			                    //配置 循环模式使能: 禁止
    dmaNewStruct.m_PeriphAddr = (uint32_t)&(SPIx->BUFR);      	    //配置 外设起始地址
#if SPI_MASTER
    dmaNewStruct.m_MemoryAddr = (uint32_t)&Tx_master;               //配置 内存起始地址
#else
    dmaNewStruct.m_MemoryAddr = (uint32_t)&Tx_slave;                //配置 内存起始地址
#endif
    DMA_Configuration (DMA_COMx,&dmaNewStruct);				        //写入配置

	SPI_Transmit_DMA_INT_Enable(SPIx,TRUE);							//发送DMA
    DMA_Channel_Enable (DMA_COMx, DMA_Tx, TRUE);				    //DMA通道使能
	DMA_Finish_Transfer_INT_Enable(DMA_COMx,DMA_Tx,TRUE);		    //接收完整数据DMA中断

	SPI_Cmd(SPIx,TRUE);											     //使能SPIx
}
