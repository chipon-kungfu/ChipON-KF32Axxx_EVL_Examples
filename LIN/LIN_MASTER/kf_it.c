/****************************************************************************************
 *
 * 文件名 kf_it.c
 * 作  者	ChipON_AE/FAE_Group
 * 版  本	V2.1
 * 日  期	2019-11-16
 * 描  述  该文件提供了中断入口地址,在T14中断中实现LIN发送
 *
 ****************************************************************************************/
#include"system_init.h"
#include "lin_master.h"

//*****************************************************************************************
//                                 USART1中断函数
//*****************************************************************************************	
void __attribute__((interrupt))_USART1_exception (void)
{
	if(USART_Get_Receive_BUFR_Ready_Flag(USART1_SFR))
	{
		Rev_Temp=USART_ReceiveData(USART1_SFR);//读寄存器值，清除接收中断标志
		//用户接收代码
	}
}


//*****************************************************************************************
//                              T14中断函数
//*****************************************************************************************	//

void __attribute__((interrupt))_T14_exception (void)
{

	BTIM_Clear_Updata_INT_Flag(T14_SFR);										//清更新时间标志位
	BTIM_Clear_Overflow_INT_Flag (T14_SFR);										//清T14溢出中断标志位
//
	GPIO_Toggle_Output_Data_Config(GPIOB_SFR,GPIO_PIN_MASK_8);
	switch(xxx)  //定时循环发送
	{
		case 0://主机读
				l_Tx_data[0]=0xC1;   //标识符0x01 ID场 0xC1
				LIN_Send(USART1_SFR,l_Tx_data,1);//LIN发送
				break;
		case 1://主机写
				l_Tx_data[0]=0x42;   //标识符0x02 ID场 0x42
				l_Tx_data[1]=0x11;   //数据1
				l_Tx_data[2]=0x22;   //数据2
				l_Tx_data[3]=0x11;   //数据3
				l_Tx_data[4]=0x22;   //数据4
				l_Tx_data[5]=0x11;   //数据5
				l_Tx_data[6]=0x12;   //数据6
				l_Tx_data[7]=0x21;   //数据7
				l_Tx_data[8]=0x22;   //数据8
				l_Tx_data[9]=Check_Sum(l_Tx_data,0,9);
				LIN_Send(USART1_SFR,l_Tx_data,10);//LIN发送
				break;
		default:
				break;
	}
	xxx++;
	if(xxx>1)
	{
		xxx=0;
	}


}

