/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2021-3-14
  * 描  述  该文件提供了CAN的收发参考代码
  *
 ****************************************************************************************/
#include "system_init.h"
#include "Usart.h"
#include "canhl.h"
#include "stdio.h"
#define CAN_STB_HIG()        GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_0,Bit_SET)  //设置为高电平
#define CAN_STB_LOW()        GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_0,Bit_RESET)//设置为低电平


void GPIO_USART2();//USART2引脚重映射



//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=7000;
		while(j--);
	}
}
//检测can是否离线，如果离线，做离线处理，恢复上线
void Can_check_busoff()
{
	if(CAN1_SFR->CTLR &(1<<23))//Boff置位
	{
		CAN1_SFR->CTLR &= (~0x01);//清除复位模式后，
	}
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
   /* 用户可参考该例程实现兼容CAN2.0的报文收发，CAN波特率500K，上电后发送
    * 标准数据帧，使用中断处理RAM接收的报文，接收的报文数据通过USART2发出
    * demo用USB口供电*/
	SystemInit();//系统时钟72M,外设高频时钟16M
	GPIO_USART2();//USART2引脚重映射
	USART_Async_config(USART2_SFR);//全双工异步8bit 9600波特率

	/*初始化复位GPIOH,使能GPIOH外设时钟*/
	GPIO_Reset(GPIOH_SFR);
    GPIO_Write_Mode_Bits(GPIOH_SFR,GPIO_PIN_MASK_6,GPIO_MODE_OUT);
    CAN_STB_LOW();//CAN_STB 初始化为低

	xGPIO_CAN();//CAN引脚重映射
	xInit_CAN(CAN1_SFR,CAN_BAUDRATE_500K,CAN_MODE_NORMAL);//CAN模块初始化，125K,正常模式,滤波寄存器组在canhl.c的宏定义修改
	xINT_CAN(CAN1_SFR);//使能CAN接收中断
	CAN_TX_FLAG=1;//发送标志位

    //配置LP_CAN0的PA13、PA14
    GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_13,GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_14,GPIO_MODE_RMP);

    xInit_LP_CAN(CAN0_SFR,CAN_BAUDRATE_500K,CAN_MODE_NORMAL);
//    xInit_CAN(CAN0_SFR,CAN_BAUDRATE_500K,CAN_MODE_NORMAL);
    xINT_CAN(CAN0_SFR);//使能CAN接收中断

    INT_All_Enable (TRUE);

	while(1)
	{
		delay_ms(500);
		Can_check_busoff();
		if(CAN_TX_FLAG)
		{
//				CAN_TX_FLAG=0;
				/* 标准数据帧  */
				CAN_Transmit_DATA(CAN1_SFR,               //CAN通道
								  SENDID,                 //ID
								  SendData,               //数据指针
								  8,   					  //长度
								  CAN_DATA_FRAME,         //帧类型
								  CAN_FRAME_FORMAT_SFF    //帧格式
								 );

//				USART_Send(USART2_SFR,SendData,sizeof(SendData));
		}
		CAN_Transmit_DATA(CAN0_SFR,               //CAN通道
						  SENDID,                 //ID
						  SendData,               //数据指针
						  8,   					  //长度
						  CAN_DATA_FRAME,         //帧类型
						  CAN_FRAME_FORMAT_SFF    //帧格式
						 );

	}
}

/**
  * 描述   USART2引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_USART2()
{
	/* 端口重映射AF5 */
	//USART2_RX		PB15
	//USART2_TX0	PB14
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_15, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOB_SFR ,GPIO_PIN_MASK_14, GPIO_MODE_RMP);          //重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_15, GPIO_RMP_AF5_USART2);	   //重映射为USART2
	GPIO_Pin_RMP_Config (GPIOB_SFR ,GPIO_Pin_Num_14, GPIO_RMP_AF5_USART2);     //重映射为USART2
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_15, TRUE);                  //配置锁存
	GPIO_Pin_Lock_Config (GPIOB_SFR ,GPIO_PIN_MASK_14, TRUE);                  //配置锁存
}
/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  fprintf(USART2_STREAM,"Wrong parameters value: file %s on line %d\r\n", file, line);
	  }
}

