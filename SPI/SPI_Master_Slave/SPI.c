/**
  ******************************************************************************
  *
  * 文件名  SPI.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了SPI主从模式相关功能，包括
  *          + SPI配置
  *
  ******************************************************************************/
#include "system_init.h"
#include "SPI.h"
volatile uint8_t  Command_flag;//命令标志位
volatile uint8_t  Dummy_flag;  //dummy标志位
volatile uint8_t  Uart_flag;   //串口标志位
volatile uint32_t Tx_length;   //发送长度变量
volatile uint32_t Rx_length;   //接收长度变量
uint8_t Tx_master[BufferSize] = "KF32A151-KungFu32 SPI Example master";  //主机Tx
uint8_t Tx_slave[BufferSize]  = "KF32A151-KungFu32 SPI Example slaves";  //从机Tx
uint8_t Read_data[BufferSize]; //读数据缓存

/**
  * 描述    SPI配置
  * 输入   SPIx：指向SPI内存结构的指针，取值为SPI0_SFR~SPI3_SFR
  * 返回   无
  */
void SPI_Init_Configuration(SPI_SFRmap* SPIx)
{
	SPI_InitTypeDef newStruct_SPI;
	/*SPI配置*/
#if SPI_MASTER
	newStruct_SPI.m_Mode = SPI_MODE_MASTER_CLKDIV4;                   //主模式主时钟4分频 18M
#else
	newStruct_SPI.m_Mode = SPI_MODE_SLAVE;                            //从模式
#endif
 	newStruct_SPI.m_Clock = SPI_CLK_SCLK;							  //SPI主频时钟
	newStruct_SPI.m_FirstBit = SPI_FIRSTBIT_MSB;					  //MSB
	newStruct_SPI.m_CKP = SPI_CKP_LOW;                                //SCK空闲为高
	newStruct_SPI.m_CKE = SPI_CKE_1EDGE;                              //第一个时钟开始发送数据
	newStruct_SPI.m_DataSize = SPI_DATASIZE_8BITS;                    //8bit
	newStruct_SPI.m_BaudRate = 0x59;                                  //Fck_spi=Fck/2(m_BaudRate+1)=10us
    SPI_Reset(SPIx);											      //复位模块
    SPI_Configuration(SPIx, &newStruct_SPI);					      //写入结构体配置
	SPI_Cmd(SPIx,TRUE);											      //使能
}
