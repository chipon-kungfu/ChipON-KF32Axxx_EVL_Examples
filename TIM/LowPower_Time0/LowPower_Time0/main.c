/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: LowPower_Time0
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了实现低功耗定时器T0功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"

/**
  * 描述 低功耗定时器T0定时时间配置。
  * 输入  Period：新的周期值，取值16位数据
  *     Prescaler：对INTLF的预分频值
  * 返回  无。
  */
void LPTIMER_T0_Config(uint32_t Period,uint32_t Prescaler)
{

	OSC_SCK_Source_Config(SCLK_SOURCE_INTHF);                 //选择INTHF作为系统时钟

	OSC_Backup_Write_Read_Enable(TRUE);                       //使能备份域读写功能

	PM_CCP0CLKLPEN_Enable(TRUE);                              //CCP0低功耗时钟使能

	TIM_Reset(T0_SFR);										  //定时器外设复位，使能外设时钟
	GPTIM_Updata_Immediately_Config(T0_SFR,TRUE);			  //立即更新控制
	GPTIM_Updata_Enable(T0_SFR,TRUE);						  //配置更新使能
	GPTIM_Work_Mode_Config(T0_SFR,GPTIM_TIMER_MODE);		  //定时模式选择
	GPTIM_Set_Counter(T0_SFR,0);							  //定时器计数值
	GPTIM_Set_Period(T0_SFR,Period);						  //定时器周期值
	GPTIM_Set_Prescaler(T0_SFR,Prescaler);				      //定时器预分频值
	GPTIM_Counter_Mode_Config(T0_SFR,GPTIM_COUNT_UP_DOWN_OUF);//向上-向下计数,上溢和下溢产生中断标志
	GPTIM_Clock_Config(T0_SFR,GPTIM_LFCLK);					  //选用LFCLK时钟

	INT_Interrupt_Priority_Config(INT_T0,4,0);				  //抢占优先级4,子优先级0
	GPTIM_Overflow_INT_Enable(T0_SFR,TRUE);					  //计数溢出中断使能
	INT_Interrupt_Enable(INT_T0,TRUE);						  //外设中断使能
	INT_Clear_Interrupt_Flag(INT_T0);						  //清中断标志
	GPTIM_Cmd(T0_SFR,TRUE);									  //定时器启动控制使能
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);			  //中断自动堆栈使用单字对齐
	INT_All_Enable (TRUE);									  //全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断

	OSC_Backup_Write_Read_Enable(FALSE);                      //禁止备份域读写功能

	OSC_SCK_Source_Config(SCLK_SOURCE_PLL);                   //选择PLL作为系统时钟
	while(OSC_Get_INTHF_INT_Flag() != SET);                   //等PLL时钟稳定

}





/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
		/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
			GPIO_Reset(GPIOx);

		/* 配置 Pxy作为输出模式参数 */
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_Struct_Init(&GPIO_InitStructure);
		GPIO_InitStructure.m_Pin = GpioPin;
		GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
		GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
		GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
		GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
		GPIO_Configuration(GPIOx,&GPIO_InitStructure);

		GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	 //先设置为高电平
}



//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现使用低功耗定时器T0定时功能。
	 * 例程中定时器T0时钟源为内部低频32KHZ 1：64分频，计数500次，1秒进一次中断，PB13在T0中断函数中翻转，D4灯闪烁
	 *
	 *注意：在设置使能低功耗定时器T0之前，需要使能备份域的读写，并且切换在低频时钟里操作完后再切换成高频时钟
     */
	//系统时钟120M,外设高频时钟16M
	SystemInit();   //时钟初始化
	OSC_LFCK_Division_Config(LFCK_DIVISION_1);		//低频工作分频选择
	OSC_LFCK_Source_Config (LFCK_INPUT_INTLF);		//选择INTLF作为LFCLK时钟
	OSC_LFCK_Enable (TRUE);							//LFCLK时钟信号允许
	OSC_INTLF_Software_Enable(TRUE);
	OSC_INTHF_Software_Enable(TRUE);				//内部高频振荡器软件使能
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//把PB13设为输出，并设为高电平
	//低功耗定时器设置周期值
	LPTIMER_T0_Config(500,63);//   预分频值1:64  时钟源32KHZ, 500HZ计数500次，1秒进一次中断
    while(1)
    {
    }

}


void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

