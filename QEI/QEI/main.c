/****************************************************************************************
  *
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-11-16
  * 描  述  该文件提供了QEI应用参考例程
  *
 ****************************************************************************************/
#include "system_init.h"
#include "QEI.h"
#define LED_ON_OFF()    GPIO_Toggle_Output_Data_Config(GPIOB_SFR, GPIO_PIN_MASK_13)    //LED电平取反

void GPIO_QEI0();//SQEI0 IO口重映射

/**
  * 描述    程序延时
  * 输入   a：延时参数
  *      b：延时参数
  * 返回   无
  */
void Delay(volatile uint32_t cnt)
{
	while(cnt--);
}
/**
  * 描述   主函数
  * 输入   无
  * 返回   无
  */
void main()
{
	uint32_t i;
   /* 用户可参考该例程在KF32A151_demo板上实现QEI正交编码周期计数功能，QEIA,QEIB接编码器，
	* 编码器A/B相信号为A超前B(00,10,11,01)的每两个循环，或者B超前A(00,10,11,01)的每两个
	* 循环，进入一次中断,GPIOF3输出一次脉冲。换向时反向计数。PA6输出高低电平指示计数方向，
	* 可根据该信号判定正反转。另提供T0模拟的正交编码信号供板上测试使用(PF1,PF2)
	* 信号连接：PB14(QEA0) --- PF1
	*          PB15(QEBA0) --- PF2*/

	//系统时钟120M,外设高频时钟16M
	SystemInit();
    /*初始化复位GPIOF外设用于输出,使能GPIOF外设时钟*/
    GPIO_Reset(GPIOF_SFR);
	GPIO_Write_Mode_Bits(GPIOF_SFR,GPIO_PIN_MASK_1|GPIO_PIN_MASK_2|GPIO_PIN_MASK_3,GPIO_MODE_OUT);
	/*初始化复位GPIOB外设用于LED,使能GPIOB外设时钟*/
    GPIO_Reset(GPIOB_SFR);
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,GPIO_MODE_OUT);

	/*T0定时模式*/
    QT0_Configuration();
	/*QEI0_IO配置*/
    GPIO_QEI0(); //PB14=QEI_A0,PB15=QEI_B0,PA6=QEI0DIR
	/*QEI0配置*/
    QEI_Init_Configuration(QEI0_SFR);
	INT_All_Enable(TRUE);//总中断使能

    while(1)
	{
    	LED_ON_OFF();
    	for(i =0;i<1000;i++)
    	{
    		Delay(1000);
    	}
	}
}

/**
  * 描述   GPIO_QEI0()引脚重映射
  * 输入   无
  * 返回   无
  */
void GPIO_QEI0()
{
	/*QEI_IO配置*/
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_14,GPIO_MODE_RMP);			//PB14配置成QEI_A0 引脚
	GPIO_Write_Mode_Bits(GPIOB_SFR,GPIO_PIN_MASK_15,GPIO_MODE_RMP);			//PB15配置成QEI_B0 引脚
	GPIO_Write_Mode_Bits(GPIOA_SFR,GPIO_PIN_MASK_6,GPIO_MODE_RMP);		    //PA6配置成QEI_DIR0 引脚
    GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_14,GPIO_RMP_AF4_QEI0);		//复用到QEI
	GPIO_Pin_RMP_Config(GPIOB_SFR,GPIO_Pin_Num_15,GPIO_RMP_AF4_QEI0);		//复用到QEI
    GPIO_Pin_RMP_Config(GPIOA_SFR,GPIO_Pin_Num_6,GPIO_RMP_AF4_QEI0);		//复用到QEI
}

/**
  * 描述   报告校验发生错误的文件和行
  * 输入   file: 指向文件名称的指针
  *      line： 校验错误的行
  * 返回  无。
  */
void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	while(1)
	  {

	  }
}
