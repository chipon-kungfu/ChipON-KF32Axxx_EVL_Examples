/****************************************************************************************
 *
 * 文件名: kf_it.c
 * 项目名: KF32F350_TimeBase
 * 版 本: v1.0
 * 日 期: 2019年10月17日 14时41分04秒
 * 作 者: 80984
 * 
 ****************************************************************************************/
#include "system_init.h"
#include "I2C_DMA.h"
#include "Usart.h"

//*****************************************************************************************
//                               I2C1中断函数
//*****************************************************************************************
void __attribute__((interrupt))_I2C1_exception(void)
{

	I2C_Clear_INTERRUPT_Flag(I2C1_SFR);//清I2C1中断标志位
	I2C_Ack_DATA_Config(I2C1_SFR,I2C_ACKDATA_ACK);//回复ACK


}
//*****************************************************************************************
//                               DMA1中断函数
//*****************************************************************************************
void __attribute__((interrupt))_DMA0_exception(void)
{
	DMA_Clear_INT_Flag (DMA0_SFR,DMA_CHANNEL_6,DMA_INT_FINISH_TRANSFER);
	DMA_Clear_INT_Flag (DMA0_SFR,DMA_CHANNEL_7,DMA_INT_FINISH_TRANSFER);
}


