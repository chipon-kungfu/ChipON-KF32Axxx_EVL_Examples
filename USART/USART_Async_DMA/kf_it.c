/****************************************************************************************
 *
 * 文件名 kf_it.c
 * 作  者	ChipON_AE/FAE_Group
 * 版  本	V2.1
 * 日  期	2019-11-16
 * 描  述  该文件提供了例程串口中断入口地址
 *
 ****************************************************************************************/
#include"system_init.h"
#include "Usart.h"
//*****************************************************************************************
//                                 USART1中断函数
//*****************************************************************************************	
void __attribute__((interrupt))_USART2_exception (void)
{
	if(USART_Get_Receive_Frame_Idel_Flag(USART2_SFR))
	{
		USART_Clear_IDLE_INT_Flag(USART2_SFR);
		ReceiveIdlE_flag=1;
	}

}

void __attribute__((interrupt)) _DMA0_exception (void)
{

	if(DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR,DMA_CHANNEL_6))
	{
		DMAReceiveover_flag = 1;
		DMA_Clear_INT_Flag(DMA0_SFR,DMA_CHANNEL_6,DMA_INT_FINISH_TRANSFER);
	}
}
