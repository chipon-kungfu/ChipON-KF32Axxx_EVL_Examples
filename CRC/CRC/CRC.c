/**
  ******************************************************************************
  * 文件名  CRC.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了CRC模块与应用相关函数，包括
  *          + CRC配置
  *          + 硬件CRC运算
  ******************************************************************************/
#include "system_init.h"
#include "CRC.h"
void CRC_Init_Config(); //CRC配置
uint32_t CRC_CalcBlockCRC(uint32_t* aData,uint32_t length);//硬件CRC运算


/**
  * 描述    硬件CRC运算
  * 输入   aData：32bit CRC校验输入数据
  *      length：33bit 数据长度
  * 返回   无
  */
uint32_t CRC_CalcBlockCRC(uint32_t* aData,uint32_t length)
{
	uint32_t i;
	uint32_t CRCValue_buf;

	CRC_INPUT_DATA(aData[0]);//设置初值
	CRC_SET_RSET ();//复位计算单元

	for(i=1;i<length;i++)
	{
		CRC_INPUT_DATA(aData[i]);//循环运算按
	}
	CRCValue_buf=CRC_GET_RESULT();//获取CRC计算结果


	return CRCValue_buf;
}
/**
  * 描述   CRC外设初始化配置
  * 输入   无
  * 返回   无
  */
void CRC_Init_Config()
{
	CRC_InitTypeDef CRCInitStruct;

	CRC_Reset ();//CRC模块复位（使能模块）
	CRC_SET_INITVALUE(0xFFFFFFFF);//CRC初始值
	CRC_SET_RXOR(0x00000000);//CRC-32/MPEG-2异或选择
	//CRC_SET_RXOR(0xFFFFFFFF);//CRC-32异或选择
	CRC_SET_PLN(0x04C11DB7);//默认CRC_32

	CRCInitStruct.m_CalUnitReset = CRC_CAL_RSET_ENABLE;  //CRC功能复位（重开始计算）
	CRCInitStruct.m_InputSize = CRC_INPUT_SEZE_32;//32位数据
	CRCInitStruct.m_InputReverse = CRC_INPUT_REVERSE_DISABLE;//输入反向禁止
	CRCInitStruct.m_ResultReverse = CRC_RESULT_REVERSE_DISABLE;//输出反向禁止
	CRC_Configuration(&CRCInitStruct);//配置CRC
}
