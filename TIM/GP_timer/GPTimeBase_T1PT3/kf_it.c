/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
//*****************************************************************************************
//                              T1中断函数
//*****************************************************************************************
void __attribute__((interrupt))_T1_exception (void)
{
	GPTIM_Clear_Updata_INT_Flag(T1_SFR);
	GPTIM_Clear_Overflow_INT_Flag(T1_SFR);

}

void __attribute__((interrupt))_T3_exception (void)
{
	GPTIM_Clear_Updata_INT_Flag(T3_SFR);
	GPTIM_Clear_Overflow_INT_Flag (T3_SFR);
	//PB13翻转
	GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_13);

}


