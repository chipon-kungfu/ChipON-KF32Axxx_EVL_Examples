/**
  ******************************************************************************
  * 文件名  Adv_timer.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了关于高级定时器的使用方法
  *
  *********************************************************************
  */
#include "system_init.h"
#include "Adv_timer.h"
/**
  * 描述   Tx系列定时器的配置
  * 输入   T5_SFR
  * 返回   无
  */
void cfg_Tx(ATIM_SFRmap* ATIMx)
{
	ATIM_X_Work_Mode_Config(ATIMx,ATIM_TIMER_MODE);									//定时模式   ECCP_CMP_TOGGLE_LEVEL
	ATIM_X_Set_Counter(ATIMx,0);														//T9计数值
	ATIM_X_Set_Period(ATIMx,0xFFFF);													//周期值
	ATIM_X_Set_Prescaler(ATIMx,15);													//预分频器 1:1
	ATIM_X_Postscaler_Config(ATIMx,ATIM_POSTSCALER_DIV_1);								//后分频比为1:1
	ATIM_X_Counter_Mode_Config(ATIMx,ATIM_COUNT_UP_OF);								//中心对齐的PWM信号,向上-向下计数，上溢时产生中断标志
	ATIM_X_Clock_Config(ATIMx,ATIM_SCLK);												//选用HFCLK时钟为Tx工作时钟

	ATIM_X_Updata_Output_Ctl(ATIMx,TRUE);												//立即更新以Tx为时基的输出控制寄存器
	ATIM_X_Updata_Enable(ATIMx,TRUE);													//允许以Tx为时基占空比、周期、预分频、更新计数器、输出控制寄存器更新
	ATIM_X_Updata_Immediately_Config(ATIMx,TRUE);										//立即更新控制位

	ATIM_X_Cmd(ATIMx,TRUE);															//Tx启动控制使能
}

/**
  * 描述   配置T5定时器的中断
  * 输入   无
  * 返回   无
  */
void cfg_T5_intrup()
{
	ATIM_X_Overflow_INT_Enable(T5_SFR,TRUE);
	INT_Interrupt_Enable(INT_T5,TRUE);													//外设中断使能
	INT_Clear_Interrupt_Flag(INT_T5);													//清中断标志
	INT_All_Enable (TRUE);																//全局可屏蔽中断使能,该中断使能控制不包含复位/NMI/硬件错误中断
}

