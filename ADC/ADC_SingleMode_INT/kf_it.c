/**
  ******************************************************************************
  * 文件名  kf_it.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.3
  * 日  期  2019-11-16
  * 描  述  该文件提供了部分中断入口地址
  *
  *********************************************************************
  */
#include "system_init.h"
#include "ADC_user.h"
#include "stdio.h"
//*****************************************************************************************
//                                 NMI中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               硬件错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                                堆栈错误中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick中断函数
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//*****************************************************************************************
//                              ADC中断函数
//*****************************************************************************************
void __attribute__((interrupt))_ADC0_exception (void)
{
	uint16_t buf=0;
    static uint8_t flag =1;
	if(ADC_Get_INT_Flag(ADC_UNIT,ADC_INT_EOC))  //常规通道中断
	{
		ADC_Clear_INT_Flag(ADC_UNIT,ADC_INT_EOC);
		buf=ADC_Get_Conversion_Value(ADC_UNIT);

		if(flag)
		{
			fprintf(USART2_STREAM,"ADC0 CH42 ADC值为: %d \r\n",buf);
			ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_2, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_2
			flag =0;
		}
		else
		{
			fprintf(USART2_STREAM,"ADC0 CH2 ADC值为: %d \r\n",buf);
			ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_42, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_1
			flag =1;
		}
	}
}

void __attribute__((interrupt))_ADC1_exception (void)
{
	uint16_t buf=0;
    static uint8_t flag =1;
	if(ADC_Get_INT_Flag(ADC_UNIT,ADC_INT_EOC))  //常规通道中断
	{
		ADC_Clear_INT_Flag(ADC_UNIT,ADC_INT_EOC);
		buf=ADC_Get_Conversion_Value(ADC_UNIT);

		if(flag)
		{
			fprintf(USART2_STREAM,"ADC1 CH42 ADC值为: %d \r\n",buf);
			ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_2, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_2
			flag =0;
		}
		else
		{
			fprintf(USART2_STREAM,"ADC1 CH2 ADC值为: %d \r\n",buf);
			ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_42, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_1
			flag =1;
		}
	}
}

void __attribute__((interrupt))_ADC2_exception (void)
{
	uint16_t buf=0;
    static uint8_t flag =1;
	if(ADC_Get_INT_Flag(ADC_UNIT,ADC_INT_EOC))  //常规通道中断
	{
		ADC_Clear_INT_Flag(ADC_UNIT,ADC_INT_EOC);
		buf=ADC_Get_Conversion_Value(ADC_UNIT);

		if(flag)
		{
			fprintf(USART2_STREAM,"ADC2 CH42 ADC值为: %d \r\n",buf);
			ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_2, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_2
			flag =0;
		}
		else
		{
			fprintf(USART2_STREAM,"ADC2 CH2 ADC值为: %d \r\n",buf);
			ADC_Regular_Channel_Config (ADC_UNIT, ADC_CHANNEL_42, 0x01); //ADC0常规通道1的采样源设置为ADC_CHANNEL_1
			flag =1;
		}
	}
}

