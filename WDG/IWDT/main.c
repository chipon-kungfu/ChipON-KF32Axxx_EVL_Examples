/****************************************************************************************
 *
 * 文件名: main.c
 * 项目名: IWDG
 * 版 本:  V2.3
 * 日 期:  2019年11月16日
 * 作  者  ChipON_AE/FAE_Group
 * 描  述  该文件提供了实现独立看门狗功能配置参考例程。
 *
 ****************************************************************************************/
#include "system_init.h"
#define    KEYUSER    GPIO_Read_Input_Data_Bit(GPIOD_SFR,GPIO_PIN_MASK_7)  //读取PD7的输入电平

//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(volatile uint32_t nms)
{
	volatile uint32_t i,j;
	for(i=0;i<nms;i++)
	{
		j=7000;
		while(j--);
	}

}

/**
  * 描述  GPIOx 输出初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Output_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输出模式参数 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;          //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_OUT;            //初始化 GPIO方向为输出
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;            //初始化 GPIO是否上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;          //初始化 GPIO是否下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);

	GPIO_Set_Output_Data_Bits(GPIOx,GpioPin,Bit_SET);	  //先设置为高电平

}

/**
  * 描述  GPIOx 输入初始化配置。
  * 输入 : GPIOx: 指向GPIO内存结构的指针，取值为GPIOA_SFR~GPIOH_SFR。
  *       GpioPin: 端口引脚掩码，取值为GPIO_PIN_MASK_0~GPIO_PIN_MASK_15中的一个或多个组合。
  * 返回  无。
  */
void GPIOInit_Input_Config(GPIO_SFRmap* GPIOx,uint16_t GpioPin)
{
	/*初始化复位GPIOx外设，使能GPIOx外设时钟*/
		GPIO_Reset(GPIOx);

	/* 配置 Pxy作为输入模式 */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_Struct_Init(&GPIO_InitStructure);
	GPIO_InitStructure.m_Pin = GpioPin;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;                   //初始化 GPIO输出速度
	GPIO_InitStructure.m_Mode = GPIO_MODE_IN;                      //初始化 GPIO方向为输入
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;                     //初始化 GPIO是否上拉 不上拉
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;                   //初始化 GPIO是否下拉 不下拉
	GPIO_Configuration(GPIOx,&GPIO_InitStructure);
}


/**
  * 描述  GPIOx 输入与输出初始化配置。
  * 输入 : 无
  * 返回  无。
  */
void GPIO_Init(void)
{
	GPIOInit_Input_Config(GPIOD_SFR,GPIO_PIN_MASK_7);//把PD7设为输入模式
	GPIOInit_Output_Config(GPIOB_SFR,GPIO_PIN_MASK_13);//把PB13设为输出，并设为高电平
}



/**
  * 描述 独立看门狗设定时间配置。
  * 输入  Overflow：取值为0~0xFFF。 独立看门狗溢出值
  *     Prescaler：对INTLF的预分频值
  * 返回  无。
  */
void IWDT_Config(uint32_t Overflow,uint32_t Prescaler)
{

#ifdef SYSCLK_FREQ_HSI
	OSC_SCK_Source_Config(SCLK_SOURCE_INTHF);                       //选择INTHF作为系统时钟降频
#else
	OSC_SCK_Source_Config(SCLK_SOURCE_EXTHF);                       //选择EXTHF作为系统时钟降频
#endif
	BKP_Write_And_Read_Enable(TRUE);                                //备份域读写使能
	PM_Independent_Watchdog_Reset_Config(PERIPHERAL_OUTRST_STATUS); //使能独立看门狗退出复位状态
	PM_Internal_Low_Frequency_Enable(TRUE);                         //内部低频晶振使能
	IWDT_Overflow_Config (Overflow);                                //独立看门狗溢出值，取值为500。//1秒钟看门狗溢出
	IWDT_Prescaler_Config(Prescaler);                               //独立看门狗对INTLF的1：64预分频值
	IWDT_Enable(TRUE);                                              //使能独立看门狗
	BKP_Write_And_Read_Enable(FALSE);

	OSC_SCK_Source_Config(SCLK_SOURCE_PLL);                         //选择pll作为系统时钟
}




//主函数
void main()
{
	/*用户可参考该例程在KF32A151_demo板上实现使用独立看门狗功能。
	 * 例程中独立看门狗的时钟源为内部32KHZ 1：64分频，计数500次，复位时间为1秒
	 * 在没有KEY2按键按下时，系统以1秒复位一次D4灯闪烁，按下KEYUSER键后不松手系统正常，D4灯常亮
	 *
	 *注意：在设置独立看门狗之前，需要切换到低频后打开备份域才设置独立看门狗功能
     */
	//系统时钟120M,外设高频时钟16M
	SystemInit(); //系统时钟初始化
	GPIO_Init();  //GPIO初始化
	//独立看门狗配置
	IWDT_Config(500,IWDT_PRESCALER_64);//独立看门狗配置 32KHZ 1：64分频，计数500次， 1秒钟后溢出系统复位

	//PB13输出高电平
	GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_13,Bit_SET);
	while(1)
    {
		if(KEYUSER)
		{
			IWDT_Feed_The_Dog();//按键KEYUSER按下后喂狗系统正常工作，没喂狗时则1秒种系统复位

		}
		delay_ms(20);
    }

}



void check_failed(uint8_t* file, uint32_t line)
{
	  /* 用户可以添加自己的代码实现报告文件名和行号,
	             例如: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	  while(1)
	  {
		  ;
	  }
}

