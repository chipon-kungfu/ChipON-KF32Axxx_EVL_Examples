/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd
 ******************************************************************************
 *  $File Name        : __driver_Flash_API.h
 *  $Author           : ChipON AE/FAE Group
 *  $Data             : 23-05-17
 *  $Lib Version      : V2.0.2.230517
 *  $Description      :
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd
 *  All rights reserved.
 *
 *  This software is copyright protected and proprietary to
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.
 ******************************************************************************/

#ifndef DRIVER_FLASH_OP_API_HEAD_H_
#define DRIVER_FLASH_OP_API_HEAD_H_
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ user config 1

#include <stdint.h>

#define Function_Parameter_Validate 0x5A5A6688

#define CMD_SUCCESS 0x00U
#define CMD_ERROR   0xAAU
#define PARAM_ERROR 0x0CU

#define TimeOutCountValue 5000000U
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define __FLASH__ __attribute__((section(".text")))
#define __RAM__   __attribute__((section(".indata")))

#define optimize_Os __attribute__((optimize("-Os")))
#define optimize_O2 __attribute__((optimize("-O2")))
#define optimize_O0 __attribute__((optimize("-O0")))

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ user config

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
uint8_t __FLASH_Read_Byte__(uint32_t address);
uint32_t  __FLASH_Read_WORD__(uint32_t address);
uint32_t  __FLASH_Read_One__(uint32_t address);
uint32_t  __FLASH_Read__(uint32_t address, uint32_t length, uint32_t buffers[]);

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
uint32_t __FLASH_UNLOCK_FUNCTION__(volatile uint32_t key, volatile uint32_t isopen);
uint32_t __FLASH_Erase__(volatile uint32_t key, volatile uint32_t WriteAddr, volatile uint32_t DataLength);
uint32_t __FLASH_Program__(volatile uint32_t key, volatile uint32_t WriteAddr, volatile uint32_t DataLength, uint32_t WriteData[]);
uint32_t __FLASH_Program_NBytes__(volatile uint32_t key, volatile uint32_t WriteAddr, volatile uint32_t DataLength, uint32_t WriteData[]);
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ user config 1
#endif /* DRIVER_FLASH_OP_API_HEAD_H_ */
