/**
  ******************************************************************************
  * 文件名  main.c
  * 作  者  ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了基于外设库的最小代码
  *
  *********************************************************************
  */

#include "system_init.h"
#include "ADC_user.h"
void Init_adc0()
{
	ADC_InitTypeDef adcStruct;

	GPIO_Write_Mode_Bits(GPIOG_SFR,GPIO_PIN_MASK_4,GPIO_MODE_AN);//ADC_CHANNEL_22
	GPIO_Write_Mode_Bits(GPIOG_SFR,GPIO_PIN_MASK_1,GPIO_MODE_AN);//ADC_CHANNEL_19
	GPIO_Write_Mode_Bits(GPIOG_SFR,GPIO_PIN_MASK_0,GPIO_MODE_AN);//ADC_CHANNEL_18

	ADC_Reset (ADC0_SFR);

	/* 初始化 ADC时钟源选择 */
	adcStruct.m_Clock = ADC_HFCLK;
	/* 初始化 ADC时钟分频 */
	adcStruct.m_ClockDiv = ADC_CLK_DIV_32; //8M的ADC转换时钟源
	/* 初始化 ADC扫描模式使能 */
	adcStruct.m_ScanMode = TRUE;
	/* 初始化 ADC连续转换模式 */
	adcStruct.m_ContinuousMode = ADC_SINGLE_MODE;
	/* 初始化 ADC转换结果输出格式 */
	adcStruct.m_DataAlign = ADC_DATAALIGN_RIGHT;
	/* 初始化 ADC常规通道外部触发转换模式使能 */
	adcStruct.m_ExternalTrig_EN = TRUE;
	/* 初始化 ADC常规通道外部触发事件 */
	adcStruct.m_ExternalTrig = ADC_EXTERNALTRIG_T1TRGO; //T1的触发事件作为触发信号，T1的触发信号的来源由T1自己配置
	/* 初始化 ADC高优先级通道外部触发转换模式使能 */
	adcStruct.m_HPExternalTrig_EN = TRUE;
	/* 初始化 高优先级通道外部触发事件 */
	adcStruct.m_HPExternalTrig = ADC_HPEXTERNALTRIG_CCP2_CH1; //CCP1的比较模式的事件作为触发信号
	/* 参考电压选择，取值为宏“ADC参考电压选择”中的一个。 */
	adcStruct.m_VoltageRef=ADC_REF_AVDD;
	/* 初始化 ADC常规通道扫描长度 */
	adcStruct.m_NumOfConv = 3;
	/* 初始化 ADC高优先级通道扫描长度 */
	adcStruct.m_NumOfHPConv = 3;
	ADC_Configuration (ADC0_SFR, &adcStruct);
	ADC_Cmd (ADC0_SFR, TRUE); //ADC使能

	ADC_Regular_Channel_Config (ADC0_SFR, ADC_CHANNEL_22, 0x01);
	ADC_Regular_Channel_Config (ADC0_SFR, ADC_CHANNEL_19, 0x02);
	ADC_Regular_Channel_Config (ADC0_SFR, ADC_CHANNEL_18, 0x03);
}

void Init_adc1()
{
	ADC_InitTypeDef adcStruct;

	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_9,GPIO_MODE_AN);//ADC_CHANNEL_9
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_1,GPIO_MODE_AN);//ADC_CHANNEL_1
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_2,GPIO_MODE_AN);//ADC_CHANNEL_2

	ADC_Reset (ADC1_SFR);

	/* 初始化 ADC时钟源选择 */
	adcStruct.m_Clock = ADC_HFCLK;
	/* 初始化 ADC时钟分频 */
	adcStruct.m_ClockDiv = ADC_CLK_DIV_32;
	/* 初始化 ADC扫描模式使能 */
	adcStruct.m_ScanMode = TRUE;
	/* 初始化 ADC连续转换模式 */
	adcStruct.m_ContinuousMode = ADC_SINGLE_MODE;
	/* 初始化 ADC转换结果输出格式 */
	adcStruct.m_DataAlign = ADC_DATAALIGN_RIGHT;
	/* 初始化 ADC常规通道外部触发转换模式使能 */
	adcStruct.m_ExternalTrig_EN = FALSE;
	/* 初始化 ADC常规通道外部触发事件 */
	adcStruct.m_ExternalTrig = ADC_EXTERNALTRIG_T1TRGO;
	/* 初始化 ADC高优先级通道外部触发转换模式使能 */
	adcStruct.m_HPExternalTrig_EN = FALSE;
	/* 初始化 高优先级通道外部触发事件 */
	adcStruct.m_HPExternalTrig = ADC_HPEXTERNALTRIG_CCP1_CH1;
	/* 参考电压选择，取值为宏“ADC参考电压选择”中的一个。 */
	adcStruct.m_VoltageRef=ADC_REF_AVDD;
	/* 初始化 ADC常规通道扫描长度 */
	adcStruct.m_NumOfConv = 3;
	/* 初始化 ADC高优先级通道扫描长度 */
	adcStruct.m_NumOfHPConv = 3;
	ADC_Configuration (ADC1_SFR, &adcStruct);
	ADC_Cmd (ADC1_SFR, TRUE); //ADC使能

	ADC_Regular_Channel_Config (ADC1_SFR, ADC_CHANNEL_9, 0x01);
	ADC_Regular_Channel_Config (ADC1_SFR, ADC_CHANNEL_1, 0x02);
	ADC_Regular_Channel_Config (ADC1_SFR, ADC_CHANNEL_2, 0x03);
}
